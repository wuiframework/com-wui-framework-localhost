/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Connectors {
    "use strict";
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import HttpMethodType = Com.Wui.Framework.Commons.Enums.HttpMethodType;
    import BaseResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.BaseResponse;
    import IResponsePromise = Com.Wui.Framework.Localhost.Interfaces.IResponsePromise;

    class MockResponse extends BaseResponse {
        public Send(...$args : any[]) : IResponsePromise {
            this.callbacks.onsend.apply(this, $args);
            return super.Send($args);
        }

        public OnStart(...$args : any[]) : IResponsePromise {
            this.callbacks.onstart.apply(this, $args);
            return super.OnStart($args);
        }

        public OnChange(...$args : any[]) : IResponsePromise {
            this.callbacks.onchange.apply(this, $args);
            return super.OnChange($args);
        }

        public OnComplete(...$args : any[]) : IResponsePromise {
            this.callbacks.oncomplete.apply(this, $args);
            return super.OnComplete($args);
        }
    }

    export class FileSystemHandlerTest extends UnitTestRunner {

        private fileSystem : FileSystemHandler;
        private testDir : string;

        constructor() {
            super();
            // this.setMethodFilter("testDownloadFile");
        }

        public testExists() : void {
            assert.ok(this.fileSystem.Exists(process.cwd()));
            assert.equal(this.fileSystem.Exists(this.testDir + ""), true);
            assert.equal(this.fileSystem.Exists(this.testDir + "/testFile.txt"), true);
            assert.equal(this.fileSystem.Exists(this.testDir + "/*"), false);
            assert.equal(this.fileSystem.Exists(this.testDir + "/**/*"), false);
            assert.equal(this.fileSystem.Exists(this.testDir + "/archTest/doc1.txt"), true);
            assert.equal(this.fileSystem.Exists(""), false);
            assert.equal(this.fileSystem.Exists(this.testDir + "\\archTest\\doc1.txt"), true);
            assert.equal(this.fileSystem.Exists("!@#$%^&*()_+}{\"?>MNBG$#"), false);
            assert.equal(this.fileSystem.Exists(EnvironmentHelper.IsWindows() ? "C:/" : "/"), true);
        }

        public testIsEmpty() : void {
            assert.equal(this.fileSystem.IsEmpty(this.testDir), false);
            assert.equal(this.fileSystem.IsEmpty(this.testDir + "/testFile.ts"), true);
            assert.equal(this.fileSystem.IsEmpty(this.testDir + "/archTest"), false);
            assert.equal(this.fileSystem.IsEmpty(this.testDir + "/emptyDirectory"), true);
            assert.equal(this.fileSystem.IsEmpty(this.testDir + "/testRead.txt"), true);
            assert.equal(this.fileSystem.IsEmpty(""), true);
            assert.equal(this.fileSystem.IsEmpty("!@#$%^&*()_+}{:L>?><M"), true);
        }

        public testIsFile() : void {
            assert.equal(this.fileSystem.IsFile(this.testDir + ""), false);
            assert.equal(this.fileSystem.IsFile(this.testDir + "/testFile.txt"), true);
            assert.equal(this.fileSystem.IsFile(this.testDir + "/*"), false);
            assert.equal(this.fileSystem.IsFile(this.testDir + "/**/*"), false);
            assert.equal(this.fileSystem.IsFile(this.testDir + "/archTest/doc1.txt"), true);
            assert.equal(this.fileSystem.IsFile(""), false);
            assert.equal(this.fileSystem.IsFile(this.testDir + "\\archTest\\doc1.txt"), true);
            assert.equal(this.fileSystem.IsFile("!@#$%^&*()_+}{\"?>MNBG$#"), false);
            assert.equal(this.fileSystem.IsFile(EnvironmentHelper.IsWindows() ? "C:/" : "/"), false);
        }

        public testIsDirectory() : void {
            assert.equal(this.fileSystem.IsDirectory(this.testDir + ""), true);
            assert.equal(this.fileSystem.IsDirectory(this.testDir + "/archTest/*"), false);
            assert.equal(this.fileSystem.IsDirectory(this.testDir + "/archTest/f1"), true);
            assert.equal(this.fileSystem.IsDirectory(this.testDir + "/testFile.txt"), false);
            assert.equal(this.fileSystem.IsDirectory(""), false);
            assert.equal(this.fileSystem.IsDirectory("!@#$%^&*()_+}{\"?>MNBG$#"), false);
            assert.equal(this.fileSystem.IsDirectory(EnvironmentHelper.IsWindows() ? "C:/" : "/"), true);
        }

        public testIsSymbolicLink() : void {
            assert.equal(this.fileSystem.IsSymbolicLink(this.testDir + ""), false);
            assert.equal(this.fileSystem.IsSymbolicLink(this.testDir + "/testFile.txt"), false);
            const symbol : string = "resource/data/Com/Wui/Framework/Localhost/Connectors/archTest/doc1.txt";
            assert.equal(this.fileSystem.IsSymbolicLink(symbol), false);
            assert.equal(this.fileSystem.IsSymbolicLink("www.wuiframework.com"), false);
            assert.equal(this.fileSystem.IsSymbolicLink("!$%^&*()_+{{}\":>?><"), false);
        }

        public __IgnoretestIsSymbolicLinkOnFS() : void {
            if (!EnvironmentHelper.IsWindows()) {
                // test this only on linux, for windows it needs admin rights to run mklink <link> <target>
                // or mklink /D <link> <target> for directory link and it could be hard to use by real apps
                // and may be not working with lstat()

                // TODO: create links dynamically over terminal command
                assert.equal(this.fileSystem.IsSymbolicLink(this.testDir + "/swichlink"), true);
                assert.equal(this.fileSystem.IsSymbolicLink(this.testDir + "\\swichlink"), true);
            }
        }

        public testgetTempPath() : void {
            if (EnvironmentHelper.IsWindows()) {
                assert.equal(StringUtils.PatternMatched("*:/Users/*/AppData/Local/Temp", this.fileSystem.getTempPath()), true);
            } else if (EnvironmentHelper.IsLinux()) {
                assert.equal(this.fileSystem.getTempPath(), "/tmp");
            } else if (EnvironmentHelper.IsMac()) {
                assert.equal(this.fileSystem.getTempPath(), "/var/folder/../T");
            }
        }

        public testCreateDirectory() : void {
            assert.equal(this.fileSystem.CreateDirectory("/test/resource/data/Com/Wui/Framework/Localhost/Connectors"), true);
            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/copydir"), true);

            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/copydir/subdir"),
                true);

            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/copydir/subdir/testFileX"), true);
            // too long name of directory
            assert.equal(this.fileSystem.CreateDirectory(
                this.testDir + "/copydir/totojestrasnedlouhe" +
                "jmenoadresaretotojestrasnedlouhejmenoadresaretotojestrasnedlouhejmenoadresaretotojestrasnedlouhejmenoadresare" +
                "totojestrasnedlouhejmenoadresaretotojestrasnedlouhejmenoadresaretotojestrasnedlouhejmenoadresare" +
                "totojestrasnedlouhejmenoadresaretotojestrasnedlouhejmenoadresare"),
                false);
            // non-authorized characters
            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/copydir/::::??????****<>"), false);
            // :::
            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/copydir/::::"), false);
            // ???
            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/copydir/??????"), false);
            // ***
            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/copydir/****"), false);
            // <><>
            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/copydir<><><><>"), false);
            // !!!
            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/copydir!!!!"), true);

            assert.equal(this.fileSystem.CreateDirectory(this.testDir + "/!@#$%^&*()_+}{\"?>MNBG$#"), false);
            assert.equal(this.fileSystem.CreateDirectory(null), false);
            assert.equal(this.fileSystem.CreateDirectory(""), false);
            this.fileSystem.Delete(this.testDir + "/copydir");
            this.fileSystem.Delete(this.testDir + "/copydir!!!!");
        }

        public testRename() : void {
            assert.equal(this.fileSystem.Rename("/test/resource/data/Com/Wui/Framework/Localhost/Connectors",
                "/test/resource/data/Com/Wui/Framework/Localhost/Connectors"), true);
            assert.equal(this.fileSystem.Rename(null, null), false);
            assert.equal(this.fileSystem.Rename(null, "/test/resource/data/Com/Wui/Framework/Localhost/Connectors"), false);
            assert.equal(this.fileSystem.Rename("/test/resource/data/Com/Wui/Framework/Localhost/Connectors", null), false);
            assert.equal(this.fileSystem.Rename(this.testDir + "/,;[o]/*-+.[=-012`", this.testDir + "/,;[o]/*-+.[=-012`"), false);
            assert.equal(this.fileSystem.Rename(this.testDir + "/rename.txt", this.testDir + "/ex.txt"), true);
            assert.equal(this.fileSystem.Rename(this.testDir + "/ex.txt", this.testDir + "/testDir/ex.txt"), false);
            assert.equal(this.fileSystem.Rename(this.getAbsoluteRoot() + "/testFile.txt", "some data"), false);
            assert.equal(this.fileSystem.Rename(this.testDir + "/ex.txt", this.testDir + "/ex"), true);
            assert.equal(this.fileSystem.Rename(this.testDir + "/ex", this.testDir + "/target.txt"), true);
            assert.equal(this.fileSystem.Rename(this.testDir + "/ex", ""), false);
            assert.equal(this.fileSystem.Rename(this.testDir + "", this.testDir + "/target.txt"), false);
            assert.equal(this.fileSystem.Rename(this.testDir + "", ""), false);
            this.fileSystem.Delete(this.testDir + "/target.txt");
        }

        public testRead() : void {
            assert.equal(this.fileSystem.Read(this.testDir + "/testCaseA.cmd").toString(),
                "@echo off\r\nfor /L %%A in (1,1,5) do (\r\n  echo \"testCaseA %%A\"\r\n  ping 1.0.0.1 -n 1 -w 1000 >nul\r\n)" +
                "\r\necho \"testCaseA exit\"");

            assert.equal(this.fileSystem.Read(this.testDir + "/testEmpty.txt").toString(), "");

            assert.patternEqual(this.fileSystem.Read(this.testDir + "/testbinFile").toString(),
                " \u0001\u0002\u0003\u0004* \u0005\u0006\u0007\b\r\n\u000b\f\r\n\u000e\u000f\u0010 \u0011\u0012\u0013\u0001\u0014" +
                "\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\" #$%&\'()*+ , -");

            assert.equal(this.fileSystem.Read(this.testDir + "").toString(), "");

            assert.equal(this.fileSystem.Read(""), "");
        }

        public testWrite() : void {
            assert.equal(this.fileSystem.Write(this.testDir + "/test.txt", "some test data", true), true);
            assert.equal(this.fileSystem.Delete(this.testDir + "/test.txt"), true);

            assert.equal(this.fileSystem.Write(this.testDir + "/test.txt", "some test data", false), true);
            assert.equal(this.fileSystem.Write(this.testDir + "/test.txt", "next test data", false), true);
            assert.equal(this.fileSystem.Read(this.testDir + "/test.txt"), "next test data");
            assert.equal(this.fileSystem.Delete(this.testDir + "/test.txt"), true);
            assert.equal(this.fileSystem.Write(this.testDir + "/test.txt", "some test data", true), true);
            assert.equal(this.fileSystem.Write(this.testDir + "/test.txt", "..next test data", true), true);
            assert.equal(this.fileSystem.Read(this.testDir + "/test.txt"), "some test data..next test data");
            assert.equal(this.fileSystem.Delete(this.testDir + "/test.txt"), true);
            assert.equal(this.fileSystem.Write(this.testDir + "", "try write", true), false);

            assert.deepEqual(this.fileSystem.Write("", "", true), false);
            assert.deepEqual(this.fileSystem.Write("", "new article", true), false);
        }

        public testDelete() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.CreateDirectory(this.testDir + "/deletedir");
                this.fileSystem.Copy(this.testDir + "/packTest", this.testDir + "/deletedir/subdir", () : void => {
                    assert.equal(this.fileSystem.Delete(this.testDir + "/deletedir/subdir/bin1.bin"), true);
                    assert.equal(this.fileSystem.Exists(this.testDir + "/deletedir/subdir/bin1.bin"), false);
                    let callbackCalled : boolean = false;
                    assert.equal(this.fileSystem.Delete(this.testDir + "/deletedir", ($status : boolean) : void => {
                        assert.equal($status, true);
                        callbackCalled = true;
                    }), true);
                    assert.equal(callbackCalled, true);
                    assert.equal(this.fileSystem.Exists(this.testDir + "/deletedir"), false);
                    assert.equal(this.fileSystem.Delete(""), false);
                    $done();
                });
            };
        }

        public testDeleteWithResponse() : IUnitTestRunnerPromise {
            // todo test this on directory with multiple sub-dirs, files and validate also OnChange
            return ($done : () => void) : void => {
                const response : MockResponse = new MockResponse();
                let onStartCalled : boolean = false;
                let onCompleteCalled : boolean = false;
                (<any>response).callbacks.onstart = ($data : any) : void => {
                    onStartCalled = true;
                };
                (<any>response).callbacks.oncomplete = ($data : any) : void => {
                    onCompleteCalled = true;
                    assert.equal($data, false);
                };
                assert.equal(this.fileSystem.Delete("", response), false);
                assert.equal(onStartCalled, true);
                assert.equal(onCompleteCalled, true);
                $done();
            };
        }

        public testCopy() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Copy(this.testDir + "/archTest1.zip", this.testDir + "/copiedArch.zip",
                    () : void => {
                        assert.ok(this.fileSystem.Exists(
                            this.testDir + "/copiedArch.zip"));
                        this.fileSystem.Delete("/test/resource/data/Com/Wui/Framework/Localhost/Connectors/copiedArch.zip");
                        $done();
                    });
            };
        }

        public __IgnoretestCopySecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Copy(this.testDir + "/copyFile", this.testDir + "/copiedFile",
                    () : void => {
                        assert.equal(this.fileSystem.Exists(this.testDir + "/copiedFile"), true);
                        this.fileSystem.Delete(this.testDir + "/copiedFile");
                        $done();
                    });
            };
        }

        public testCopyDirectory() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Copy(
                    this.testDir + "/archTest",
                    this.testDir + "/copiedDirectory",
                    ($status : boolean) : void => {
                        assert.equal($status, true);
                        assert.equal(this.fileSystem.Exists(this.testDir + "/copiedDirectory"), true);
                        assert.equal(this.fileSystem.Exists(this.testDir + "/copiedDirectory/doc1.txt"), true);
                        assert.equal(this.fileSystem.Exists(this.testDir + "/copiedDirectory/f1"), true);
                        this.fileSystem.Delete(this.testDir + "/copiedDirectory");
                        $done();
                    });
            };
        }

        public testCopyToExistingDirectory() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.CreateDirectory(this.testDir + "/copiedDirectory");
                this.fileSystem.Copy(this.testDir + "/archTest", this.testDir + "/copiedDirectory",
                    ($status : boolean) : void => {
                        assert.equal($status, true);
                        assert.equal(this.fileSystem.Exists(this.testDir + "/copiedDirectory"), true);
                        assert.equal(this.fileSystem.Exists(this.testDir + "/copiedDirectory/doc1.txt"), true);
                        assert.equal(this.fileSystem.Exists(this.testDir + "/copiedDirectory/f1"), true);
                        this.fileSystem.Delete(this.testDir + "/copiedDirectory");
                        $done();
                    });
            };
        }

        public testCopyFileToExistingFolder() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.CreateDirectory(this.testDir + "/copyToDirectory");
                this.fileSystem.Copy(this.testDir + "/testFile.txt",
                    this.testDir + "/copyToDirectory",
                    ($status : boolean) : void => {
                        assert.equal($status, true);
                        assert.equal(this.fileSystem.Exists(this.testDir + "/copyToDirectory/testFile.txt"), true);
                        this.fileSystem.Delete(this.testDir + "/copyToDirectory");
                        $done();
                    });
            };
        }

        public testCopyNull() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Copy(null, null, ($status : boolean) : void => {
                    assert.equal($status, false);
                    $done();
                });
            };
        }

        public testCopyEmpty() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Copy("", this.testDir + "/unknown", ($status : boolean) : void => {
                    assert.equal($status, false);
                    $done();
                });
            };
        }

        public testCopyToEmpty() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Copy(this.testDir + "/copyFile", "", ($status : boolean) : void => {
                    assert.equal($status, false);
                    $done();
                });
            };
        }

        public testCopyResponse() : IUnitTestRunnerPromise {
            // todo test this with some internal error/exception
            return ($done : () => void) : void => {
                const response : MockResponse = new MockResponse();
                (<any>response).callbacks.onsend = ($data : any) : void => {
                    assert.equal($data, false);
                    $done();
                };

                this.fileSystem.Copy("", this.testDir + "/archTest", response);
            };
        }

        public testDownloadFile() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const delayFileCheck : any = ($path : string, $callback : ($status : boolean) => void,
                                              $triesPending : number = 5) : void => {
                    const status : boolean = this.fileSystem.Exists($path);
                    if ($triesPending > 0 && !status) {
                        this.getEventsManager().FireAsynchronousMethod(() : void => {
                            delayFileCheck($path, $callback, $triesPending - 1);
                        }, 100);
                    } else {
                        $callback(status);
                    }
                };
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    headers: {
                        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0"
                    },
                    url    : this.getMockServerLocation() + "/com-wui-framework-localhost/README.md"
                }, ($headers : string, $bodyOrPath : string) : void => {
                    delayFileCheck($bodyOrPath, ($status : boolean) : void => {
                        assert.ok($status);
                        this.fileSystem.Delete($bodyOrPath);
                        this.initSendBox();
                        $done();
                    });
                });
            };
        }

        public testDownloadStreamOutput() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    streamOutput: true,
                    url         : this.getMockServerLocation() + "/com-wui-framework-localhost/README.md"
                }, ($headers : string, $bodyOrPath : string) : void => {
                    assert.equal($bodyOrPath, "hello");
                    this.initSendBox();
                    $done();
                });
            };
        }

        public testDownloadChunked() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    streamOutput: false,
                    url         : this.getMockServerLocation() +
                        "/git-for-windows/git/releases/download/v2.11.0.windows.1/Git-2.11.0-64-bit_1.exe"
                }, ($headers : string, $bodyOrPath : string) : void => {
                    assert.equal(StringUtils.Contains(JSON.stringify($headers), "chunked"), true);
                    assert.equal(this.fileSystem.Exists($bodyOrPath), true);
                    this.fileSystem.Delete($bodyOrPath);
                    $done();
                });
            };
        }

        public testDownloadUrl() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const url : string = this.getMockServerLocation() +
                    "/git-for-windows/git/releases/download/v2.13.1.windows.1/Git-2.13.1-32-bit.exe_2";
                this.fileSystem.Download(url, ($headers : string, $bodyOrPath : string) : void => {
                    assert.equal(this.fileSystem.Exists($bodyOrPath), true);
                    this.fileSystem.Delete($bodyOrPath);
                    $done();
                });
            };
        }

        public testDownload404() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const url : string = this.getMockServerLocation() + "/unknown-url";
                const response : MockResponse = new MockResponse();
                (<any>response).OnError = ($message : string | Error) : void => {
                    assert.equal($message, "Bad response status code: 404");
                    $done();
                };
                this.fileSystem.Download(url, response);
            };
        }

        public testDownloadCheckHeaders() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    streamOutput: true,
                    url         : this.getMockServerLocation() + "/boostorg/release/1.64.0/source/boost_1_64_0.zip"
                }, ($headers : string, $bodyOrPath : string) : void => {
                    assert.patternEqual(JSON.stringify($headers), JSON.stringify({
                        "date"             : "*",
                        "connection"       : "close",   // tslint:disable-line
                        "transfer-encoding": "chunked"
                    }));
                    assert.equal($bodyOrPath, "boost");
                    $done();
                });
            };
        }

        public testDownloadMethodGET() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    method: HttpMethodType.GET,
                    url   : this.getMockServerLocation() + "/wuiframework/com-wui-framework-localhost/resource/graphics/icon.ico"
                }, ($headers : string, $bodyOrPath : string) : void => {
                    assert.patternEqual(JSON.stringify($headers), JSON.stringify({
                        "date"             : "*",
                        "connection"       : "close",   // tslint:disable-line
                        "transfer-encoding": "chunked"
                    }));
                    assert.patternEqual($bodyOrPath, "C:/*AppData/Local/Temp/com-wui-framework-builder/downloads/icon*.ico");
                    assert.equal(this.fileSystem.Exists($bodyOrPath), true);
                    this.fileSystem.Delete($bodyOrPath);
                    $done();
                });
            };
        }

        public testDownloadObject() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    streamOutput: false,
                    url         : this.getMockServerLocation() +
                        "/webapps/download/AutoDL?BundleId=216431"
                }, ($headers : string, $bodyOrPath : string) : void => {
                    assert.patternEqual(JSON.stringify($headers),
                        JSON.stringify({"date": "*", "connection": "close", "transfer-encoding": "chunked"}));
                    assert.equal(this.fileSystem.Exists($bodyOrPath), true);
                    this.fileSystem.Delete($bodyOrPath);
                    $done();
                });
            };
        }

        public testDownloadHtml() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const url : string = this.getMockServerLocation() + "/";
                this.fileSystem.Download(url, ($headers : string, $bodyOrPath : string) : void => {
                    assert.equal(this.fileSystem.Exists($bodyOrPath), true);
                    this.fileSystem.Delete($bodyOrPath);
                    $done();
                });
            };
        }

        public testDownloadEmpty() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const url : string = this.getMockServerLocation() + "/webapp/Download?colCode=L4.1.30_MX8DV_BETA_GPU_TOOL";
                this.fileSystem.Download(url, ($headers : string, $bodyOrPath : string) : void => {
                    assert.patternEqual(JSON.stringify($headers),
                        JSON.stringify({"date": "*", "connection": "close", "transfer-encoding": "chunked"}));
                    assert.equal(this.fileSystem.Exists($bodyOrPath), true);
                    assert.equal(this.fileSystem.IsEmpty($bodyOrPath), true);
                    this.fileSystem.Delete($bodyOrPath);
                    $done();
                });
            };
        }

        public testDownloadMethodPOST() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    body   : "10",
                    headers: {
                        "Charset"       : "UTF-8",
                        "Content-Length": 20,
                        "Content-Type"  : "application/x-www-form-urlencoded; charset=UTF-8",
                        "User-Agent"    : "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" +
                            "Chrome/55.0.2883.87 Safari/537.36"
                    },
                    method : HttpMethodType.POST,
                    url    : this.getMockServerLocation() + "/xorigin/"
                }, ($headers : string, $bodyOrPath : string) : void => {
                    assert.equal(this.fileSystem.Exists($bodyOrPath), true);
                    this.fileSystem.Delete($bodyOrPath);
                    $done();
                });
            };
        }

        public testDownloadNullUrlThrowEx() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.throws(() : void => {
                    this.fileSystem.Download(<FileSystemDownloadOptions>{
                        url: null
                    });
                }, "The \"urlObject\" argument must be one of type Object or string.");
                $done();
            };
        }

        // todo validate download with proxy, mockServer may needs some update
        public __IgnoretestDownloadwithProxy() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    body   : "Don’t forget about the security.",
                    headers: {
                        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                            "Chrome/55.0.2883.87 Safari/537.36"
                    },
                    method : HttpMethodType.GET,
                    proxy  : true,
                    url    : this.getMockServerLocation() + "/eltima.com/1337x-proxy-unblocked.html"
                }, ($headers : string, $bodyOrPath : string) : void => {
                    assert.equal($headers, "[object Object]");
                    assert.equal(this.fileSystem.Exists($bodyOrPath), true);
                    this.fileSystem.Delete($bodyOrPath);
                    $done();
                });
            };
        }

        public __IgnoretestDownloadwithProxy407() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.throws(() : void => {
                    const url : string = this.getMockServerLocation() + "/stark";
                    this.fileSystem.Download(url, ($headers : string, $bodyOrPath : string) : void => {
                        assert.deepEqual($headers, "[object Object]");
                        assert.notEqual(this.fileSystem.Exists($bodyOrPath), true);
                        // this.fileSystem.Delete($bodyOrPath);
                        // $done();
                    });
                    throw new Error("The \"urlObject\" argument must be one of type Object or string.");
                }, "The \"urlObject\" argument must be one of type Object or string.");
                $done();
            };
        }

        public testDownloadStatus301() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const response : MockResponse = new MockResponse();
                (<any>response).OnError = ($message : string) : void => {
                    // todo better redirect evaluation should be used, this depends on url configured in MockServer config headers.location
                    assert.equal($message, "Selected protocol \"permanently-moved-url:\" is not supported. " +
                        "Please use http://, https:// or file:// protocol instead.");
                    $done();
                };
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    url: this.getMockServerLocation() + "/airbrake.io/blog/http-errors/301-moved-permanently"
                }, response);
            };
        }

        public testDownloadStatus302() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const response : MockResponse = new MockResponse();
                (<any>response).OnError = ($message : string) : void => {
                    // todo better redirect evaluation should be used, this depends on url configured in MockServer config headers.location
                    assert.equal($message, "Selected protocol \"permanent-redirect:\" is not supported. " +
                        "Please use http://, https:// or file:// protocol instead.");
                    $done();
                };
                this.fileSystem.Download(<FileSystemDownloadOptions>{
                    url: this.getMockServerLocation() + "/airbrake.io/blog/http-errors/302-found"
                }, response);
            };
        }

        public testAbortDownload() : void {
            const fileSystem : FileSystemHandler = new FileSystemHandler();
            let abortCalled : boolean = false;
            (<any>fileSystem).responseRegister.running[0] = {
                httpRequest: {
                    abort() {
                        abortCalled = true;
                    }
                }
            };
            assert.equal(fileSystem.AbortDownload(0), true);
            assert.equal((<any>fileSystem).responseRegister.abort.length, 1);
            assert.equal((<any>fileSystem).responseRegister.abort[0], "0");
            assert.equal((<any>fileSystem).responseRegister.running.hasOwnProperty(0), false);
            assert.equal(abortCalled, true);
            assert.equal(fileSystem.AbortDownload(404), false);
            assert.equal(fileSystem.AbortDownload(<number><any>"407"), false);
            assert.equal(fileSystem.AbortDownload(null), false);
        }

        public testAbortDownloadReal() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const filesystem : FileSystemHandler = new FileSystemHandler();
                const response : MockResponse = new MockResponse();
                let changeCount : number = 0;
                (<any>response).callbacks.onchange = ($data : any) : void => {
                    if (changeCount === 0) {
                        this.getEventsManager().FireAsynchronousMethod(() : void => {
                            filesystem.AbortDownload(response.getId());
                            assert.equal((<any>filesystem).responseRegister.abort.indexOf(0), -1);
                            assert.equal(changeCount > 1, true);
                            $done();
                        }, 100);
                    }
                    changeCount++;
                };

                filesystem.Download(<FileSystemDownloadOptions>{
                    streamOutput: true,
                    url         : this.getMockServerLocation() + "/big-data"
                }, response);
            };
        }

        public testNormalizePath() : void {
            assert.equal(this.fileSystem.NormalizePath(this.testDir + "/test.txt"), this.testDir + "/test.txt");
            assert.equal(this.fileSystem.NormalizePath(
                "test/resource/data/Com/Wui/Framework/Localhost/Connectors/test.txt"),
                "test/resource/data/Com/Wui/Framework/Localhost/Connectors/test.txt");
            assert.equal(
                this.fileSystem.NormalizePath(
                    this.getAbsoluteRoot() + "/test//resource\\data/Com/Wui/Framework/Localhost/Connectors/test.txt"),
                this.testDir + "/test.txt");
            assert.equal(this.fileSystem.NormalizePath(""), ".");
            if (EnvironmentHelper.IsWindows()) {
                assert.equal(this.fileSystem.NormalizePath("c:/some/path/to/file.txt", true),
                    "c:\\some\\path\\to\\file.txt");
            }
        }

        public testUnpackStripOff() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Unpack(
                    this.testDir + "/archTest1.zip", {
                        autoStrip: false
                    }, ($tmpPath : string) => {
                        const tempPath : string =
                            this.fileSystem.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName();
                        assert.equal(this.fileSystem.NormalizePath($tmpPath), this.fileSystem.NormalizePath(tempPath + "/archTest1"));
                        assert.ok(this.fileSystem.Exists($tmpPath));
                        assert.patternEqual(StringUtils.Replace(JSON.stringify(this.fileSystem.Expand($tmpPath + "/**/*")), "\\", "/"),
                            JSON.stringify([
                                tempPath + "/archTest1/archTest",
                                tempPath + "/archTest1/archTest/bin1.bin",
                                "*/archTest1/archTest/doc1.txt",
                                "*/archTest1/archTest/f1/f1bin1.bin",
                                "*/archTest1/archTest/f1/f1doc1.txt",
                                "*/archTest1/archTest/f2/f2bin1.bin",
                                "*/archTest1/archTest/f2/f2doc1.txt"
                            ]));
                        this.fileSystem.Delete($tmpPath);
                        $done();
                    });
            };
        }

        public testUnpackOutput() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Unpack(
                    this.testDir + "/archTest1.zip", {
                        autoStrip: false,
                        output   : this.testDir + "/archTest1"
                    }, ($tmpPath : string) => {
                        assert.equal(this.fileSystem.NormalizePath($tmpPath),
                            this.testDir + "/archTest1");
                        assert.ok(this.fileSystem.Exists($tmpPath));
                        assert.patternEqual(JSON.stringify(this.fileSystem.Expand($tmpPath + "/**/*")), JSON.stringify([
                            this.testDir + "/" +
                            "archTest1/archTest",
                            this.testDir + "/" +
                            "archTest1/archTest/bin1.bin",
                            "*/archTest1/archTest/doc1.txt",
                            "*/archTest1/archTest/f1/f1bin1.bin",
                            "*/archTest1/archTest/f1/f1doc1.txt",
                            "*/archTest1/archTest/f2/f2bin1.bin",
                            "*/archTest1/archTest/f2/f2doc1.txt"
                        ]));
                        this.fileSystem.Delete($tmpPath);
                        $done();
                    });
            };
        }

        public testUnpack() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.unpackSuite("archTestStrip", "zip", $done);
            };
        }

        public testUnpackTar() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.unpackSuite("archTestTar", "tar", $done);
            };
        }

        public testUnpackTarBz2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.unpackSuite("archTestBz2", "tar.bz2", $done);
            };
        }

        public testUnpackTarGz() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.unpackSuite("archTestGz", "tar.gz", $done);
            };
        }

        public __IgnoretestUnpackTarXz() : IUnitTestRunnerPromise {
            // TODO: untar is working as expected,
            //       but test is failing on delete of archTestXz folder as not empty directory from some reason
            return ($done : () => void) : void => {
                this.unpackSuite("archTestXz", "tar.xz", $done);
            };
        }

        public testPack() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Pack(
                    this.testDir + "/packTest",
                    null, ($tmpPath : string) => {
                        assert.ok(this.fileSystem.Exists($tmpPath));
                        this.fileSystem.Delete($tmpPath);
                        $done();
                    });
            };
        }

        public testPackOutput() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.packSuite("zip", $done);
            };
        }

        public testPackTar() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.packSuite("tar", $done);
            };
        }

        public testPackTarGz() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.packSuite("tar.gz", $done);
            };
        }

        public testPackTarBz() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.packSuite("tar.bz2", $done);
            };
        }

        public testExpand() : void {
            const input : string = this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Localhost/Connectors/packTest";
            assert.deepEqual(this.fileSystem.Expand(input + "/*"), [
                input + "/.ignore",
                input + "/bin with spaces.bin",
                input + "/bin1.bin",
                input + "/doc1.txt",
                input + "/f1",
                input + "/f2"
            ]);
        }

        protected before() : void {
            this.fileSystem = Loader.getInstance().getFileSystemHandler();
            this.testDir = this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Localhost/Connectors";
        }

        private unpackSuite($output : string, $type : string, $done : () => void) : void {
            const input : string = this.testDir + "/archTest1";
            const output : string = this.testDir + "/Connectors/" + $output;
            this.fileSystem.Unpack(input + "." + $type, {output}, ($tmpPath : string) => {
                assert.equal(this.fileSystem.NormalizePath($tmpPath), output);
                assert.ok(this.fileSystem.Exists($tmpPath));
                assert.deepEqual(this.fileSystem.Expand(output + "/**/*"), [
                    output + "/bin1.bin",
                    output + "/doc1.txt",
                    output + "/f1",
                    output + "/f1/f1bin1.bin",
                    output + "/f1/f1doc1.txt",
                    output + "/f2",
                    output + "/f2/f2bin1.bin",
                    output + "/f2/f2doc1.txt"
                ]);
                if (this.fileSystem.Delete($tmpPath)) {
                    $done();
                }
            });
        }

        private packSuite($type : string, $done : () => void) : void {
            const input : string = this.testDir + "/packTest";
            const output : string = this.testDir + "/packTestVal";
            this.fileSystem.Pack(input, {output: input + "." + $type}, ($packPath : string) => {
                assert.ok(this.fileSystem.Exists($packPath));
                this.fileSystem.Unpack($packPath, {output}, ($tmpPath : string) => {
                    assert.deepEqual(this.fileSystem.Expand(output + "/**/*"), [
                        output + "/.ignore",
                        output + "/bin with spaces.bin",
                        output + "/bin1.bin",
                        output + "/doc1.txt",
                        output + "/f1",
                        output + "/f1/f1bin1.bin",
                        output + "/f1/f1doc1.txt",
                        output + "/f2",
                        output + "/f2/f2bin1.bin",
                        output + "/f2/f2doc1.txt"
                    ]);
                    if (this.fileSystem.Delete($tmpPath) && this.fileSystem.Delete($packPath)) {
                        $done();
                    }
                });
            });
        }
    }
}
