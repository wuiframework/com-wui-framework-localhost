/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Connectors {
    "use strict";
    import TerminalOptions = Com.Wui.Framework.Services.Connectors.TerminalOptions;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;

    export class TerminalTest extends UnitTestRunner {
        private terminal : Terminal;

        constructor() {
            super();
            // this.setMethodFilter("testExecute");
        }

        public testExecute() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("ping", ["127.0.0.1"], null,
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 0);
                        assert.patternEqual($std.toString(),
                            "\r\nPinging 127.0.0.1 with 32 bytes of data:\r\nReply from 127.0.0.1: bytes=32 time* TTL=128\r\n*");
                        $done();
                    });
            };
        }

        public testExecuteWithEnv() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const options : TerminalOptions = {
                    env: {TEST_VAR_PATH: "my path"}
                };

                this.terminal.Execute("echo", ["hello"], options,
                    ($exitCode : number) : void => {
                        assert.equal($exitCode, 0);
                        assert.equal(JSON.stringify(options.env), JSON.stringify({TEST_VAR_PATH: "my path"}));
                        $done();
                    });
            };
        }

        public testExecuteNonExistingCommand() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("bla", [], null,
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 1);
                        assert.deepEqual($std, [
                            "",
                            "\'bla\' is not recognized as an internal or external command,\r\n" +
                            "operable program or batch file.\r\n"
                        ]);
                        $done();
                    });
            };
        }

        public testExecuteTestCaseA() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("testCaseA.cmd", [""],
                    this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Localhost/Connectors",
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 0);
                        assert.deepEqual($std, [
                            "\"testCaseA 1\"\r\n" +
                            "\"testCaseA 2\"\r\n" +
                            "\"testCaseA 3\"\r\n" +
                            "\"testCaseA 4\"\r\n" +
                            "\"testCaseA 5\"\r\n" +
                            "\"testCaseA exit\"\r\n",
                            ""
                        ]);
                        $done();
                    });
            };
        }

        public testExecuteErrorCwd() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("testCaseA.cmd", [""], null,
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 1);
                        assert.deepEqual($std, [
                            "",
                            "\'testCaseA.cmd\' is not recognized as an internal or external command,\r\n" +
                            "operable program or batch file.\r\n"
                        ]);
                        $done();
                    });
            };
        }

        public testExecuteEcho() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("echo", ["%PROCESSOR_ARCHITECTURE%"], null,
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 0);
                        assert.deepEqual($std, ["AMD64\r\n", ""]);
                        $done();
                    });
            };
        }

        public testLongStdData() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const cmd : string = "\"" + this.getAbsoluteRoot() +
                    "/test/resource/data/Com/Wui/Framework/Localhost/Connectors/stdio test.exe\"";
                const args : string[] = ["--stdout=9000", "--stdout-prefix=stdout_prefixed_", "--exit=111"];

                this.terminal.Execute(cmd, args, null, ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 3221225595);
                    assert.deepEqual($std, ["", ""]);
                    $done();
                });
            };
        }

        public testExecuteVerbose() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("echo", ["hello"], {
                    verbose: true
                }, ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 0);
                    assert.deepEqual($std, ["hello\r\n", ""]);
                    $done();
                });
            };
        }

        public testWithoutShell() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("echo", ["test"], {
                    shell: false
                }, ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 0);
                    assert.deepEqual($std, ["test\r\n", ""]);
                    $done();
                });
            };
        }

        public testNULL() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("", [], null, ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 0);
                    assert.deepEqual($std, ["", ""]);
                    $done();
                });
            };
        }

        public testExecuteProxy() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("echo", ["%HTTP_PROXY%"], {
                        env: {HTTP_PROXY: "http://user:pass@proxy.domain.org:3128/"}
                    },
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 0);
                        assert.deepEqual($std, ["http://user:pass@proxy.domain.org:3128/\r\n", ""]);
                        $done();
                    });
            };
        }

        public testExecuteResponse() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Execute("echo", ["test"], null,
                    ResponseFactory.getResponse(($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 0);
                        assert.deepEqual($std, ["test\r\n", ""]);
                        $done();
                    }));
            };
        }

        public testSpawn() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Spawn("ping", ["127.0.0.1"], null,
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 0);
                        assert.patternEqual($std.toString(),
                            "\r\nPinging 127.0.0.1 with 32 bytes of data:\r\nReply from 127.0.0.1: bytes=32 time* TTL=128\r\n*");
                        $done();
                    });
            };
        }

        public __IgnoretestElevate() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.terminal.Elevate("ping", ["localhost"], "", ($exitCode : number) : void => {
                    assert.equal($exitCode, 0);
                    $done();
                });
            };
        }

        public testKill() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const cmd = "C:/Windows/notepad.exe";
                setTimeout(() : void => {
                    this.terminal.Kill(cmd, ($status : boolean) : void => {
                        assert.ok($status);
                        $done();
                    });
                }, 200);
                this.terminal.Spawn(cmd, [], {
                    detached: true
                }, ($exitCode : number) : void => {
                    assert.equal($exitCode, 0);
                });
            };
        }

        public __IgnoretestOpen() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const cmd = this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Localhost/Connectors/testFile.txt";
                this.terminal.Open(cmd, ($exitCode : number) : void => {
                    assert.equal($exitCode, 0);
                    $done();
                });
            };
        }

        protected before() : void {
            this.terminal = Loader.getInstance().getTerminal();
        }
    }
}
