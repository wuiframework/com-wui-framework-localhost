/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Structures {
    "use strict";

    export class ProgramArgsTest extends UnitTestRunner {

        public testSimple() : void {
            const instance : ProgramArgs = new ProgramArgs();

            assert.deepEqual(instance.Normalize(["test", "test", "value"]), ["test", "test", "value"]);
            assert.deepEqual(instance.Normalize(["?"]), ["?"]);
            assert.deepEqual(instance.Normalize(["/v"]), ["/v"]);
            assert.deepEqual(instance.Normalize(["--path"]), ["--path"]);

            assert.deepEqual(instance.Normalize(["--option", "test test value"]), ["--option=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["--option", "\"test test value\""]), ["--option=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["--option='test test value'"]), ["--option=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["--option", "'test test value'"]), ["--option=\"test test value\""]);

            assert.deepEqual(instance.Normalize(["-o", "test test value"]), ["-o=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["-o", "\"test test value\""]), ["-o=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["-o='test test value'"]), ["-o=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["-o", "'test test value'"]), ["-o=\"test test value\""]);

            assert.deepEqual(instance.Normalize(["/o", "test test value"]), ["/o=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["/o", "\"test test value\""]), ["/o=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["/o='test test value'"]), ["/o=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["/o", "'test test value'"]), ["/o=\"test test value\""]);
        }

        public testComplex() : void {
            const instance : ProgramArgs = new ProgramArgs();
            assert.deepEqual(instance.Normalize([
                "C:\\com-wui-framework-localhost\\dependencies\\nodejs\\node.exe",
                "C:\\com-wui-framework-localhost\\build\\target\\resource\\javascript\\loader.min.js",
                "--target", "C:\\Users\\AppData\\Local\\WUI", "Framework\\WUI", "Test", "Localhost\\target",
                "--host-port", "-1",
                "-p", "-1",
                "/v",
                "--debug",
                "--option", "\"/test dir/ option\"",
                "--name", "test name",
                "--file=\"/test/unit/testFile.ts\""
            ]), [
                "C:\\com-wui-framework-localhost\\dependencies\\nodejs\\node.exe",
                "C:\\com-wui-framework-localhost\\build\\target\\resource\\javascript\\loader.min.js",
                "--target=\"C:\\Users\\AppData\\Local\\WUI Framework\\WUI Test Localhost\\target\"",
                "--host-port=-1",
                "-p=-1",
                "/v",
                "--debug",
                "--option=\"/test dir/ option\"",
                "--name=\"test name\"",
                "--file=\"/test/unit/testFile.ts\""
            ]);

            assert.deepEqual(instance.Normalize(["test", "test", "value"]), ["test", "test", "value"]);
            assert.deepEqual(instance.Normalize(["--option", "test test value"]), ["--option=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["--option", "\"test test value\""]), ["--option=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["--option='test test value'"]), ["--option=\"test test value\""]);
            assert.deepEqual(instance.Normalize(["--option", "'test test value'"]), ["--option=\"test test value\""]);
        }

        public testEmbed() : void {
            const instance : ProgramArgs = new ProgramArgs();
            assert.deepEqual(instance.Normalize([
                "C:\\com-wui-framework-localhost\\build\\target\\WuiLocalhost.exe",
                "/snapshot/resource/javascript/loader.min.js",
                "start",
                "--target", "C:\\Users\\AppData\\Local\\WUI", "Framework\\WUI", "Test", "Localhost\\target",
                "--host-port", "-1",
                "-p", "-1",
                "/v",
                "--debug",
                "--option", "\"/test dir/ option\"",
                "--name", "test name",
                "--file=\"/test/unit/testFile.ts\""
            ]), [
                "C:\\com-wui-framework-localhost\\build\\target\\WuiLocalhost.exe",
                "/snapshot/resource/javascript/loader.min.js",
                "start",
                "--target=\"C:\\Users\\AppData\\Local\\WUI Framework\\WUI Test Localhost\\target\"",
                "--host-port=-1",
                "-p=-1",
                "/v",
                "--debug",
                "--option=\"/test dir/ option\"",
                "--name=\"test name\"",
                "--file=\"/test/unit/testFile.ts\""
            ]);
        }
    }
}
