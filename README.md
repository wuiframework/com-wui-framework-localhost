# com-wui-framework-localhost v2019.1.0

> WUI Framework's code base for localhost based on Node.js.

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

## History

### v2019.1.0
Updates for node install script mainly focused on OSX. Fixed issues with elastic agent and greenlock initiation. 
Added support for external typedefs. Usage of decorators for security mechanisms and simplified external API declaration. 
Enable to use alternative origins. Fixed resolve of schema and origins over proxy.
### v2019.0.2
Bug fixes for singleton of elastic search agent. Enable to use WUI Localhost in role of runtime environment. 
Added full support for OSX. Usage of new app loader.
### v2019.0.1
Added robust support for cross compile of native libraries. Enable to filter file system logs. Added docker image configuration. 
Fixed creation of cerst under reverse proxy. Usage of hub location property. Enable to extend default.config. 
Enable to extend localhost to different namespace then Com. Added support for communication with native websocket chunks. 
Enable to send Errors with additional arguments.
### v2019.0.0
Fixed unpack in embedded mode on all platforms. Usage of websocket client from node module. Added CLI args normalization for easy parsing. 
Added support for letsencrypt certificates. Added ability to run localhost in role of reverse proxy. 
Added ability to send monitoring data to elastic APM. Optimization of RAM usage and stronger restrictions for request acceptance.
Fixed slash sensitive anonymization.  
### v2018.3.0
Added implementation for code base of WUI Connector, WUI Hub and WUI Builder.
### v2018.1.0
Refactoring to general code base focused on localhost functionality.
### v2.1.0
Conversion into the TypeScript and Node.js service.
### v2.0.2
Added fixed version for dependencies.
### v2.0.1
Added support for stand-alone toolchain. Bug fixes for user agent validation, reflection and cookies listing. Clean up of unit tests.
### v2.0.0
Namespaces refactoring.
### v1.0.1
Added PHP 7 syntax update.
### v1.0.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
 
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2010-2013 Jakub Cieslar,
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
