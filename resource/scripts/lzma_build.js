/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Com.Wui.Framework.Builder.Loader;
const LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
const ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
const EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
const StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
const DependenciesInstall = Com.Wui.Framework.Builder.Tasks.Composition.DependenciesInstall;
const OSType = Com.Wui.Framework.Builder.Enums.OSType;
const Patcher = Com.Wui.Framework.Builder.Utils.Patcher;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();

let cwd;

function finalize($done) {
    Patcher.getInstance().Rollback($done);
}

function fail($info) {
    finalize(() => {
        LogIt.Error($info);
    });
}

function validate($done) {
    let moduleNodePath = cwd;
    if (EnvironmentHelper.IsWindows()) {
        moduleNodePath += "/binding-v3.0.8-node-v64-win32-x64/lzma_native.node"
    } else if (DependenciesInstall.getCurrentInstallOsType() === OSType.LINUX) {
        moduleNodePath += "/binding-v3.0.8-node-v64-linux-x64/lzma_native.node"
    } else if (DependenciesInstall.getCurrentInstallOsType() === OSType.IMX) {
        moduleNodePath += "/binding_imx_aarch64/lzma_native.node";
    } else if (EnvironmentHelper.IsMac()) {
        moduleNodePath += "/binding-v3.0.8-node-v64-darwin-x64/lzma_native.node";
    } else {
        fail("Validate is not implemented for demand platform");
    }
    if (filesystem.Exists(moduleNodePath)) {
        CheckModule(moduleNodePath, ($status) => {
            $done($status);
        });
    } else {
        $done(false);
    }
}

function build($done) {
    if (EnvironmentHelper.IsWindows() || EnvironmentHelper.IsMac() || (DependenciesInstall.getCurrentInstallOsType() === OSType.LINUX)) {
        terminal.Spawn((EnvironmentHelper.IsMac() ? "CXX=clang++ " : "") + "npm", ["rebuild", "lzma-native", "--build-from-source=lzma-native"],
            {cwd: cwd + "/../../"}, ($exitCode) => {
                if ($exitCode === 0) {
                    $done();
                } else {
                    LogIt.Error("Lzma build failed");
                }
            });
    } else if (DependenciesInstall.getCurrentInstallOsType() === OSType.IMX) {
        const list = filesystem.Expand(cwd + "/binding-*");

        let index = 0;
        const removeFolders = function ($handler) {
            if (index < list.length) {
                filesystem.Delete(list[index++], () => {
                    removeFolders($handler);
                });
            } else {
                $handler();
            }
        };

        removeFolders(() => {
            const env = process.env;
            env.CC = "/usr/bin/aarch64-linux-gnu-gcc-7";
            env.CXX = "/usr/bin/aarch64-linux-gnu-g++-7";
            env.LINK = "/usr/bin/aarch64-linux-gnu-g++-7";
            env.RANLIB = "/usr/bin/aarch64-linux-gnu-ranlib";
            env.AR = "/usr/bin/aarch64-linux-gnu-ar";

            env.CC_host = "gcc";
            env.CXX_host = "g++";
            env.LINK_host = "g++";
            env.RANLIB_host = "ranlib";
            env.AR_host = "ar";

            env.module_path = cwd;

            Patcher.getInstance().Patch(cwd + "/liblzma-config.sh", "--disable-rpath", "--disable-rpath --host=aarch64", () => {
                terminal.Spawn(EnvironmentHelper.getNodejsRoot() + "/node_modules/npm/bin/node-gyp-bin/node-gyp",
                    ["rebuild", "--dest-cpu=arm64", "--dest-os=linux", "--cross-compiling", "--target_arch=arm64",
                        "--module_path=" + cwd + "/binding_imx_aarch64", "--module_name=lzma_native"],
                    {cwd: cwd, env: env},
                    ($exitCode) => {
                        if ($exitCode === 0) {
                            finalize($done);
                        } else {
                            fail("Can not cross-compile lzma-native");
                        }
                    });
            });
        })
    } else {
        fail("Build is not implemented for demand platform");
    }
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    validate(($status) => {
        if ($status !== true) {
            LogIt.Info("Starting lzma-native build...");
            build($done);
        } else {
            LogIt.Info("Lzma-native is ready.");
            finalize($done);
        }
    })
};
