/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Com.Wui.Framework.Builder.Loader;
const LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
const ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
const StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
const EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
const StringReplaceType = Com.Wui.Framework.Builder.Enums.StringReplaceType;
const StringReplace = Com.Wui.Framework.Builder.Tasks.Utils.StringReplace;
const DependenciesInstall = Com.Wui.Framework.Builder.Tasks.Composition.DependenciesInstall;
const OSType = Com.Wui.Framework.Builder.Enums.OSType;
const Resources = Com.Wui.Framework.Services.DAO.Resources;
const ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();

const http = require("http");
let server;

let cwd;
let installPath;
let args;

function execute($args, $callback) {
    const env = process.env;

    if (DependenciesInstall.getCurrentInstallOsType() === OSType.IMX) {
        env.NODEJSRE_SKIP_LOADER = 1;
        env.NODEJSRE_NODE_PATH = installPath;

        const wuiExecute = function () {
            terminal.Spawn("wui", [EnvironmentHelper.getNodejsRoot() + "/node_modules/npm/bin/npm-cli.js"].concat($args), {
                cwd: installPath,
                env: env
            }, $callback);
        };

        if (!filesystem.Exists(cwd + "/builder_patch/node") && EnvironmentHelper.IsEmbedded()) {
            env.PATH = cwd + "/builder_patch:" + env.PATH;
            filesystem.CreateDirectory(cwd + "/builder_patch");
            terminal.Spawn("ln", ["-s", "/usr/bin/wui", cwd + "/builder_patch" + "/node"], cwd, () => {
                wuiExecute();
            });
        } else {
            env.PATH = EnvironmentHelper.getNodejsRoot() + ":" + env.PATH;
            wuiExecute();
        }
    } else {
        terminal.Spawn((EnvironmentHelper.IsMac() ? "CXX=clang++ " : "") + "npm", $args, {
            cwd: installPath,
            env: env
        }, $callback);
    }
}

function checkBinary($path, $callback) {
    LogIt.Info("Checking module binary compatibility at: " + $path);
    if (EnvironmentHelper.IsWindows()) {
        $callback(!StringUtils.Contains(filesystem.Read($path), "node.exe"));
    } else if (EnvironmentHelper.IsMac()) {
        terminal.Execute("lipo", [$path, "-verify_arch", "x86_64"], {
            verbose: false,
            advanced: {noTerminalLog: true, noExitOnStderr: true}
        }, ($exitCode, $std) => {
            if ($exitCode !== 0) {
                LogIt.Error("Can not validate " + $path + " binary format.\n" + [].concat($std));
            } else {
                $callback(true);
            }
        });
    } else {
        const transTable = {
            "linux": "X86-64",
            "imx": "AArch64"
        };
        terminal.Execute("readelf", ["-h", $path], {
            verbose: false,
            advanced: {noTerminalLog: true, noExitOnStderr: true}
        }, ($exitCode, $std) => {
            if ($exitCode !== 0) {
                LogIt.Error("Can not validate " + $path + " binary format.\n" + [].concat($std));
            } else {
                const lines = $std[0].split("\n");
                const line = lines[lines.findIndex((item) => {
                    return item.trim().startsWith("Machine");
                })];
                if (!StringUtils.ContainsIgnoreCase(line, transTable[DependenciesInstall.getCurrentInstallOsType()])) {
                    LogIt.Warning("Library format not matching expected " +
                        transTable[DependenciesInstall.getCurrentInstallOsType()] + "\n" + $path + " >> \n" + [].concat($std));
                    $callback(false)
                } else {
                    $callback(true);
                }
            }
        });
    }
}

function checkModule($modulePath, $callback) {
    let modules = $modulePath;
    if (ObjectValidator.IsString($modulePath)) {
        modules = [$modulePath];
    }

    let index = 0;
    let status = true, required = true;
    const processNext = () => {
        if (index < modules.length) {
            const module = modules[index++];
            checkBinary(module, ($status) => {
                LogIt.Info("Checking module by require...");
                if ((DependenciesInstall.getCurrentInstallOsType() === OSType.LINUX) || EnvironmentHelper.IsWindows() || EnvironmentHelper.IsMac()) {
                    const script = "\"try{require('" + filesystem.NormalizePath(module) +
                        "');console.log('succeed');}catch(e){console.log('failed');}\"";
                    terminal.Execute(
                        "node",
                        ["-e", script],
                        {cwd, verbose: false, advanced: {noTerminalLog: true, noExitOnStderr: true}},
                        ($exitCode, $std) => {
                            status &= $status;
                            required &= $exitCode === 0 && !StringUtils.Contains($std[1], "failed");
                            processNext();
                        });
                } else {
                    LogIt.Warning("Native module require is supported only for WIN, LINUX, and MAC host os. Skipping this check...");
                    status &= $status;
                    processNext();
                }
            });
        } else {
            $callback(status, required);
        }
    };

    processNext();
}

function validateNative($callback) {
    const list = filesystem.Expand(cwd + "/build/node_modules/**/*.node");
    let log = "\n[binary-format, require(\"<module-path>\")]   <module-path>\n";
    let isCorrupted = false;

    let index = 0;
    const processNext = function () {
        if (index < list.length) {
            const item = list[index++];
            if (!StringUtils.Contains(item, "fswin")) {
                checkModule(item, ($binaryFormat, $required) => {
                    log += "\n" + StringUtils.Format("[{0} {1}]   {2}", $binaryFormat ? "OK" : "NOK", $required ? "OK" : "NOK", item);
                    isCorrupted |= !$binaryFormat || !$required;
                    processNext();
                });
            } else {
                processNext();
            }
        } else {
            LogIt.Debug("{0}\n", log);
            $callback(!isCorrupted);
        }
    };

    processNext();
}

function validate($callback) {
    const packageConf = JSON.parse(filesystem.Read(installPath + "/package.json").toString());
    const required = [];
    const optional = [];
    let name;
    if (packageConf.hasOwnProperty("devDependencies")) {
        for (name in packageConf.devDependencies) {
            if (packageConf.devDependencies.hasOwnProperty(name)) {
                required.push(name);
            }
        }
    }
    if (packageConf.hasOwnProperty("optionalDependencies")) {
        for (name in packageConf.optionalDependencies) {
            if (packageConf.optionalDependencies.hasOwnProperty(name)) {
                optional.push(name);
            }
        }
    }
    execute(["list", "--depth=0"], ($exitCode, $std) => {
        let missing = [];
        required.forEach(($name) => {
            if (ObjectValidator.IsEmptyOrNull($std[0].match(new RegExp("-- .*" + $name + "@", "gm")))) {
                missing.push($name);
            }
        });
        if (missing.length === 0) {
            let missingOptional = [];
            optional.forEach(($name) => {
                if (ObjectValidator.IsEmptyOrNull($std[0].match(new RegExp("-- .*" + $name + "@", "gm")))) {
                    missingOptional.push($name);
                }
            });
            if (missingOptional.length !== 0) {
                LogIt.Info("> NodeJS instance is missing some of optional dependencies: " +
                    JSON.stringify(missingOptional));
            }
            validateNative($callback);
        } else {
            LogIt.Info("> NodeJS instance is missing some of dependencies: " + JSON.stringify(missing));
            $callback(false);
        }
    });
}

function prepare($callback) {
    filesystem.CreateDirectory(installPath);
    if (EnvironmentHelper.IsWindows()) {
        LogIt.Warning("\nPython and Microsoft Build Tools for C++ (or Visual Studio) has to be available and due to " +
            "possible problems with admin rights an internal installation is not part of this install script.\n" +
            "Check for proper environment settings is not explicitly defined while it is too complex, so if build result" +
            "is \"strange\" please check full npm log in dependencies/nodejs/build/patched-appdata/npm-cache/_logs.\n" +
            "For more info please check official documentation for node-gyp at https://www.npmjs.com/package/node-gyp\n");
        terminal.Spawn("wui", ["--", "npm", "config", "set", "python", "python"],
            {cwd: installPath},
            () => {
                terminal.Spawn("wui", ["--", "npm", "config", "set", "msvs_version", "2017"],
                    {cwd: installPath},
                    $callback)
            });
    } else {
        $callback();
    }
}

function invokeModuleBuild($scriptObj, $callback) {
    if ($scriptObj.hasOwnProperty("name") && $scriptObj.hasOwnProperty("path")) {
        const vm = require("vm");
        const envBackup = JSON.stringify(process.env);
        const sandbox = {
            process,
            require,
            console,
            module: {
                exports: null
            },
            Com,
            CheckModule($path, $done) {
                checkModule($path, $done)
            },
            Process($cwd, $args, $done) {
                LogIt.Warning("Custom build skipped: script did not specify module.exports and " +
                    "also did not override global Process method");
                $done();
            }
        };
        let script = Loader.getInstance().getAppProperties().projectBase + "/resource/scripts/" + $scriptObj.name;
        if (!filesystem.Exists(script)) {
            script = filesystem.Expand(Loader.getInstance().getAppProperties().dependenciesResources + "/scripts/" + $scriptObj.name);
            if (!ObjectValidator.IsEmptyOrNull(script)) {
                script = script[0];
            }
        }
        const modulePath = installPath + "/node_modules/" + $scriptObj.path;
        let attributes = [];
        if ($scriptObj.hasOwnProperty("attributes")) {
            attributes = $scriptObj.attributes;
        }
        if (filesystem.Exists(modulePath)) {
            LogIt.Info(">> Running build-script " + script + " in " + modulePath);
            vm.createContext(sandbox);
            vm.runInContext(filesystem.Read(script), sandbox, script);
            sandbox.Process(modulePath, attributes, () => {
                process.env = JSON.parse(envBackup);
                $callback();
            });
        } else {
            LogIt.Warning("Skipping build-script for \"" + modulePath + "\". Module has not been installed.");
            $callback();
        }
    } else {
        LogIt.Error("Wants to run build-script without name or path option. Both has to be specified. " +
            "Please check your nodejs dependency configuration in \"install-script\":{\"attributes\":[...]}");
    }
}

function createPackageConf($path) {
    const projectBase = Loader.getInstance().getAppProperties().projectBase;
    const updatePath = ($path, $cwd) => {
        if (StringUtils.StartsWith($path, "resource/") &&
            !filesystem.Exists(projectBase + "/" + $path) &&
            filesystem.Exists($cwd + "/" + $path)) {
            $path = $cwd + "/" + $path;
        }
        return $path;
    };
    const preparePkg = ($config) => {
        let pkg = {};
        try {
            if (ObjectValidator.IsEmptyOrNull($config.pkg)) {
                $config.pkg = {};
            } else {
                pkg = JSON.parse(JSON.stringify($config.pkg));
            }
            if (ObjectValidator.IsEmptyOrNull(pkg)) {
                pkg = {assets: [], patches: []};
            } else {
                if (ObjectValidator.IsEmptyOrNull(pkg.assets)) {
                    pkg.assets = [];
                }
                if (ObjectValidator.IsEmptyOrNull(pkg.patches)) {
                    pkg.patches = [];
                }
            }
        } catch (ex) {
            LogIt.Debug($config.pkg);
            LogIt.Error(ex);
        }
        return pkg;
    };

    const configs = [];
    const traverse = ($path) => {
        let config = JSON.parse(filesystem.Read($path).toString());
        configs.push(config);
        if (config.hasOwnProperty("extends")) {
            traverse(config.extends);
        }
    };
    traverse($path);
    configs.reverse();

    const output = JSON.parse(JSON.stringify(configs[0]));
    const assets = [];
    const patches = [];
    const registered = new ArrayList();
    const mergeAssets = ($config) => {
        $config.assets.forEach(($asset) => {
            if (assets.indexOf($asset) === -1) {
                assets.push($asset);
            }
        });
    };
    const mergePatches = ($config) => {
        $config.patches.forEach(($patch) => {
            registered.Add({patch: $patch, cwd: $config.cwd}, StringUtils.getCrc(JSON.stringify($patch)));
        });
    };
    if (configs.length === 1) {
        const parentPkg = preparePkg(configs[0]);
        mergeAssets(parentPkg);
        mergePatches(parentPkg);
    } else if (configs.length > 1) {
        for (let $index = 1; $index < configs.length; $index++) {
            const child = configs[$index];
            const parent = configs[$index - 1];
            const childPkg = preparePkg(child);
            const parentPkg = preparePkg(parent);

            const depKey = "dependencies/";
            const depIndex = StringUtils.IndexOf(child.extends, depKey) + depKey.length;
            const cwd = StringUtils.Substring(child.extends, 0,
                StringUtils.IndexOf(child.extends, "/", true, depIndex));
            childPkg.cwd = cwd;
            parentPkg.cwd = cwd;

            Resources.Extend(output, child);
            mergeAssets(parentPkg);
            mergeAssets(childPkg);
            mergePatches(parentPkg);
            mergePatches(childPkg);
        }
    }
    registered.foreach(($patch) => {
        if (!ObjectValidator.IsEmptyOrNull($patch.patch.src)) {
            $patch.patch.src = updatePath($patch.patch.src, $patch.cwd);
        }
        if (!ObjectValidator.IsEmptyOrNull($patch.patch.script)) {
            $patch.patch.script = updatePath($patch.patch.script, $patch.cwd);
        }
        patches.push($patch.patch);
    });
    output.pkg = {assets, patches};

    return StringReplace.Content(JSON.stringify(output, null, 2), StringReplaceType.VARIABLES);
}

function install($callback) {
    filesystem.Write(installPath + "/package.json", createPackageConf(args[0]));
    filesystem.Delete(installPath + "/package-lock.json");

    validate(($status) => {
        if ($status) {
            LogIt.Info("Install skipped: NodeJS is up-to-date");
            validateNative($callback);
        } else {
            execute(["install"], ($exitCode) => {
                if ($exitCode === 0) {
                    if (args.length > 1) {
                        LogIt.Info("Customized module build preparation...");
                        let index = 1;
                        const buildModule = function () {
                            if (index < args.length) {
                                invokeModuleBuild(args[index++], buildModule);
                            } else {
                                validateNative($callback);
                            }
                        };

                        buildModule();
                    } else {
                        LogIt.Info("No script for customized module build is defined.");
                        validateNative($callback);
                    }
                } else {
                    LogIt.Error("NodeJS install has failed");
                }
            });
        }
    });
}

function getSha($path) {
    const sha = require("crypto").createHash("sha256");
    sha.update(filesystem.Read($path));
    return sha.digest("hex");
}

function startMockServer($callback) {
    let tarPath = "";
    let shasumList = "";
    server = http.createServer();
    server.on("request", ($request, $response) => {
        LogIt.Info("request.url: " + $request.url);

        if ($request.url === "/node.lib") {
            $response.writeHead(200);
            const readStream = require("fs").createReadStream(cwd + "/node.lib");
            readStream.pipe($response);
        } else if ($request.url === "/node-v10.15.0-headers.tar.gz") {
            $response.writeHead(200);
            const readStream = require("fs").createReadStream(tarPath);
            readStream.pipe($response);
        } else if ($request.url === "/SHASUMS256.txt") {
            $response.writeHead(200);
            $response.end(shasumList);
        } else {
            $response.writeHead(404);
            $response.end();
        }
    });
    server.listen(0, () => {
        // initialize mock server
        process.env.NODEJSRE_MOCK_SERVER_MAP = JSON.stringify({
            "https://nodejs.org/download/release/v10.15.0/win-x64/node.lib": "http://127.0.0.1:" +
                server.address().port + "/node.lib",
            "https://nodejs.org/dist/v10.15.0/win-x64/node.lib": "http://127.0.0.1:" +
                server.address().port + "/node.lib",

            "https://nodejs.org/download/release/v10.15.0/node-v10.15.0-headers.tar.gz": "http://127.0.0.1:" +
                server.address().port + "/node-v10.15.0-headers.tar.gz",
            "https://nodejs.org/dist/v10.15.0/node-v10.15.0-headers.tar.gz": "http://127.0.0.1:" +
                server.address().port + "/node-v10.15.0-headers.tar.gz",

            "https://nodejs.org/download/release/v10.15.0/SHASUMS256.txt": "http://127.0.0.1:" +
                server.address().port + "/SHASUMS256.txt",
            "https://nodejs.org/dist/v10.15.0/SHASUMS256.txt": "http://127.0.0.1:" +
                server.address().port + "/SHASUMS256.txt"
        });

        const dirPath = filesystem.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() + "/downloads";
        filesystem.Copy(cwd + "/include", dirPath + "/node-v10.15.0/include/node", ($status) => {
            if ($status) {
                filesystem.Pack(dirPath + "/node-v10.15.0", {type: "tar.gz"}, ($outputPath) => {
                    filesystem.Delete(dirPath, () => {
                        tarPath = $outputPath;
                        shasumList = StringUtils.Format("" +
                            "ede58d70f22373d7abfd71582ad3c93ec8f6a8b59efd7aa8e207d7cb30481010 win-x86/node.lib\n" +  // official distr.
                            "{0} win-x64/node.lib\n" +
                            "{1} node-v10.15.0-headers.tar.gz\n", getSha(cwd + "/node.lib"), getSha(tarPath));
                        $callback();
                    });
                });
            } else {
                LogIt.Error("Can not prepare data for tarball");
            }
        });
    });
}

function stopMockServer($callback) {
    LogIt.Info("Stop nodejs-install mock server");
    server.close($callback);
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    installPath = $cwd + "/build";
    args = [];
    const envBackup = JSON.parse(JSON.stringify(process.env));
    process.env.PATH = terminal.NormalizeEnvironmentPath(cwd);
    process.env.URSA_VERBOSE = true;
    process.env.SKIP_SASS_BINARY_DOWNLOAD_FOR_CI = true;

    process.env[(EnvironmentHelper.IsWindows()) ? "USERPROFILE" : "HOME"] = cwd + "/build/patched-home";
    if (EnvironmentHelper.IsWindows()) {
        process.env["APPDATA"] = cwd + "/build/patched-appdata";
    }

    // reorder args list, expect only SINGLE string at the top
    let counter = 0;
    $args.forEach(($item) => {
        if (ObjectValidator.IsString($item)) {
            if (counter++ < 1) {
                args.unshift($item);
            } else {
                LogIt.Error("Found more than one string in argument list. Please check your nodejs dependency configuration.\n" +
                    ">> " + JSON.stringify($args));
            }
        } else {
            if (args.findIndex(($value) => {
                return JSON.stringify($value) === JSON.stringify($item);
            }) === -1) {
                args.push($item);
            }
        }
    });

    const finalize = function () {
        stopMockServer(() => {
            process.env = envBackup;
            $done();
        })
    };
    startMockServer(() => {
        if (!filesystem.Exists(installPath)) {
            prepare(() => {
                install(finalize);
            });
        } else {
            install(finalize);
        }
    });
};
