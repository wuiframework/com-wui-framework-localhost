/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

var ajaxConnector = null;
var serviceConsumer = null;
var dataBuffer = [];

try {
    ajaxConnector = new XMLHttpRequest();
} catch (ex1) {
    try {
        ajaxConnector = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (ex2) {
        try {
            ajaxConnector = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (ex3) {
        }
    }
}

function sendDataToServer($data, $success, $error, $processing) {
    try {
        if ($data !== "" && $data[0] === "{" && $data[$data.length - 1] === "}") {
            ajaxConnector.open("POST", "<? @var xservices.request ?>");
            ajaxConnector.onreadystatechange = function () {
                try {
                    if (ajaxConnector.readyState === 4) {
                        $success();
                    } else if ($processing !== undefined) {
                        $processing();
                    }
                } catch (ex) {
                    console.log("Data transfer failure: " + ex.message);
                }
            };
            ajaxConnector.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            ajaxConnector.setRequestHeader("charset", "UTF-8");
            ajaxConnector.send("jsonData=" + $data);
        } else {
            console.log("Data transfer skipped: incorrect JSON format detected. \"" + $data + "\"");
        }
    } catch (ex) {
        if ($error !== undefined) {
            $error(ex);
        } else {
            console.log("Data transfer failure: " + ex.message);
        }
    }
}

function sendDataToClient($message) {
    if (serviceConsumer !== null) {
        try {
            var postMessage = function ($data) {
                serviceConsumer.postMessage($data, "*");
            };
            if (dataBuffer.length > 0) {
                for (var index = 0; index < dataBuffer.length; index++) {
                    postMessage(dataBuffer[index]);
                }
                dataBuffer = [];
            }
            postMessage($message);
        } catch (ex) {
            console.log("message send failure: " + ex.message);
        }
    } else {
        dataBuffer.push($message);
    }
}

function createResponse($message) {
    return "{\"type\":\"Server.Exception\",\"status\":500,\"origin\":\"<? @var xservices.origin ?>\",\"id\":-1,\"data\":{\"message\":\"" + $message + "\"}}";
}

function receiveDataFromClient($event) {
    try {
        serviceConsumer = $event.source;
        if (ajaxConnector !== null) {
            sendDataToServer($event.data, function () {
                var response;
                if (ajaxConnector.status === 200 || ajaxConnector.status === 500) {
                    response = ajaxConnector.responseText;
                } else if (ajaxConnector.status === 0) {
                    response = "{\"type\":\"Server.Timeout\",\"status\":500,\"origin\":\"<? @var xservices.origin ?>\",\"id\":-1,\"data\":\"Connection has been lost.\"}";
                } else {
                    response = createResponse("Data transfer failure with status " + ajaxConnector.status + ": " + ajaxConnector.statusText);
                }
                sendDataToClient(response);
            }, function (ex) {
                sendDataToClient(createResponse("Data transfer failure: " + ex.message + ex.stack));
            });
        } else {
            sendDataToClient(createResponse("Unable to transfer data due to missing XMLHttpRequest utility."));
        }
    } catch (ex) {
        sendDataToClient(createResponse("Data receive failure: " + ex.message));
    }
}

function windowErrorHandler($event) {
    try {
        sendDataToClient(createResponse($event.message + " At: " +
            $event.filename + " " +
            $event.lineno + ":" +
            $event.colno + " For more info see the console log."));
    } catch (ex) {
        console.log("Global error handler failure: " + ex.message);
    }
}

if (window.addEventListener) {
    window.onmessage = receiveDataFromClient;
    window.addEventListener("error", windowErrorHandler, false);
} else if (window.attachEvent) {
    window.attachEvent("onmessage", receiveDataFromClient);
    window.onerror = windowErrorHandler;
}
