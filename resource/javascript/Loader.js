/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
var isBrowser = true;
try {
    if (require) {
        isBrowser = false;
    }
    if (window) {
        isBrowser = true;
    }
} catch (ex) {
}
if (!isBrowser) {
    var fs = require("fs");
    var path = require("path");
    var nodejsPath = path.normalize(path.dirname(process.execPath)).replace(/\\/g, "/");
    try {
        if (process.env.NODE_PATH === undefined || process.env.NODE_PATH === "" || !fs.existsSync(process.env.NODE_PATH)) {
            process.env.NODE_PATH = path.normalize(path.dirname(process.argv0) + "/build/node_modules").replace(/\\/g, "/");
        }
        if (!fs.existsSync(process.env.NODE_PATH)) {
            console.error("Unable to find NODE_PATH at \"" + process.env.NODE_PATH + "\", please specify existing folder.");
            process.exit(1);
        }

        var sourceFile = path.join(__dirname, "<? @var properties.packageName ?>.min.js");
        var sourceMap = sourceFile + ".map";
        var content = fs.readFileSync(sourceFile).toString().replace(/^var\s?\S*;$/gm, "");
        if (!fs.existsSync(sourceMap)) {
            if (process.argv.indexOf("--debug") !== -1) {
                console.log("Map file \"" + sourceMap + "\" not found. Running without stacktrace remap support.");
            }
        } else {
            var contentMap = JSON.parse(fs.readFileSync(sourceMap).toString());
            require(process.env.NODE_PATH + "/source-map-support").install({
                environment: "node",
                handleUncaughtExceptions: true,
                retrieveSourceMap: function ($source) {
                    if ($source === sourceFile || $source.indexOf("evalmachine") !== -1) {
                        return {
                            map: contentMap,
                            url: sourceMap
                        };
                    }
                    return null;
                }
            });
        }
        process.nodejsRoot = nodejsPath;

        var sandbox = {
            require: function ($id) {
                try {
                    return require($id);
                } catch (e) {
                    return require(process.env.NODE_PATH + "/" + $id);
                }
            },
            process: process,
            setTimeout: setTimeout,
            setInterval: setInterval,
            clearTimeout: clearTimeout,
            clearInterval: clearInterval,
            Buffer: Buffer,
            Error: Error,
            ErrorEvent: function () {
            },
            console: console,
            __filename: __filename,
            __dirname: __dirname,
            global: {},
            window: {},
            document: {},
            localStorage: {},
            WebSocket: {},
            nodejsRoot: nodejsPath,
            JsonpData: function () {
            }
        };
        sandbox.console.clear = function () {
        };

        var namespaces = "<? @var serverConfig.namespaces ?>".split(",");
        for (var index = 0; index < namespaces.length; index++) {
            var rootPart = namespaces[index].split(".")[0];
            if (!sandbox.hasOwnProperty(rootPart)) {
                sandbox[rootPart] = {};
                sandbox.global[rootPart] = sandbox[rootPart];
                sandbox.window[rootPart] = sandbox[rootPart];
            }
        }

        var vm = require("vm");
        vm.createContext(sandbox);
        content += "\n" +
            "Com.Wui.Framework.Commons.Primitives.String = Com.Wui.Framework.Commons.Utils.StringUtils;\n" +
            "<? @var serverConfig.loaderClass ?>.Load(<? @var serverConfig ?>);";
        vm.runInContext(content, sandbox, {filename: sourceFile, displayErrors: true});
    } catch (ex) {
        console.log("Package \"<? @var properties.packageName ?>\" has not been loaded.");
        console.error(ex.stack);
        fs.writeFileSync(nodejsPath + "/error.stack.log", ex.stack);
        process.exit(1);
    }
} else {
    /* jshint ignore:start */
    <!-- @import dependencies/com-wui-framework-commons/resource/javascript/Loader.js -->
    /* jshint ignore:end */
}
