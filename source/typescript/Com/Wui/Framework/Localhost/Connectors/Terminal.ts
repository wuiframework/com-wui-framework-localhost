/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StdinType = Com.Wui.Framework.Localhost.Enums.StdinType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import TerminalOptions = Com.Wui.Framework.Services.Connectors.TerminalOptions;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;

    export class Terminal extends BaseConnector {

        public Execute($cmd : string, $args : string[], $cwdOrOptions : string | TerminalOptions,
                       $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
            const exec : Types.NodeJS.Modules.child_process.exec = require("child_process").exec;
            const path : Types.NodeJS.path = require("path");
            const fs : Types.NodeJS.fs = require("fs");
            const response : IResponse = ResponseFactory.getResponse($callback);
            let cwd : string = <string>$cwdOrOptions;
            const cmdOptions : TerminalOptions = <any>{
                advanced   : {
                    colored       : false,
                    noExitOnStderr: false,
                    noTerminalLog : false
                },
                shell      : true,
                verbose    : true,
                windowsHide: true
            };
            if (!ObjectValidator.IsSet(cwd) || ObjectValidator.IsEmptyOrNull(cwd)) {
                cwd = process.cwd();
            } else if (ObjectValidator.IsObject($cwdOrOptions)) {
                const options : TerminalOptions = <TerminalOptions>$cwdOrOptions;
                if (ObjectValidator.IsSet(options.cwd)) {
                    cwd = options.cwd;
                } else {
                    cwd = process.cwd();
                }
                if (ObjectValidator.IsSet(options.env)) {
                    cmdOptions.env = options.env;
                }
                if (ObjectValidator.IsSet(options.verbose)) {
                    cmdOptions.verbose = options.verbose;
                }
                if (ObjectValidator.IsSet(options.detached)) {
                    LogIt.Warning("Detached option is not supported by Execute method use Spawn instead.");
                }
                if (ObjectValidator.IsSet(options.shell)) {
                    cmdOptions.shell = options.shell;
                }
                if (ObjectValidator.IsSet(options.advanced)) {
                    if (ObjectValidator.IsSet(options.advanced.colored)) {
                        cmdOptions.advanced.colored = options.advanced.colored;
                    }
                    if (ObjectValidator.IsSet(options.advanced.noTerminalLog)) {
                        cmdOptions.advanced.noTerminalLog = options.advanced.noTerminalLog;
                    }
                    if (ObjectValidator.IsSet(options.advanced.noExitOnStderr)) {
                        cmdOptions.advanced.noExitOnStderr = options.advanced.noExitOnStderr;
                    }
                }
            }
            if (cmdOptions.hasOwnProperty("env") && cmdOptions.env.hasOwnProperty("PATH")) {
                cmdOptions.env.PATH = this.NormalizeEnvironmentPath(cmdOptions.env.PATH);
            }
            if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getAppConfiguration().httpProxy)) {
                if (!ObjectValidator.IsSet(cmdOptions.env)) {
                    cmdOptions.env = process.env;
                }
                cmdOptions.env.HTTP_PROXY = process.env.HTTP_PROXY;
                cmdOptions.env.http_proxy = process.env.http_proxy;
                cmdOptions.env.HTTPS_PROXY = process.env.HTTPS_PROXY;
                cmdOptions.env.https_proxy = process.env.https_proxy;
            }

            if (EnvironmentHelper.IsDetached() || cmdOptions.detached) {
                if (!ObjectValidator.IsSet(cmdOptions.env)) {
                    cmdOptions.env = process.env;
                }
                cmdOptions.env.IS_WUI_DETACHED_PROCESS = 1;
            }

            if (!ObjectValidator.IsSet($args) || ObjectValidator.IsEmptyOrNull($args)) {
                $args = [];
            }
            if (cmdOptions.verbose) {
                if (!cmdOptions.advanced.noTerminalLog) {
                    LogIt.Info(cwd + " > " + $cmd + " " + $args.join(" "));
                }
            } else {
                LogIt.Info("Execute process in silent mode at: " + cwd);
            }

            let counter : number = 0;
            const progress : any = setInterval(() : void => {
                if (counter < 100) {
                    if (cmdOptions.verbose && !cmdOptions.advanced.noTerminalLog) {
                        Echo.Print(".");
                    }
                } else {
                    if (cmdOptions.verbose && !cmdOptions.advanced.noTerminalLog) {
                        Echo.Println(".");
                    }
                    counter = 0;
                }
                response.OnChange(".");
                counter++;
            }, 250);

            let exitCode : number = -1;
            const onExitHandler : any = ($exitCode : number, $stdout : string, $stderr : string) : void => {
                clearInterval(progress);
                $stdout = this.normalizeStdout($stdout, cmdOptions.advanced.colored);
                $stderr = this.normalizeStdout($stderr, cmdOptions.advanced.colored);
                if (cmdOptions.verbose) {
                    Echo.Println("");
                    LogIt.Info($stdout + $stderr);
                }
                if (!cmdOptions.advanced.noTerminalLog) {
                    LogIt.Info("exit code: " + $exitCode);
                }
                try {
                    response.OnComplete(exitCode, [$stdout, $stderr]);
                } catch (ex) {
                    LogIt.Info(ex.stack);
                }
            };

            response.OnStart();
            try {
                cmdOptions.cwd = cwd.replace(/[/\\]/g, path.sep);

                let tmpCmd : string = path.normalize(StringUtils.Remove($cmd, "\""));
                if (fs.existsSync(tmpCmd)) {
                    if (this.checkWuiApp(tmpCmd)) {
                        $args.push("--disable-console");
                    }
                } else {
                    tmpCmd = path.normalize(cmdOptions.cwd + "/" + tmpCmd);
                    if (fs.existsSync(tmpCmd)) {
                        if (this.checkWuiApp(tmpCmd)) {
                            $args.push("--disable-console");
                        }
                    }
                }
                const child : Types.NodeJS.Modules.child_process.ChildProcess = exec($cmd + " " + $args.join(" "), <any>cmdOptions,
                    ($error : Error, $stdout : string, $stderr : string) : void => {
                        onExitHandler(exitCode, $stdout, $stderr);
                    });
                child.on("exit", ($exitCode : number) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($exitCode)) {
                        exitCode = $exitCode;
                    }
                });
                response.AddAbortHandler(() : void => {
                    const process : string = cmdOptions.verbose ? ($cmd + " " + $args.join(" ")) : "";
                    LogIt.Warning(cmdOptions.cwd + ">" + process + " Process kill requested by abort.");
                    child.kill("SIGINT");
                });
                return child.pid;
            } catch (ex) {
                onExitHandler(-1, "", ex.message);
                LogIt.Info(ex.stack);
            }
            return -1;
        }

        public Spawn($cmd : string, $args : string[], $cwdOrOptions : string | TerminalOptions,
                     $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
            const spawn : Types.NodeJS.Modules.child_process.spawn = require("child_process").spawn;
            const pathNormalize : Types.NodeJS.Modules.path.normalize = require("path").normalize;
            const fs : Types.NodeJS.fs = require("fs");
            const response : IResponse = ResponseFactory.getResponse($callback);
            const cmdOptions : TerminalOptions = <any>{
                advanced   : {
                    colored       : false,
                    noExitOnStderr: false,
                    noTerminalLog : false,
                    useStdIn      : false
                },
                detached   : false,
                shell      : true,
                verbose    : true,
                windowsHide: true
            };
            let cwd : string = <string>$cwdOrOptions;
            const colored : boolean = false;
            if (!ObjectValidator.IsSet($cwdOrOptions) || ObjectValidator.IsEmptyOrNull($cwdOrOptions)) {
                cwd = process.cwd();
            } else if (ObjectValidator.IsObject($cwdOrOptions)) {
                const options : TerminalOptions = <TerminalOptions>$cwdOrOptions;
                if (ObjectValidator.IsSet(options.cwd)) {
                    cwd = options.cwd;
                } else {
                    cwd = process.cwd();
                }
                if (ObjectValidator.IsSet(options.env)) {
                    cmdOptions.env = options.env;
                }
                if (ObjectValidator.IsSet(options.verbose)) {
                    cmdOptions.verbose = options.verbose;
                }
                if (ObjectValidator.IsSet(options.detached)) {
                    cmdOptions.detached = options.detached;
                }
                if (ObjectValidator.IsSet(options.shell)) {
                    cmdOptions.shell = options.shell;
                }
                if (ObjectValidator.IsSet(options.advanced)) {
                    if (ObjectValidator.IsSet(options.advanced.colored)) {
                        cmdOptions.advanced.colored = options.advanced.colored;
                    }
                    if (ObjectValidator.IsSet(options.advanced.noTerminalLog)) {
                        cmdOptions.advanced.noTerminalLog = options.advanced.noTerminalLog;
                    }
                    if (ObjectValidator.IsSet(options.advanced.noExitOnStderr)) {
                        cmdOptions.advanced.noExitOnStderr = options.advanced.noExitOnStderr;
                    }
                    if (ObjectValidator.IsSet(options.advanced.useStdIn)) {
                        cmdOptions.advanced.useStdIn = options.advanced.useStdIn;
                    }
                }
            }
            cmdOptions.cwd = cwd;
            if (cmdOptions.hasOwnProperty("env") && cmdOptions.env.hasOwnProperty("PATH")) {
                cmdOptions.env.PATH = this.NormalizeEnvironmentPath(cmdOptions.env.PATH);
            }
            if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getAppConfiguration().httpProxy)) {
                if (!ObjectValidator.IsSet(cmdOptions.env)) {
                    cmdOptions.env = process.env;
                }
                cmdOptions.env.HTTP_PROXY = process.env.HTTP_PROXY;
                cmdOptions.env.http_proxy = process.env.http_proxy;
                cmdOptions.env.HTTPS_PROXY = process.env.HTTPS_PROXY;
                cmdOptions.env.https_proxy = process.env.https_proxy;
            }

            if (!ObjectValidator.IsSet($args) || ObjectValidator.IsEmptyOrNull($args)) {
                $args = [];
            }
            if (cmdOptions.verbose) {
                if (!cmdOptions.advanced.noTerminalLog) {
                    LogIt.Info(cwd + " > " + $cmd + " " + $args.join(" "));
                }
            } else {
                LogIt.Info("Spawn process in silent mode at: " + cwd);
            }

            let fired : boolean = false;
            let stdout : string = "";
            let stderr : string = "";
            const onExitHandler : any = ($exitCode : number) : void => {
                if (!fired) {
                    fired = true;
                    try {
                        if (!cmdOptions.advanced.noTerminalLog) {
                            LogIt.Info("exit code: " + $exitCode);
                        }
                        response.OnComplete($exitCode, [stdout, stderr]);
                    } catch (ex) {
                        LogIt.Info(ex.stack);
                    }
                }
            };
            const onErrorHandler : any = ($error : Error) : void => {
                stderr += $error.message;
                LogIt.Info((<any>$error).stack);
                onExitHandler(-1);
            };
            try {
                let tmpCmd : string = pathNormalize(StringUtils.Remove($cmd, "\""));
                if (fs.existsSync(tmpCmd)) {
                    if (this.checkWuiApp(tmpCmd)) {
                        $args.push("--disable-console");
                    }
                } else {
                    tmpCmd = pathNormalize(cmdOptions.cwd + "/" + tmpCmd);
                    if (fs.existsSync(tmpCmd)) {
                        if (this.checkWuiApp(tmpCmd)) {
                            $args.push("--disable-console");
                        }
                    }
                }
                if (cmdOptions.detached) {
                    (<any>cmdOptions).stdio = ["pipe", "pipe", "pipe", "ipc"];
                } else if (cmdOptions.advanced.useStdIn) {
                    (<any>cmdOptions).stdio = ["inherit", "pipe", "pipe"];
                }
                if (EnvironmentHelper.IsDetached() || cmdOptions.detached) {
                    if (!ObjectValidator.IsSet(cmdOptions.env)) {
                        cmdOptions.env = process.env;
                    }
                    cmdOptions.env.IS_WUI_DETACHED_PROCESS = 1;
                }
                response.OnStart();

                const child : Types.NodeJS.Modules.child_process.ChildProcess = spawn($cmd, $args, <any>cmdOptions);
                child.on("exit", onExitHandler);
                child.on("close", onExitHandler);
                child.on("error", onErrorHandler);
                child.stdout.on("data", ($data : string) : void => {
                    $data = this.normalizeStdout($data, colored);
                    if ($data.indexOf(StdinType.STRING) !== -1 || $data.indexOf(StdinType.PASS) !== -1) {
                        response.OnChange($data, child.stdin);
                    } else {
                        stdout += $data;
                        if (cmdOptions.verbose) {
                            Echo.Print($data);
                        }
                        response.OnChange($data);
                    }
                });
                child.stderr.on("data", ($data : string) : void => {
                    $data = this.normalizeStdout($data, colored);
                    stderr += $data;
                    if (cmdOptions.verbose) {
                        Echo.Print($data);
                    }
                    response.OnChange($data);
                    if (cmdOptions.advanced.noExitOnStderr) {
                        if ($data.toLowerCase().indexOf("fatal:") !== -1 ||
                            $data.toLowerCase().indexOf("error:") !== -1) {
                            onExitHandler(-1);
                        }
                    }
                });
                if (cmdOptions.detached) {
                    child.on("message", ($data : string) : void => {
                        $data = this.normalizeStdout($data, colored);
                        stdout += $data;
                        if (cmdOptions.verbose) {
                            Echo.Print($data);
                        }
                        response.OnChange($data);
                    });
                }
                response.AddAbortHandler(() : void => {
                    const process : string = cmdOptions.verbose ? ($cmd + " " + $args.join(" ")) : "";
                    LogIt.Warning(cmdOptions.cwd + ">" + process + " Process kill requested by abort.");
                    child.kill("SIGINT");
                });
                return child.pid;
            } catch (ex) {
                onErrorHandler(ex);
            }
            return -1;
        }

        public Open($path : string, $callback? : (($exitCode : number) => void) | IResponse) : number {
            const exec : Types.NodeJS.Modules.child_process.exec = require("child_process").exec;
            const onOpenHandler : any = ($error : number | Error, $data : string) : void => {
                if ($error !== null) {
                    LogIt.Info(<any>$error);
                }
                LogIt.Info($data.toString());
                ResponseFactory.getResponse($callback).Send($error === null ? 0 : -1);
            };
            LogIt.Info("open > " + $path);
            if (ObjectValidator.IsEmptyOrNull($path)) {
                onOpenHandler(-1, "Undefined or empty path for open.");
            } else {
                return exec(FileSystemHandler.getInstance<FileSystemHandler>()
                    .NormalizePath($path, true), {windowsHide: true}, onOpenHandler).pid;
            }
            return -1;
        }

        public Elevate($cmd : string, $args : string[], $cwd : string,
                       $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
            const pathSeparator : string = require("path").sep;
            let cmd : string;
            let args : string[];
            if (EnvironmentHelper.IsWindows()) {
                cmd = "powershell.exe";
                args = [
                    "-Command \"(Start-Process " +
                    "-WindowStyle hidden '" + $cmd + "' " +
                    (ObjectValidator.IsEmptyOrNull($args) ? "" : "-ArgumentList '" + $args.join(" ") + "' ") +
                    "-Verb RunAs " +
                    "-PassThru).Id\""
                ];
            } else {
                cmd = "sudo";
                args = [$cmd];
                if (!ObjectValidator.IsEmptyOrNull($args)) {
                    $args.forEach(($arg : string) : void => {
                        args.push($arg);
                    });
                }
            }
            if (ObjectValidator.IsEmptyOrNull($cwd)) {
                cmd = "cd \"" + $cwd.replace(/\//gi, pathSeparator) + "\" && " + cmd;
            }
            return this.Execute(cmd, args, null, ($exitCode : number, $std : string[]) : void => {
                ResponseFactory.getResponse($callback).Send($exitCode, $std);
            });
        }

        public NormalizeEnvironmentPath($input : string) : string {
            const envSeparator = EnvironmentHelper.IsWindows() ? ";" : ":";
            if (EnvironmentHelper.IsWindows()) {
                $input = StringUtils.Replace($input, "/", "\\");
            } else {
                $input = StringUtils.Replace($input, "\\", "/");
            }
            if (!StringUtils.EndsWith($input, envSeparator)) {
                $input += envSeparator;
            }
            let systemPath : string = process.env.PATH;
            $input = StringUtils.Replace($input, envSeparator + envSeparator, envSeparator);
            StringUtils.Split($input, envSeparator).forEach(($path : string) : void => {
                if ($path !== "") {
                    systemPath = StringUtils.Replace(systemPath, $path + envSeparator, "");
                }
            });
            if (!StringUtils.EndsWith($input, envSeparator)) {
                $input += envSeparator;
            }
            return StringUtils.Replace($input + systemPath, envSeparator + envSeparator, envSeparator);
        }

        public Kill($path : string, $callback? : (($status : boolean) => void) | IResponse) : void {
            const path : Types.NodeJS.path = require("path");
            const response : IResponse = ResponseFactory.getResponse($callback);
            $path = StringUtils.Replace($path, "\\", "/");
            $path = StringUtils.Replace($path, "/", path.sep);
            if (EnvironmentHelper.IsWindows()) {
                this.Execute("wmic", [
                    "process", "where name=\"" + path.basename($path) + "\" get processid, executablepath | MORE +1"
                ], {
                    verbose: false
                }, ($exitCode : number, $std : string[]) : void => {
                    if ($exitCode === 0) {
                        const outList : string = (<any>$std[0]).trim();
                        if (!ObjectValidator.IsEmptyOrNull(outList)) {
                            const entries : string[] = StringUtils.Split(StringUtils.Remove(outList, "\r"), "\n");
                            let status : boolean = true;
                            entries.forEach(($item : any) : void => {
                                if (!ObjectValidator.IsEmptyOrNull($item)) {
                                    const parts : any[] = $item.trim().replace(/\s+/g, "*").split("*");
                                    if (parts.length >= 2 && StringUtils.ContainsIgnoreCase(parts[0], $path)) {
                                        try {
                                            process.kill(StringUtils.ToInteger(parts[1]));
                                        } catch (ex) {
                                            status = false;
                                            LogIt.Warning("Failed to kill process with PID " + parts[1] + ": " + ex.message);
                                        }
                                    }
                                }
                            });
                            response.Send(status);
                        } else {
                            response.Send(true);
                        }
                    } else {
                        LogIt.Warning($std[0] + $std[1]);
                        response.Send(false);
                    }
                });
            } else {
                let pids : string[] = [];
                let status : boolean = true;
                const killProcess : any = ($index : number) : void => {
                    if ($index < pids.length) {
                        if (!ObjectValidator.IsEmptyOrNull(pids[$index])) {
                            this.Execute("ls", ["-l", "/proc/" + pids[$index] + "/exe"], {
                                verbose: false
                            }, ($exitCode : number, $std : string[]) : void => {
                                if ($exitCode === 0) {
                                    const processInfo : string = (<any>$std[0]).trim();
                                    if (!ObjectValidator.IsEmptyOrNull(processInfo)) {
                                        const parts : string[] = StringUtils.Split(processInfo, " -> ");
                                        if (parts.length >= 2 && StringUtils.ContainsIgnoreCase(parts[1], $path)) {
                                            try {
                                                process.kill(StringUtils.ToInteger(pids[$index]));
                                            } catch (ex) {
                                                LogIt.Warning("Failed to kill process with PID " + pids[$index] + ": " + ex.message);
                                                status = false;
                                            }
                                        }
                                    }
                                } else {
                                    LogIt.Warning($std[0] + $std[1]);
                                    status = false;
                                }
                                killProcess($index + 1);
                            });
                        } else {
                            killProcess($index + 1);
                        }
                    } else {
                        response.Send(status);
                    }
                };
                this.Execute("pgrep", ["\"" + path.basename($path) + "\""], {
                    verbose: false
                }, ($exitCode : number, $std : string[]) : void => {
                    if ($exitCode === 0 || $exitCode === 1 && ObjectValidator.IsEmptyOrNull($std[0] + $std[1])) {
                        const outList : string = (<any>$std[0]).trim();
                        if (!ObjectValidator.IsEmptyOrNull(outList)) {
                            pids = StringUtils.Split(outList, "\n");
                            killProcess(0);
                        } else {
                            response.Send(true);
                        }
                    } else {
                        LogIt.Warning($std[0] + $std[1]);
                        response.Send(false);
                    }
                });
            }
        }

        private normalizeStdout($value : string, $colorized : boolean = false) : string {
            const os : Types.NodeJS.os = require("os");
            $value = Buffer.from($value + "").toString("utf8")
                .replace(/█/gm, "[__BAR__]")
                .replace(/▇/gm, "[__BAR__]")
                .replace(/✓/gm, "[__OK__]")
                .replace(/×/gm, "[__FAIL__]")
                .replace(/→/gm, "[__ARROW__]")
                .replace(/\r\n/gm, "\n")
                .replace(/\n/gm, "[__EOL__]");
            if (!$colorized) {
                try {
                    $value = $value.replace(
                        /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, "");
                } catch ($ex) {
                    // let std as is
                }
                $value = $value.replace(/[^ -~]+/g, "");
            }
            $value = $value.replace(/\[__EOL__\]/gm, os.EOL);
            if ($value.match(/(\(.*\d+\/\d+\))?.*\[[#-]+\].*%/gm) !== null) {
                $value = $value.replace(/%/gm, "%" + os.EOL);
            }
            return $value;
        }

        private checkWuiApp($path : string) : boolean {
            let status : boolean = false;
            if (EnvironmentHelper.IsWindows()) {
                try {
                    const filePath : string = require("path").normalize(StringUtils.Remove($path, "\""));
                    if (require("fs").existsSync(filePath)) {
                        const verInfo : any = require("win-version-info")(filePath);
                        if (!ObjectValidator.IsEmptyOrNull(verInfo) &&
                            verInfo.hasOwnProperty("BaseProjectName") &&
                            StringUtils.StartsWith(verInfo.BaseProjectName, "com-wui-framework-")) {
                            LogIt.Info("Executing command has been found as WUI Framework application.");
                            status = true;
                        }
                    }
                } catch (ex) {
                    // Can not read version info. Executable will be processed as default.
                }
            } else {
                this.checkWuiApp = ($path : string) : boolean => {
                    return false;
                };
            }
            return status;
        }
    }
}
