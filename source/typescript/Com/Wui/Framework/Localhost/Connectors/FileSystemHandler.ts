/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import ArchiveOptions = Com.Wui.Framework.Services.Connectors.ArchiveOptions;
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import Resources = Com.Wui.Framework.Services.DAO.Resources;
    import LogSeverity = Com.Wui.Framework.Commons.Enums.LogSeverity;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;

    export class FileSystemHandler extends BaseConnector {
        private responseRegister : ResponsesRegister;
        private terminal : Terminal;

        constructor() {
            super();
            this.responseRegister = <ResponsesRegister>{
                abort  : [],
                running: {}
            };
            this.terminal = Loader.getInstance().getTerminal();
        }

        public Exists($path : string) : boolean {
            const status : boolean = this.exists($path);
            LogIt.Info("> validate path \"" + $path + "\" existence: " + status, LogSeverity.MEDIUM);
            return status;
        }

        public IsEmpty($path : string) : boolean {
            const fs : Types.NodeJS.fs = require("fs");
            LogIt.Info("> validate if \"" + $path + "\" is empty", LogSeverity.LOW);
            let status : boolean = true;
            if (!ObjectValidator.IsEmptyOrNull($path)) {
                try {
                    if (fs.existsSync($path)) {
                        if (fs.statSync($path).isDirectory()) {
                            status = this.Expand($path + "/*").length === 0;
                        } else {
                            status = fs.readFileSync($path).length === 0;
                        }
                    }
                } catch (ex) {
                    LogIt.Warning(ex.message);
                }
            }
            return status;
        }

        public IsFile($path : string) : boolean {
            return this.exists($path) && require("fs").lstatSync($path).isFile();
        }

        public IsDirectory($path : string) : boolean {
            return this.exists($path) && require("fs").lstatSync($path).isDirectory();
        }

        public IsSymbolicLink($path : string) : boolean {
            return this.exists($path) && require("fs").lstatSync($path).isSymbolicLink();
        }

        public getTempPath() : string {
            let value : string;
            if (EnvironmentHelper.IsWindows()) {
                const spawnSync : Types.NodeJS.Modules.child_process.spawnSync = require("child_process").spawnSync;
                let retVal : Types.NodeJS.Modules.child_process.SpawnSyncReturns<Buffer> = spawnSync(
                    "reg query \"HKCU\\Environment\" /v \"TEMP\"", {
                        shell      : true,
                        windowsHide: true
                    });
                if (retVal.status === 0) {
                    let regValue : string = retVal.stdout.toString();
                    regValue = StringUtils.Substring(regValue, StringUtils.IndexOf(regValue, "REG_EXPAND_SZ") + 13).trim();
                    if (!ObjectValidator.IsEmptyOrNull(regValue)) {
                        retVal = spawnSync(
                            "echo " + regValue, {
                                shell      : true,
                                windowsHide: true
                            });
                        if (retVal.status === 0) {
                            regValue = retVal.stdout.toString().trim();
                            if (!ObjectValidator.IsEmptyOrNull(regValue)) {
                                if (require("fs").existsSync(regValue)) {
                                    value = this.NormalizePath(regValue);
                                    this.getTempPath = () : string => {
                                        return value;
                                    };
                                }
                            }
                        }
                    }
                } else {
                    LogIt.Warning("> " + retVal.stdout + retVal.stderr);
                }
            }
            if (ObjectValidator.IsEmptyOrNull(value)) {
                const os : Types.NodeJS.os = require("os");
                value = os.tmpdir();
            }
            return value;
        }

        public Expand($pattern : string | string[]) : string[] {
            LogIt.Info("> search for pattern \"" + $pattern + "\"");
            if (ObjectValidator.IsString(<string>$pattern)) {
                $pattern = [<string>$pattern];
            }
            const glob : any = require("glob");
            let output : string[] = [];
            const input : string[] = [];
            const ignore : string[] = [];
            (<string[]>$pattern).forEach(($pattern : string) : void => {
                StringUtils.StartsWith($pattern, "!") ? ignore.push(StringUtils.Substring($pattern, 1)) : input.push($pattern);
            });
            let cwd : string = Loader.getInstance().getProgramArgs().TargetBase();
            if (cwd !== process.cwd()) {
                cwd = this.NormalizePath(process.cwd());
            }
            input.forEach(($pattern : string) : void => {
                $pattern = StringUtils.Replace($pattern, "\\", "/");
                try {
                    output = output.concat(glob.sync($pattern, {
                        cwd,
                        dot: true,
                        ignore
                    }));
                } catch (ex) {
                    LogIt.Warning("> " + ex.message);
                }
            });
            return output;
        }

        public CreateDirectory($path : string) : boolean {
            LogIt.Info("> create directory at \"" + $path + "\"", LogSeverity.MEDIUM);
            let status : boolean = false;
            if (!ObjectValidator.IsEmptyOrNull($path)) {
                try {
                    this.pmkdir($path);
                    status = true;
                } catch (ex) {
                    LogIt.Warning(ex.message);
                }
            }
            return status;
        }

        public Rename($oldPath : string, $newPath : string) : boolean {
            const fs : Types.NodeJS.fs = require("fs");
            LogIt.Info("> rename from \"" + $oldPath + "\" to \"" + $newPath + "\"");
            let status : boolean = false;
            try {
                if (this.exists($oldPath) && !ObjectValidator.IsEmptyOrNull($newPath)) {
                    fs.renameSync($oldPath, $newPath);
                    status = true;
                }
            } catch (ex) {
                LogIt.Warning(ex.message);
            }
            return status;
        }

        public Read($path : string) : string | Buffer {
            const fs : Types.NodeJS.fs = require("fs");
            let data : string | Buffer = "";

            if (!ObjectValidator.IsEmptyOrNull($path)) {
                $path = this.NormalizePath($path);
                if (!StringUtils.StartsWith($path, "/snapshot/") && (
                    (EnvironmentHelper.IsWindows() && !StringUtils.Contains($path, ":/")) ||
                    (!EnvironmentHelper.IsWindows() && ($path[0] !== "/")))) {
                    let cwd : string = Loader.getInstance().getProgramArgs().TargetBase();
                    if (cwd !== process.cwd()) {
                        cwd = this.NormalizePath(process.cwd());
                    }
                    $path = cwd + "/" + $path;
                }
                $path = this.NormalizePath($path);

                LogIt.Info("> read content from \"" + $path + "\"", LogSeverity.MEDIUM);
                try {
                    if (fs.existsSync($path)) {
                        data = fs.readFileSync($path);
                    }
                } catch (ex) {
                    LogIt.Warning(ex.message);
                }
            }
            return data;
        }

        public Write($path : string, $data : any, $append : boolean = false) : boolean {
            let status : boolean = false;

            if (!ObjectValidator.IsEmptyOrNull($path)) {
                $path = this.NormalizePath($path);

                LogIt.Info("> write content to \"" + $path + "\"", LogSeverity.MEDIUM);
                try {
                    this.pmkdir(StringUtils.Substring($path, 0, StringUtils.IndexOf($path, "/", false)));
                    if ($append) {
                        require("fs").appendFileSync($path, $data);
                    } else {
                        require("fs").writeFileSync($path, $data);
                    }
                    status = true;
                } catch (ex) {
                    LogIt.Warning(ex.message);
                }
            }
            return status;
        }

        public Copy($sourcePath : string, $destinationPath : string, $callback? : (($status : boolean) => void) | IResponse) : void {
            const fs : Types.NodeJS.fs = require("fs");
            const path : Types.NodeJS.path = require("path");
            const response : IResponse = ResponseFactory.getResponse($callback);
            const onComplete : any = ($status : boolean) : void => {
                response.Send($status);
            };
            try {
                if (this.exists($sourcePath) && !ObjectValidator.IsEmptyOrNull($destinationPath)) {
                    const fileStats : any = fs.lstatSync($sourcePath);
                    if (fileStats.isFile() || fileStats.isSymbolicLink()) {
                        LogIt.Info("> copy file from \"" + $sourcePath + "\" to \"" + $destinationPath + "\"", LogSeverity.MEDIUM);
                        // tslint:disable no-bitwise
                        const permissions : number = fs.lstatSync($sourcePath).mode & parseInt("777", 8);
                        // tslint:enable no-bitwise
                        this.pmkdir(path.dirname($destinationPath));
                        if (this.IsDirectory($destinationPath)) {
                            $destinationPath = $destinationPath + "/" + path.basename($sourcePath);
                        }
                        const write : any = fs.createWriteStream($destinationPath, {mode: permissions});
                        write.on("close", () : void => {
                            onComplete(true);
                        });
                        write.on("error", ($error : Error) : void => {
                            response.OnError($error);
                        });
                        fs.createReadStream($sourcePath).pipe(write);
                    } else {
                        let cmd : string;
                        let args : string[];
                        $sourcePath = this.NormalizePath($sourcePath, true);
                        $destinationPath = this.NormalizePath($destinationPath, true);
                        if (EnvironmentHelper.IsWindows()) {
                            cmd = "Robocopy";
                            args = ["\"" + $sourcePath + "\"", "\"" + $destinationPath + "\"", "/E", "/MT", "/NP", "/NFL"];
                        } else {
                            cmd = "cp";
                            args = ["-Rp", "\"" + $sourcePath + "\"/*", "\"" + $destinationPath + "\""];
                            this.pmkdir($destinationPath);
                        }
                        this.terminal.Spawn(cmd, args, "", ($exitCode : number) : void => {
                            if (EnvironmentHelper.IsWindows()) {
                                onComplete($exitCode === 1 || $exitCode === 3);
                            } else {
                                onComplete($exitCode === 0);
                            }
                        });
                    }
                } else {
                    LogIt.Info("> copy from \"" + $sourcePath + "\" to \"" + $destinationPath +
                        "\" skipped: source does not exist or destination is empty");
                    onComplete(false);
                }
            } catch (ex) {
                response.OnError(ex);
            }
        }

        public Download($urlOrOptions : string | FileSystemDownloadOptions,
                        $callback? : (($headers : string, $bodyOrPath : string) => void) | IResponse) : void {
            const path : Types.NodeJS.path = require("path");
            const fs : Types.NodeJS.fs = require("fs");
            const url : Types.NodeJS.url = require("url");
            const http : Types.NodeJS.http = require("http");
            const https : Types.NodeJS.https = require("https");
            const options : any = {
                body               : "",
                headers            : {},
                maxReconnection    : 5,
                method             : "GET",
                proxy              : false,
                streamOutput       : false,
                streamReturnsBuffer: false,
                url                : ""
            };
            if (ObjectValidator.IsString($urlOrOptions)) {
                options.url = $urlOrOptions;
            } else if (ObjectValidator.IsObject($urlOrOptions)) {
                let parameter : string;
                for (parameter in <any>$urlOrOptions) {
                    if ($urlOrOptions.hasOwnProperty(parameter)) {
                        options[parameter] = $urlOrOptions[parameter];
                    }
                }
            }
            if (ObjectValidator.IsString(options.url)) {
                options.url = url.parse(options.url);
                options.url.method = options.method;
                options.url.headers = options.headers;
            }
            const response : IResponse = ResponseFactory.getResponse($callback);
            const prepareTmp : any = ($fileName : string) : string => {
                let tmpPath : string = this.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() + "/downloads";
                this.pmkdir(tmpPath);
                if (!ObjectValidator.IsEmptyOrNull($fileName)) {
                    tmpPath += "/" + $fileName;
                } else {
                    if (options.url.pathname.indexOf("/") !== -1) {
                        const name : string = options.url.pathname.substr(options.url.pathname.lastIndexOf("/"));
                        if (name !== "") {
                            tmpPath += "/" + name;
                        } else {
                            tmpPath += "/" + new Date().getTime();
                        }
                    } else {
                        tmpPath += "/" + new Date().getTime();
                    }
                }
                tmpPath = this.NormalizePath(tmpPath);

                if (fs.existsSync(tmpPath)) {
                    let index : number = 2;
                    let pathExtension : string = path.extname(tmpPath);
                    const pathMatch : RegExpMatchArray = tmpPath.match(/.tar.[a-zA-Z0-9]+$/g);
                    if (pathMatch !== null && pathMatch.length >= 0 && pathMatch[0] !== undefined) {
                        pathExtension = pathMatch[0];
                    }

                    let newPath : string;
                    do {
                        if (pathExtension !== "") {
                            newPath = StringUtils.Replace(tmpPath, pathExtension, "_" + index + pathExtension);
                        } else {
                            newPath = tmpPath + "_" + index;
                        }
                        index++;
                    }
                    while (fs.existsSync(newPath));
                    tmpPath = newPath;
                }

                return tmpPath;
            };

            LogIt.Info("> download (" + options.method + ") from: " + url.format(options.url));
            const headers : string = JSON.stringify(options.url.headers);
            if (headers !== "{}") {
                LogIt.Info("> with headers: " + headers);
            }
            if (options.body !== "") {
                LogIt.Info("> with body: " + options.body);
            }

            if (options.url.protocol === "http:" || options.url.protocol === "https:") {
                let client : any = http;
                if (options.url.protocol === "https:") {
                    client = https;
                }
                const origOptions : any = JSON.parse(JSON.stringify(options));
                if (options.proxy && !ObjectValidator.IsEmptyOrNull(Loader.getInstance().getAppConfiguration().httpProxy)) {
                    const origUrl : any = options.url;
                    const proxy : any = url.parse(
                        StringUtils.Replace(Loader.getInstance().getAppConfiguration().httpProxy, "https://", "http://"));
                    client = http;
                    origUrl.headers.Host = origUrl.hostname;
                    proxy.path = origUrl.href;
                    proxy.headers = origUrl.headers;
                    proxy.method = origUrl.method;
                    options.url = proxy;
                }

                const httpRequest : any = client.request(options.url, ($httpResponse : any) : void => {
                    if ($httpResponse.statusCode === 200) {
                        const length : number = StringUtils.ToInteger($httpResponse.headers["content-length"]);
                        const isChunked : boolean = !ObjectValidator.IsEmptyOrNull($httpResponse.headers["transfer-encoding"]) &&
                            StringUtils.ContainsIgnoreCase($httpResponse.headers["transfer-encoding"], "chunked");
                        let currentLength : number = 0;
                        try {
                            let fileName : string = "";
                            let streamBuffers : any[] = [];
                            if (!options.streamOutput) {
                                const disposition : string = "content-disposition";
                                if ($httpResponse.headers.hasOwnProperty(disposition)) {
                                    const filenameKey : string = "filename=";
                                    const dispositionValue : string = $httpResponse.headers[disposition];
                                    if (StringUtils.Contains(dispositionValue, "attachment;") &&
                                        StringUtils.Contains(dispositionValue, filenameKey)) {
                                        const parts : string[] = StringUtils.Split(dispositionValue, ";");
                                        parts.forEach(($part : string) : void => {
                                            if (StringUtils.Contains($part, filenameKey) && ObjectValidator.IsEmptyOrNull(fileName)) {
                                                $part = $part.trim();
                                                $part = StringUtils.Remove($part, filenameKey);
                                                $part = StringUtils.Remove($part, "\"");
                                                fileName = ObjectDecoder.Url($part.trim());
                                            }
                                        });
                                    }
                                }
                                fileName = prepareTmp(fileName);
                                const fsStream : any = fs.createWriteStream(fileName);
                                $httpResponse.pipe(fsStream);
                            }

                            let counter : number = 0;
                            $httpResponse.on("data", ($chunk : string) : void => {
                                currentLength += $chunk.length;
                                if (options.streamOutput) {
                                    streamBuffers.push($chunk);
                                }
                                if (counter < 100) {
                                    Echo.Print(".");
                                } else {
                                    if (!isNaN(length)) {
                                        Echo.Println(" (" + Math.floor(100 / length * currentLength) + "%)");
                                    } else {
                                        Echo.Println(".");
                                    }
                                    counter = 0;
                                }
                                counter++;
                                response.OnChange({
                                    currentValue: currentLength,
                                    rangeEnd    : length,
                                    rangeStart  : 0
                                });
                            });

                            let completed : boolean = false;
                            const onComplete : any = () : void => {
                                if (!completed) {
                                    completed = true;
                                    if (options.streamOutput) {
                                        const data : any = Buffer.concat(streamBuffers);
                                        response.OnComplete($httpResponse.headers, options.streamReturnsBuffer ? data : data.toString());
                                    } else {
                                        response.OnComplete($httpResponse.headers, fileName);
                                    }

                                    delete this.responseRegister.running[response.getId()];
                                }
                            };
                            let connectionClosed : boolean = false;
                            $httpResponse.on("close", () : void => {
                                if (currentLength !== length && !isChunked) {
                                    connectionClosed = true;
                                    if (!options.streamOutput) {
                                        this.Delete(fileName);
                                    } else {
                                        streamBuffers = [];
                                    }
                                    const abortIndex : number =
                                        this.responseRegister.abort.indexOf(response.getId() + "");
                                    if (abortIndex !== -1) {
                                        this.responseRegister.abort.splice(abortIndex, 1);
                                        LogIt.Info("Connection has been aborted.");
                                    } else {
                                        if (this.responseRegister.running.hasOwnProperty(response.getId())) {
                                            if (this.responseRegister.running[response.getId()]
                                                .reconnectCounter < options.maxReconnection) {
                                                LogIt.Warning("Reconnecting ...");
                                                httpRequest.end();
                                                this.Download($urlOrOptions, response);
                                            } else {
                                                response.OnError("Connection has been lost.");
                                            }
                                            this.responseRegister.running[response.getId()].reconnectCounter++;
                                        } else {
                                            response.OnError("Connection has been lost.");
                                        }
                                    }
                                } else {
                                    onComplete();
                                }
                            });

                            $httpResponse.on("end", () : void => {
                                if (!connectionClosed) {
                                    Echo.Println(" (100%)");
                                    onComplete();
                                }
                            });
                        } catch (ex) {
                            response.OnError(ex);
                        }
                    } else if ($httpResponse.statusCode === 301 || $httpResponse.statusCode === 302) {
                        options.url = $httpResponse.headers.location;
                        this.Download(options, response);
                    } else if (options.proxy && $httpResponse.statusCode === 407) {
                        origOptions.proxy = false;
                        LogIt.Info("Unable to make request over proxy. Trying without proxy ...");
                        this.Download(origOptions, response);
                    } else {
                        response.OnError("Bad response status code: " + $httpResponse.statusCode);
                        delete this.responseRegister.running[response.getId()];
                    }
                });

                httpRequest.on("error", ($ex : Error) : void => {
                    response.OnError($ex);
                    if (this.responseRegister.running[response.getId()].reconnectCounter < options.maxReconnection) {
                        Echo.Println($ex.message);
                        this.Download($urlOrOptions, response);
                    } else {
                        Echo.Println("");
                        response.OnError($ex);
                    }
                    this.responseRegister.running[response.getId()].reconnectCounter++;
                });

                if (options.body !== "") {
                    httpRequest.write(options.body);
                }

                httpRequest.end();

                if (!this.responseRegister.running.hasOwnProperty(response.getId())) {
                    response.OnStart();
                    this.responseRegister.running[response.getId()] = {
                        httpRequest,
                        reconnectCounter: 0
                    };
                } else {
                    this.responseRegister.running[response.getId()].httpRequest = httpRequest;
                }
            } else if (options.url.protocol === "file:") {
                try {
                    const formatUrl : any = ($url : string) : string => {
                        return StringUtils.Replace(StringUtils.Replace($url, "file:///", ""), "file://", "");
                    };
                    let urlTemp : string = options.url.href;
                    if (ObjectValidator.IsString($urlOrOptions)) {
                        urlTemp = <string>$urlOrOptions;
                    }

                    if (options.streamOutput) {
                        const data : Buffer = <Buffer>this.Read(formatUrl(urlTemp));
                        response.OnComplete({}, data);
                    } else {
                        const tmpPath : string = prepareTmp();
                        this.Copy(formatUrl(urlTemp), tmpPath, () : void => {
                            response.OnComplete({}, tmpPath);
                        });
                    }
                } catch (ex) {
                    response.OnError(ex);
                }
            } else {
                response.OnError("Selected protocol \"" + options.url.protocol + "\" is not supported." +
                    " Please use http://, https:// or file:// protocol instead.");
            }
        }

        public AbortDownload($id : number) : boolean {
            const id : string = $id + "";
            let status : boolean = false;
            if (this.responseRegister.running.hasOwnProperty(id)) {
                try {
                    this.responseRegister.abort.push(id);
                    this.responseRegister.running[id].httpRequest.abort();
                    delete this.responseRegister.running[id];
                    status = true;
                    LogIt.Info("Download with connection id \"" + $id + "\" has been abort.");
                } catch (ex) {
                    LogIt.Warning(ex.message);
                }
            }
            return status;
        }

        public Delete($path : string, $callback? : (($status : boolean) => void) | IResponse) : boolean {
            const fs : Types.NodeJS.fs = require("fs");
            let status : boolean = false;
            const response : IResponse = ResponseFactory.getResponse($callback);
            response.OnStart();
            if (!ObjectValidator.IsEmptyOrNull($path)) {
                try {
                    let counter : number = 0;
                    const deleteRecursive : any = ($path : string) : void => {
                        if (this.exists($path)) {
                            try {
                                if (fs.lstatSync($path).isDirectory()) {
                                    fs.readdirSync($path).forEach(($file : string) : void => {
                                        deleteRecursive($path + "/" + $file);
                                    });
                                    fs.rmdirSync($path);
                                } else {
                                    fs.unlinkSync($path);
                                }
                            } catch (ex) {
                                LogIt.Warning("Trying to remove file as directory because of:" + ex.message);
                                fs.rmdirSync($path);
                            }

                            response.OnChange({
                                currentValue: counter,
                                rangeEnd    : 100,
                                rangeStart  : 0
                            });
                            if (counter < 100) {
                                Echo.Print(".");
                            } else {
                                Echo.Println(".");
                                counter = 0;
                            }
                            counter++;
                        }
                    };
                    $path = this.NormalizePath($path);
                    LogIt.Info("> delete path \"" + $path + "\"", LogSeverity.MEDIUM);
                    if (this.exists($path)) {
                        if (fs.lstatSync($path).isDirectory()) {
                            deleteRecursive($path);
                            Echo.Println("");
                        } else {
                            fs.unlinkSync($path);
                        }
                    }
                    status = true;
                } catch (ex) {
                    LogIt.Warning(ex.message);
                }
            }
            response.OnComplete(status);
            return status;
        }

        public Pack($path : string | string[], $options? : ArchiveOptions, $callback? : (($tmpPath : string) => void) | IResponse) : void {
            const path : Types.NodeJS.path = require("path");
            const fs : Types.NodeJS.fs = require("fs");
            const archiver : any = require("archiver");
            const compressjs : any = require("compressjs");

            let cwd : string = "";
            if (ObjectValidator.IsString($path)) {
                if (this.IsFile(<string>$path)) {
                    $path = path.dirname(<string>$path);
                }
                if (!StringUtils.Contains(<string>$path, "*")) {
                    $path += "/**/*";
                }
                cwd = StringUtils.Substring(<string>$path, 0, StringUtils.IndexOf(<string>$path, "/*", false));
            }
            const paths : string[] = this.Expand($path);
            let notFound : boolean = false;

            const response : IResponse = ResponseFactory.getResponse($callback === undefined ? <any>$options : $callback);
            if (paths.length > 0) {
                paths.forEach(($element : string, $index : number) : void => {
                    if (!fs.existsSync($element)) {
                        response.OnError("Unable to locate file \"" + $element + "\" for archive creation.");
                        notFound = true;
                    } else {
                        if (StringUtils.Contains($element, " ")) {
                            $element = "\"" + $element + "\"";
                        }
                        paths[$index] = this.NormalizePath($element, true);
                    }
                });
            } else {
                notFound = true;
                response.OnError("Unable to create archive from empty source \"" + $path + "\"");
            }

            if (!notFound) {
                const options : ArchiveOptions = {
                    cwd,
                    output  : "",
                    override: true,
                    type    : EnvironmentHelper.IsWindows() ? "zip" : "tar.bz2"
                };
                if (ObjectValidator.IsObject($options)) {
                    Resources.Extend(options, $options);
                }
                if (ObjectValidator.IsEmptyOrNull(options.output)) {
                    let tmpOutput : string;
                    if (ObjectValidator.IsString($path)) {
                        tmpOutput = path.basename(this.NormalizePath(StringUtils.Remove(<string>$path, "*")));
                    } else {
                        tmpOutput = "archive";
                    }
                    options.output = this.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() + "/" +
                        tmpOutput + "." + options.type;
                } else if (StringUtils.Contains(options.output, ".tar")) {
                    options.type = StringUtils.Substring(options.output, StringUtils.IndexOf(options.output, ".tar"));
                }
                if (StringUtils.StartsWith(options.type, ".")) {
                    options.type = StringUtils.Substring(options.type, 1);
                }
                if (["zip", "tar", "tar.gz", "tar.bz2"].indexOf(options.type) === -1) {
                    response.OnError("Failed to create archive: required format \"" + options.type + "\" does not match " +
                        "any of supported formats (zip, tar, tar.gz and tar.bz2)");
                } else {
                    options.output = this.NormalizePath(options.output);
                    if (StringUtils.Contains(options.output, ".")) {
                        options.output = StringUtils.Substring(options.output, 0, StringUtils.IndexOf(options.output, "."));
                    }
                    options.output += "." + options.type;
                    options.cwd = this.NormalizePath(options.cwd);
                    LogIt.Info("> pack \"" + options.cwd + "\" to \"" + options.output + "\"");

                    options.output = StringUtils.Remove(options.output, ".bz2");
                    if (StringUtils.OccurrenceCount(options.cwd, "/") > 1) {
                        options.cwd = path.dirname(options.cwd);
                    }
                    try {
                        if (!options.override && this.Exists(options.output)) {
                            response.OnError("Failed to create archive: destination file already exists");
                        } else {
                            this.Delete(options.output);
                            this.pmkdir(path.dirname(options.output));
                            const output : any = fs.createWriteStream(options.output);
                            const archive : any = archiver(StringUtils.Contains(options.type, "tar") ? "tar" : options.type, {
                                gzip: StringUtils.Contains(options.type, ".gz"),
                                zlib: {level: 9}
                            });
                            output.on("close", () : void => {
                                if (StringUtils.Contains(options.type, ".bz2")) {
                                    LogIt.Info("> compression to bzip2 format");
                                    this.Write(options.output + ".bz2", compressjs.Bzip2.compressFile(this.Read(options.output)));
                                    this.Delete(options.output);
                                    response.Send(options.output + ".bz2");
                                } else {
                                    response.Send(options.output);
                                }
                            });
                            archive.on("warning", ($error : any) : void => {
                                if ($error.code === "ENOENT") {
                                    LogIt.Warning($error);
                                } else {
                                    response.OnError($error);
                                }
                            });
                            archive.on("error", ($error : Error) : void => {
                                response.OnError($error);
                            });

                            let rootFolder : string = path.basename(options.output);
                            rootFolder = StringUtils.Substring(rootFolder, 0, StringUtils.IndexOf(rootFolder, "."));
                            paths.forEach(($file : string) : void => {
                                $file = StringUtils.Remove($file, "\"");
                                archive.file($file, {
                                    name: rootFolder + "/" + StringUtils.Remove(this.NormalizePath($file), options.cwd + "/")
                                });
                            });

                            archive.pipe(output);
                            archive.finalize();
                        }
                    } catch (ex) {
                        response.OnError(ex);
                    }
                }
            }
        }

        public Unpack($path : string, $options? : ArchiveOptions, $callback? : (($tmpPath : string) => void) | IResponse) : void {
            const path : Types.NodeJS.path = require("path");
            const fs : Types.NodeJS.fs = require("fs");
            const decompress : any = require("decompress");

            let pathExtension : string = path.extname($path);
            const pathMatch : RegExpMatchArray = $path.match(/\.tar\.[a-zA-Z0-9]+$/g);
            if (!ObjectValidator.IsEmptyOrNull(pathMatch) && !ObjectValidator.IsEmptyOrNull(pathMatch[0])) {
                pathExtension = pathMatch[0];
            }

            const response : IResponse = ResponseFactory.getResponse(!ObjectValidator.IsSet($callback) ? <any>$options : $callback);
            const options : ArchiveOptions = {
                autoStrip: true,
                output   : "",
                override : true
            };
            if (ObjectValidator.IsObject($options)) {
                Resources.Extend(options, $options);
            }

            if (options.output === "") {
                options.output = this.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() + "/" +
                    path.basename($path).replace(pathExtension, "");
            }
            options.output = this.NormalizePath(options.output);
            $path = this.NormalizePath($path);

            LogIt.Info("> unpack \"" + $path + "\" to \"" + options.output + "\"");

            const onComplete : any = () : void => {
                response.OnComplete(options.output);
            };
            if (!options.override && fs.existsSync(options.output)) {
                onComplete();
            } else {
                this.Delete(options.output);
                try {
                    decompress($path, options.output, {
                        map    : ($file : any) : any => {
                            if ($file.type === "symlink") {
                                if (EnvironmentHelper.IsWindows() && !fs.existsSync($file.linkname)) {
                                    $file.linkname = this.NormalizePath(options.output + "/" + $file.linkname);
                                }
                            }
                            return $file;
                        },
                        plugins: [
                            require("decompress-unzip")(),
                            require("decompress-tar")(),
                            require("decompress-tarbz2")(),
                            require("decompress-targz")(),
                            require("decompress-tarxz")()
                        ],
                        strip  : options.autoStrip ? 1 : 0
                    }).then(() : void => {
                        onComplete();
                    }).catch(($error : any) : void => {
                        if ($error.code === "EPERM" || $error.code === "ENOENT") {
                            LogIt.Warning($error.stack);
                            onComplete();
                        } else {
                            response.OnError($error);
                        }
                    });
                } catch (ex) {
                    response.OnError(ex);
                }
            }
        }

        public NormalizePath($source : string, $osSeparator : boolean = false) : string {
            const path : Types.NodeJS.path = require("path");
            $source = path.normalize($source);
            if ($osSeparator) {
                $source = StringUtils.Replace($source, path.sep + path.sep, path.sep);
            } else {
                if (path.sep !== "/") {
                    $source = StringUtils.Replace($source, path.sep, "/");
                }
                $source = StringUtils.Replace($source, "//", "/");
            }
            return $source;
        }

        private exists($path : string) : boolean {
            let status : boolean = false;
            if (!ObjectValidator.IsEmptyOrNull($path)) {
                $path = this.NormalizePath($path);
                const fs : Types.NodeJS.fs = require("fs");
                const isLink : any = () : boolean => {
                    let retVal : boolean = false;
                    try {
                        retVal = fs.lstatSync($path).isSymbolicLink();
                        LogIt.Info("> is existing symbolic link", LogSeverity.LOW);
                    } catch (e) {
                        // leave this block empty
                    }
                    return retVal;
                };
                status = fs.existsSync($path) || isLink();
            }
            return status;
        }

        private pmkdir($path : string) : void {
            const fs : Types.NodeJS.fs = require("fs");
            if (!fs.existsSync($path)) {
                $path.split("/").reduce(($currentPath : string, $folder : string) : string => {
                    $currentPath += $folder + "/";
                    if (!fs.existsSync($currentPath)) {
                        LogIt.Info("> create dir: " + $currentPath, LogSeverity.LOW);
                        fs.mkdirSync($currentPath);
                    }
                    return $currentPath;
                }, "");
            }
        }
    }

    export abstract class ResponsesRegister {
        public running : any[];
        public abort : string[];
    }
}
