/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Events {
    "use strict";

    export class EventsManager extends Com.Wui.Framework.Gui.Events.EventsManager {

        public Subscribe() : void {
            if (!Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
                // do not handle events
            } else {
                super.Subscribe();
            }
        }
    }
}
