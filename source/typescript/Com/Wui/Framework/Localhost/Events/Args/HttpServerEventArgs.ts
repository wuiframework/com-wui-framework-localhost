/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    export class HttpServerEventArgs extends EventArgs {
        private clientId : number;

        constructor($clientId? : number) {
            super();
            this.clientId = $clientId;
        }

        public ClientId($value? : number) : number {
            return this.clientId = Property.Integer(this.clientId, $value);
        }
    }
}
