/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class HttpRequestParser extends Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser {

        public getUrl() : string {
            if (this.isOnBackend()) {
                const url : string = this.getOwner().url;
                if (ObjectValidator.IsEmptyOrNull(url)) {
                    return "/";
                }
                return url;
            }
            return super.getUrl();
        }

        public getScheme() : string {
            if (this.isOnBackend()) {
                return this.getOwner().protocol;
            }
            return this.parsedInfo.scheme;
        }

        public getClientIP() : string {
            if (this.isOnBackend()) {
                const request : any = this.getOwner();
                try {
                    if (!ObjectValidator.IsEmptyOrNull(request) && request !== window) {
                        const ip = request.headers["x-forwarded-for"] ||
                            request.connection.remoteAddress ||
                            request.socket.remoteAddress ||
                            request.connection.socket.remoteAddress;
                        return ip.split(",")[0];
                    }
                } catch (ex) {
                    // unable to read or request properties correctly
                }
            }
            return super.getClientIP();
        }

        public getServerIP() : string {
            if (this.isOnBackend()) {
                const interfaces : Types.NodeJS.Modules.os.networkInterfaces = require("os").networkInterfaces();
                let output : string = this.parsedInfo.hostIpAddress;
                Object.keys(interfaces).forEach(($name : string) : void => {
                    interfaces[$name].forEach(($networkInterface : any) : void => {
                        if ("IPv4" !== $networkInterface.family || $networkInterface.internal !== false) {
                            return;
                        }
                        output = $networkInterface.address;
                    });
                });
                return output;
            }
            return this.parsedInfo.hostIpAddress;
        }

        protected getServerLocation() : Location {
            if (this.isOnBackend()) {
                if (this.getOwner() === window) {
                    return require("url").parse("http://" + Loader.getInstance().getAppConfiguration().domain.location);
                }
                return require("url").parse(this.getUrl());
            }
            return super.getServerLocation();
        }

        protected getNavigator() : Navigator {
            if (this.isOnBackend()) {
                return <any>{
                    cookieEnabled: false,
                    language     : "en-US",
                    onLine       : true,
                    platform     : "win64",
                    userAgent    : "Node.js Chrome/52.0.2743.116"
                };
            }
            return super.getNavigator();
        }

        protected getRawHeaders() : ArrayList<string> {
            if (this.isOnBackend()) {
                const headers : ArrayList<string> = new ArrayList<string>();
                try {
                    if (!ObjectValidator.IsEmptyOrNull(this.getOwner()) && this.getOwner() !== window) {
                        let key : string;
                        for (key in this.getOwner().headers) {
                            if (this.getOwner().headers.hasOwnProperty(key)) {
                                headers.Add(this.getOwner().headers[key], StringUtils.ToLowerCase(key));
                            }
                        }
                    }
                } catch (ex) {
                    // unable to read or parse headers
                }
                return headers;
            }
            return new ArrayList<string>();
        }

        private isOnBackend() : boolean {
            const status : boolean = !Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed();
            this.isOnBackend = () : boolean => {
                return status;
            };
            return status;
        }
    }
}
