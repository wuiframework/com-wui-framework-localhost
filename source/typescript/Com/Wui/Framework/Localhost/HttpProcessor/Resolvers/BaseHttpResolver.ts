/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IResponseProtocol = Com.Wui.Framework.Localhost.Interfaces.IResponseProtocol;
    import WebResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.WebResponse;

    export class BaseHttpResolver extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        private connector : WebResponse;
        private protocol : IResponseProtocol;

        public Process() : void {
            super.Process();
            if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
                this.getConnector().Send(document.documentElement.innerHTML.replace(/<body(.*)>/gm, "<body>"));
            }
        }

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            if ($POST.KeyExists("Connector")) {
                this.connector = $POST.getItem("Connector");
            }
            if ($POST.KeyExists("Protocol")) {
                this.protocol = $POST.getItem("Protocol");
            }
        }

        protected getRequest() : HttpRequestParser {
            return <HttpRequestParser>super.getRequest();
        }

        protected getConnector() : WebResponse {
            return this.connector;
        }

        protected getProtocol() : IResponseProtocol {
            return this.protocol;
        }
    }
}
