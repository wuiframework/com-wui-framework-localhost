/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.Resolvers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import IResponseProtocol = Com.Wui.Framework.Localhost.Interfaces.IResponseProtocol;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import JsonUtils = Com.Wui.Framework.Commons.Utils.JsonUtils;

    export class ConnectorResponse extends BaseHttpResolver {
        private clientId : string;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            if ($GET.KeyExists("id")) {
                this.clientId = $GET.getItem("id");
            } else if ($GET.KeyExists("clientId")) {
                this.clientId = $GET.getItem("clientId");
            }
            super.argsHandler($GET, $POST);
        }

        protected resolver() : void {
            const responsePersistence : IPersistenceHandler = PersistenceFactory.getPersistence(this.getClassName());
            const url : string = this.RequestArgs().Url();

            if (StringUtils.Contains(url, "/connector.config.js")) {
                const headers : ArrayList<string> = this.getRequest().getHeaders();
                const data : any = JsonUtils.Clone(this.getProtocol());
                if (headers.KeyExists("host")) {
                    let scheme : string = this.getRequest().getScheme();
                    if (!ObjectValidator.IsEmptyOrNull(headers.getItem("referer"))) {
                        scheme = StringUtils.Contains(headers.getItem("referer"), "https://") ? "https://" : "http://";
                    }
                    if (!ObjectValidator.IsEmptyOrNull(headers.getItem("x-forwarded-host"))) {
                        data.address = headers.getItem("x-forwarded-host");
                    } else {
                        data.address = headers.getItem("host");
                    }
                    if (StringUtils.Contains(data.address, ":")) {
                        data.port = StringUtils.ToInteger(StringUtils.Substring(data.address,
                            StringUtils.IndexOf(data.address, ":") + 1));
                        data.address = StringUtils.Substring(data.address,
                            0, StringUtils.IndexOf(data.address, ":"));
                    } else {
                        data.port = scheme === "https://" ? 443 : 80;
                    }
                    data.responseUrl = scheme + data.address + ":" + data.port + "/{0}/response.jsonp";
                }
                this.getConnector().Send(<IResponseProtocol>{
                    body   : "JsonpData(" + JSON.stringify(data) + ");",
                    headers: {"content-type": "application/javascript"},
                    status : HttpStatusType.SUCCESS
                });
            } else if (!ObjectValidator.IsEmptyOrNull(this.clientId)) {
                if (StringUtils.Contains(url, "CloseClient")) {
                    responsePersistence.Destroy(this.clientId);
                    this.getConnector().Send("");
                } else {
                    if (responsePersistence.Exists(this.clientId)) {
                        this.getConnector().Send(<IResponseProtocol>{
                            body   : "JsonpData(" + responsePersistence.Variable(this.clientId) + ");",
                            headers: {"content-type": "application/javascript"},
                            status : HttpStatusType.SUCCESS
                        });
                    } else {
                        this.getConnector().Send(<IResponseProtocol>{
                            body   : "Response data for client \"" + this.clientId + "\" does not exist.",
                            headers: {"content-type": "application/javascript"},
                            status : HttpStatusType.ERROR
                        });
                    }
                }
            } else {
                this.getConnector().Send(<IResponseProtocol>{
                    body   : "Client id \"" + this.clientId + "\" has not been defined.",
                    headers: {"content-type": "application/javascript"},
                    status : HttpStatusType.ERROR
                });
            }
        }
    }
}
