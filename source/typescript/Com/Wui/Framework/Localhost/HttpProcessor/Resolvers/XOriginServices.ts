/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.Resolvers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class XOriginServices extends FileRequestResolver {

        protected streamProcessor($filePath : string) : any {
            const fs : Types.NodeJS.fs = require("fs");
            const Readable : any = require("stream").Readable;

            let content : string = fs.readFileSync($filePath).toString();
            let origin : string = Loader.getInstance().getAppConfiguration().domain.location;
            const headers : ArrayList<string> = this.getRequest().getHeaders();
            if (headers.KeyExists("host")) {
                if (!ObjectValidator.IsEmptyOrNull(headers.getItem("x-forwarded-host"))) {
                    origin = headers.getItem("x-forwarded-host");
                } else {
                    origin = headers.getItem("host");
                }
            }
            let schema : string = "http://";
            if (headers.KeyExists("referer")) {
                if (StringUtils.StartsWith(headers.getItem("referer"), "https://")) {
                    schema = "https://";
                }
            } else {
                schema = this.getRequest().getScheme();
            }
            content = StringUtils.Replace(content, "<? @var xservices.origin ?>", origin + "/xorigin/");
            content = StringUtils.Replace(content, "<? @var xservices.request ?>", schema + origin + "/xorigin/");

            const stream : Types.NodeJS.Modules.stream.Readable = new Readable();
            stream.push(content);
            stream.push(null);
            return stream;
        }
    }
}
