/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IResponseProtocol = Com.Wui.Framework.Localhost.Interfaces.IResponseProtocol;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import CertsManager = Com.Wui.Framework.Localhost.Utils.CertsManager;

    export class AcmeResolver extends BaseHttpResolver {
        private token : string;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            if ($GET.KeyExists("token")) {
                this.token = $GET.getItem("token");
            }
            super.argsHandler($GET, $POST);
        }

        protected resolver() : void {
            const dataPath : string = CertsManager.getInstance().getStoragePath() + "/" + this.token;
            if (Loader.getInstance().getFileSystemHandler().Exists(dataPath)) {
                this.getConnector().Send(<IResponseProtocol>{
                    body   : require("fs").createReadStream(dataPath),
                    headers: {"content-type": "text/plain"},
                    status : HttpStatusType.SUCCESS
                });
            } else {
                this.getConnector().Send(<IResponseProtocol>{
                    body  : "Data for ACME challenge has not been found.",
                    status: HttpStatusType.NOT_FOUND
                });
            }
        }
    }
}
