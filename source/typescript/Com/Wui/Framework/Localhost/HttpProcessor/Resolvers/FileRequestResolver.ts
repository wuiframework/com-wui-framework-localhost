/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.Resolvers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import IProject = Com.Wui.Framework.Localhost.Interfaces.IProject;

    export class FileRequestResolver extends BaseHttpResolver {
        private static patterns : string[];
        private static targetAppDataPath : string;

        public Process() : void {
            if (!this.browserValidator()) {
                LogIt.Error("Unsupported browser. User Agent: " + this.getRequest().getUserAgent());
                this.getHttpManager().ReloadTo("/ServerError/Browser", null, true);
                this.stop();
            } else {
                this.resolver();
            }
        }

        protected browserValidator() : boolean {
            return !this.getRequest().IsSearchBot();
        }

        protected filePathResolver() : IFilePath {
            const fs : Types.NodeJS.fs = require("fs");
            const path : Types.NodeJS.path = require("path");
            let url : string = this.getRequest().getUrl();
            if (StringUtils.Contains(url, "?")) {
                url = StringUtils.Substring(url, 0, StringUtils.IndexOf(url, "?"));
            }
            const resourcePath : string = "/resource/";
            if (!StringUtils.StartsWith(url, resourcePath) && StringUtils.Contains(url, resourcePath) ||
                StringUtils.OccurrenceCount(url, resourcePath) > 1) {
                const searchFor : string = StringUtils.Contains(url, "/test" + resourcePath) ? "/test/" + resourcePath : resourcePath;
                url = StringUtils.Substring(url, StringUtils.IndexOf(url, searchFor, false));
            }

            let filePath : string = Loader.getInstance().getProgramArgs().TargetBase() + url;
            if (!fs.existsSync(filePath)) {
                if (url === "/favicon.ico") {
                    filePath = Loader.getInstance().getProgramArgs().TargetBase() + resourcePath + "graphics/icon.ico";
                    if (!fs.existsSync(filePath)) {
                        filePath = Loader.getInstance().getProgramArgs().ProjectBase() + resourcePath + "graphics/icon.ico";
                    }
                } else {
                    filePath = FileRequestResolver.targetAppDataPath + "/" + url;
                }
            }
            const name = path.basename(filePath);
            let extension = name;
            extension = StringUtils.Substring(extension,
                StringUtils.IndexOf(name, ".", false) + 1, StringUtils.Length(name));
            extension = StringUtils.ToLowerCase(extension);
            if (extension === "ts" || extension === "sass") {
                filePath = Loader.getInstance().getProgramArgs().TargetBase() + "/../.." + url;
            }
            filePath = path.normalize(filePath);
            return {
                extension,
                name,
                path: filePath,
                url
            };
        }

        protected getAllowedPatterns() : string[] {
            if (ObjectValidator.IsSet(FileRequestResolver.patterns)) {
                return FileRequestResolver.patterns;
            }
            let patterns : string[] = [
                "/connector.config.js*",
                "/LICENSE.txt",
                "/favicon.ico",
                "/resource/css/*",
                "/resource/graphics/*",
                "/resource/javascript/*",
                "/resource/data/Com/Wui/Framework/Services/*",
                "/resource/libs/FontOxygen/*",
                "/test/resource/data/*",
                "*.ts"
            ];
            const loader : Loader = Loader.getInstance();
            const properties : IProject = loader.getAppConfiguration();
            const fileSystem : FileSystemHandler = loader.getFileSystemHandler();
            const targetPath : string = loader.getProgramArgs().TargetBase();
            let appDataPath : string = loader.getProgramArgs().AppDataPath();

            let config : string[] = properties.target.requestPatterns;
            if (ObjectValidator.IsString(config)) {
                config = StringUtils.Split(<any>config, ",");
            }
            if (!ObjectValidator.IsEmptyOrNull(config)) {
                patterns = patterns.concat(config);
            }
            const configPath : string = targetPath + "/wuirunner.config.jsonp";
            if (properties.connectorProxy && fileSystem.Exists(configPath)) {
                try {
                    let configData : string = fileSystem.Read(configPath).toString();
                    configData = StringUtils.Substring(configData,
                        StringUtils.IndexOf(configData, "({"),
                        StringUtils.IndexOf(configData, "});", false) + 2);
                    /* tslint:disable: no-eval */
                    const runnerConfig : any = eval(configData);
                    /* tslint:enable */
                    config = runnerConfig.requestPatterns;
                    if (ObjectValidator.IsString(config)) {
                        config = StringUtils.Split(<any>config, ",");
                    }
                    if (!ObjectValidator.IsEmptyOrNull(config)) {
                        patterns = patterns.concat(config);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(runnerConfig.appDataPath)) {
                        appDataPath = runnerConfig.appDataPath;
                    }
                } catch (ex) {
                    LogIt.Error("Unable to parse " + configPath, ex);
                }
            }
            FileRequestResolver.patterns = patterns;
            FileRequestResolver.targetAppDataPath = appDataPath;
            this.getAllowedPatterns = () : string[] => {
                return patterns;
            };
            return patterns;
        }

        protected resolver() : void {
            const fs : Types.NodeJS.fs = require("fs");
            const patterns : string[] = this.getAllowedPatterns();
            const filePath : IFilePath = this.filePathResolver();
            LogIt.Debug("file request for: {0}", filePath.path);
            if (fs.existsSync(filePath.path)) {
                let allowedPath : boolean = false;
                const requiredUrl : string = StringUtils.Replace(filePath.url, "\\", "/");
                patterns.forEach(($pattern : string) : void => {
                    if (StringUtils.PatternMatched($pattern, requiredUrl)) {
                        allowedPath = true;
                    }
                });
                if (allowedPath) {
                    let type : string = "text/html";
                    const stats : any = fs.statSync(filePath.path);
                    const lastModifiedTime : number = stats.mtime.getTime();
                    const lastModifiedTimeGMT : string = Convert.TimeToGMTformat(lastModifiedTime);
                    const eTag : string = StringUtils.getSha1(lastModifiedTime + filePath.path);

                    if (this.getRequest().getUrlArgs().KeyExists("refresh") ||
                        StringUtils.Contains(this.getRequest().getUrl(), "/forceRefresh") ||
                        this.getRequest().getLastModifiedTime() !== lastModifiedTimeGMT &&
                        (this.getRequest().getEtags().IsEmpty() || !this.getRequest().getEtags().Contains(eTag))
                    ) {
                        const headers : any = {
                            /* tslint:disable: object-literal-sort-keys */
                            "pragma"       : "public",
                            "expires"      : 0,
                            "etag"         : eTag,
                            "Last-Modified": lastModifiedTimeGMT
                            /* tslint:enable */
                        };
                        switch (filePath.extension) {
                        case "htm":
                        case "html":
                            type = "text/html";
                            break;
                        case "txt":
                        case "ts":
                        case "sass":
                        case "log":
                        case "obj":
                            type = "text/plain";
                            break;
                        case "css":
                            headers["accept-ranges"] = "bytes";
                            type = "text/css";
                            break;
                        case "js":
                        case "json":
                        case "jsonp":
                        case "map":
                            headers["accept-ranges"] = "bytes";
                            type = "application/javascript";
                            break;
                        case "pdf":
                            type = "application/pdf";
                            break;
                        case "bmp":
                            headers["accept-ranges"] = "bytes";
                            type = "image/bmp";
                            break;
                        case "jpg":
                        case "jpe":
                        case "jpeg":
                            headers["accept-ranges"] = "bytes";
                            type = "image/jpeg";
                            break;
                        case "png":
                            headers["accept-ranges"] = "bytes";
                            type = "image/png";
                            break;
                        case "gif":
                            headers["accept-ranges"] = "bytes";
                            type = "image/gif";
                            break;
                        case "ico":
                        case "icon":
                            type = "image/x-icon";
                            break;
                        case "woff":
                            type = "application/font-woff";
                            break;
                        case "woff2":
                            type = "application/font-woff2";
                            break;
                        case "eot":
                            type = "application/vnd.ms-fontobject";
                            break;
                        case "svg":
                            type = "image/svg+xml";
                            break;
                        default :
                            if (filePath.extension !== "ts" && filePath.extension !== "sass") {
                                const fileSize : number = stats.size;
                                const fileName : string = ObjectEncoder.Url(filePath.name);
                                headers["cache-control"] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
                                headers["accept-ranges"] = "bytes";
                                headers["content-encoding"] = "identity";
                                headers["content-transfer-encoding"] = "binary";
                                headers["content-description"] = "File Transfer";
                                headers["Content-Length"] = fileSize;
                                headers["content-range"] = "0-" + (fileSize - 1) + "/" + fileSize;
                                headers["content-disposition"] =
                                    "attachment; filename*=UTF-8''" + fileName + "; filename=\"" + fileName + "\"";
                                headers["x-xss-protection"] = "1; mode=block";
                                headers["x-content-type-options"] = "nosniff";
                                headers["x-frame-options"] = "SAMEORIGIN";
                                headers["x-robots-tag"] = "none";
                                headers["x-download-options"] = "noopen";
                                headers["x-permitted-cross-domain-policies"] = "none";
                                type = "application/octet-stream";
                            }
                        }
                        headers["content-type"] = type;
                        this.getConnector().Send({
                            body  : this.streamProcessor(filePath.path),
                            headers,
                            status: HttpStatusType.SUCCESS
                        });
                    } else {
                        this.getConnector().Send({
                            body  : "",
                            status: HttpStatusType.CACHED
                        });
                    }
                } else {
                    LogIt.Debug("Access denied from: {0}", filePath.url);
                    this.getConnector().Send({
                        body  : "Access denied for: " + (ObjectValidator.IsEmptyOrNull(filePath.path) ? filePath.url : filePath.path),
                        status: HttpStatusType.FORBIDDEN
                    });
                }
            } else {
                this.getConnector().Send({
                    body  : "Unable to locate: " + (ObjectValidator.IsEmptyOrNull(filePath.path) ? filePath.url : filePath.path),
                    status: HttpStatusType.NOT_FOUND
                });
            }
        }

        protected streamProcessor($filePath : string) : any {
            return require("fs").createReadStream($filePath);
        }
    }

    export abstract class IFilePath {
        public url : string;
        public readonly path : string;
        public readonly name : string;
        public readonly extension : string;
    }
}
