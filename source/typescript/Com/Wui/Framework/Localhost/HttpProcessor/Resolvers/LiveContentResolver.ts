/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import LiveContentResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.LiveContentResponse;
    import IWebServiceProtocol = Com.Wui.Framework.Services.Interfaces.IWebServiceProtocol;
    import IRequestConnector = Com.Wui.Framework.Localhost.Interfaces.IRequestConnector;
    import ILiveContentProtocol = Com.Wui.Framework.Services.Interfaces.ILiveContentProtocol;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IAuthPromise = Com.Wui.Framework.Localhost.Interfaces.IAuthPromise;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import Metrics = Com.Wui.Framework.Localhost.Utils.Metrics;
    import IConnector = Com.Wui.Framework.Localhost.Interfaces.IConnector;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;

    export class LiveContentResolver extends BaseHttpResolver {
        private static eventSubscribers : IEventSubscriber[][];
        private static connectorsRegister : IConnectorInfo[] = [];
        private requestProtocol : IWebServiceProtocol;
        private response : IResponse;
        private weakAuth : boolean;

        public static FireEvent($name : string, $args? : any) : void {
            if (ObjectValidator.IsEmptyOrNull($args)) {
                $args = "";
            }
            if (ObjectValidator.IsEmptyOrNull(LiveContentResolver.eventSubscribers)) {
                LiveContentResolver.eventSubscribers = <any>{};
            }
            if (LiveContentResolver.eventSubscribers.hasOwnProperty($name)) {
                let index : number;
                for (index = 0; index < LiveContentResolver.eventSubscribers[$name].length; index++) {
                    const subscriber : IEventSubscriber = LiveContentResolver.eventSubscribers[$name][index];
                    subscriber.protocol.status = HttpStatusType.SUCCESS;
                    subscriber.protocol.type = "FireEvent";
                    subscriber.protocol.data = Buffer.from(JSON.stringify({
                        args: Buffer.from(JSON.stringify($args)).toString("base64"),
                        name: $name
                    })).toString("base64");
                    subscriber.connector.Send(subscriber.protocol);
                }
            }
        }

        constructor() {
            super();

            this.weakAuth = true;
        }

        public Process() : void {
            this.resolver();
        }

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            super.argsHandler($GET, $POST);
            if ($POST.KeyExists("LiveProtocol")) {
                this.requestProtocol = $POST.getItem("LiveProtocol");
            }
            if ($POST.KeyExists("LiveResponse")) {
                this.response = $POST.getItem("LiveResponse");
            }
        }

        protected resolver() : void {
            const metrics : Metrics = Metrics.getInstance();
            let data : any = null;
            try {
                data = JSON.parse(Buffer.from(<string>this.requestProtocol.data, "base64").toString("utf8"));
            } catch (ex) {
                LogIt.Info("Received message in incorrect format: " + this.requestProtocol);
                this.response.OnError(ex);
            }
            if (data !== null) {
                try {
                    const requestData : ILiveContentProtocol = data;
                    metrics.Start(requestData.name, "LiveContent");
                    if (StringUtils.StartsWith(this.requestProtocol.type, "LiveContentWrapper.")) {
                        const namespaceName : string = requestData.name.substr(0, requestData.name.lastIndexOf("."));
                        let memberName : string = requestData.name.replace(namespaceName + ".", "");
                        const args : any[] = JSON.parse(Buffer.from(requestData.args, "base64").toString("utf8"));

                        const classPrototype : any = Reflection.getInstance().getClass(namespaceName);
                        if (!ObjectValidator.IsEmptyOrNull(classPrototype)) {
                            try {
                                if (Reflection.getInstance().ClassHasInterface(classPrototype, IConnector)) {
                                    const liveResponse : LiveContentResponse = new LiveContentResponse(this.response);
                                    if (!ObjectValidator.IsEmptyOrNull(this.requestProtocol.token) || this.weakAuth) {
                                        const instance : any = this.getConnectorForToken(this.requestProtocol.token, namespaceName);
                                        if (!ObjectValidator.IsEmptyOrNull(instance)) {
                                            switch (this.requestProtocol.type) {
                                            case "LiveContentWrapper.InvokeMethod": {
                                                memberName += "_Extern";
                                                const invokeMethod : any = ($entity : any) : void => {
                                                    args.push(liveResponse);
                                                    const promise : IAuthPromise = $entity[memberName].apply($entity, args);
                                                    if (!ObjectValidator.IsEmptyOrNull(promise) &&
                                                        !ObjectValidator.IsEmptyOrNull(promise.auth) &&
                                                        !ObjectValidator.IsEmptyOrNull(promise.resolve)) {
                                                        if (this.isAuthorizedFor(this.requestProtocol.token, promise.auth)) {
                                                            promise.resolve.apply($entity, []);
                                                        } else {
                                                            this.requestProtocol.status = HttpStatusType.NOT_AUTHORIZED;
                                                            liveResponse.Send(this.requestProtocol);
                                                        }
                                                    }
                                                };
                                                if (ObjectValidator.IsFunction(classPrototype[memberName])) {
                                                    invokeMethod(classPrototype);
                                                } else if (ObjectValidator.IsFunction(instance[memberName])) {
                                                    invokeMethod(instance);
                                                } else {
                                                    liveResponse.OnError("Required method \"" + memberName + "\" " +
                                                        "does not exist in class \"" + namespaceName + "\".");
                                                }

                                                break;
                                            }
                                            case "LiveContentWrapper.setProperty": {
                                                if (ObjectValidator.IsSet(classPrototype[memberName])) {
                                                    classPrototype[memberName] = args[0];
                                                    liveResponse.Send(true);
                                                } else {
                                                    liveResponse.OnError("Required property \"" + memberName + "\" " +
                                                        "does not exist in class \"" + namespaceName + "\".");
                                                }
                                                break;
                                            }
                                            case "LiveContentWrapper.getProperty": {
                                                if (ObjectValidator.IsSet(classPrototype[memberName])) {
                                                    liveResponse.Send(classPrototype[memberName]);
                                                } else {
                                                    liveResponse.OnError("Required property \"" + memberName + "\" " +
                                                        "does not exist in class \"" + namespaceName + "\".");
                                                }
                                                break;
                                            }
                                            case "LiveContentWrapper.AddEventHandler": {
                                                liveResponse.OnError("Unsupported \"AddEventHandler\" request.");
                                                break;
                                            }
                                            case "LiveContentWrapper.getEvent": {
                                                liveResponse.OnError("Unsupported \"getEvent\" request.");
                                                break;
                                            }
                                            default: {
                                                liveResponse.OnError("Unsupported live content request type: " +
                                                    "\"" + this.requestProtocol.type + "\"");
                                            }
                                            }
                                            metrics.End(HttpStatusType.SUCCESS, true);
                                        } else {
                                            metrics.End(HttpStatusType.ERROR, true);
                                            this.response.OnError("Unable to load class \"" + namespaceName + "\"");
                                        }
                                    } else {
                                        LogIt.Debug("No token found, reject as not authorized");
                                        this.requestProtocol.status = HttpStatusType.NOT_AUTHORIZED;
                                        liveResponse.Send(this.requestProtocol);
                                    }
                                } else {
                                    metrics.End(HttpStatusType.NOT_AUTHORIZED, true);
                                    this.response.OnError("Unable to call any method from required class \"" + namespaceName +
                                        "\" due to security restrictions.");
                                }
                            } catch (ex) {
                                metrics.Error(ex);
                                this.response.OnError(ex);
                            }
                        } else {
                            metrics.End(HttpStatusType.ERROR, true);
                            this.response.OnError("Required class \"" + namespaceName + "\" does not exist.");
                        }
                    } else {
                        if (this.requestProtocol.type !== "FireEvent") {
                            if (this.requestProtocol.type === "AddEventListener") {
                                LogIt.Info("Events hook for: " + (<ILiveContentProtocol>data).name);
                            } else {
                                LogIt.Info("Resolving request: " + this.requestProtocol.type);
                            }
                        }

                        switch (this.requestProtocol.type) {
                        case "AddEventListener":
                            if (ObjectValidator.IsEmptyOrNull(LiveContentResolver.eventSubscribers)) {
                                LiveContentResolver.eventSubscribers = <any>{};
                            }
                            if (!LiveContentResolver.eventSubscribers.hasOwnProperty(requestData.name)) {
                                LiveContentResolver.eventSubscribers[requestData.name] = [];
                            }
                            LiveContentResolver.eventSubscribers[requestData.name].push({
                                connector: this.response,
                                protocol : this.requestProtocol
                            });
                            this.response.Send(true);
                            break;

                        case "FireEvent":
                            LiveContentResolver.FireEvent(requestData.name,
                                JSON.parse(Buffer.from(requestData.args, "base64").toString("utf8")));
                            this.response.Send(true);
                            break;

                        default:
                            this.resolveCustomProtocol(this.requestProtocol.type, data, this.response);
                            break;
                        }
                        metrics.End(HttpStatusType.SUCCESS, true);
                    }
                } catch (ex) {
                    metrics.Error(ex);
                    this.response.OnError(ex);
                }
            } else if (ObjectValidator.IsEmptyOrNull(this.requestProtocol.type)) {
                const message : string = "Received message with incorrect protocol: " + this.requestProtocol;
                metrics.Error(message);
                LogIt.Info(message);
                this.response.OnError("Incorrect request data.");
            }
        }

        protected isAuthorizedFor($token : string, $requiredRoles : string | string[]) : boolean {
            // override this method if token authorization should be implemented
            return true;
        }

        protected resolveCustomProtocol($type : string, $args : any, $response : IResponse) : void {
            LogIt.Info("Received undefined protocol: " + this.requestProtocol);
            this.response.OnError("Requested protocol \"" + $type + "\" has not been found.");
        }

        protected getConnectorForToken($token : string, $namespace : string) : BaseConnector {
            let instance : BaseConnector = null;
            if (!ObjectValidator.IsEmptyOrNull($token)) {
                LiveContentResolver.connectorsRegister.forEach(($info : IConnectorInfo) : void => {
                    if ($info.token === $token && $info.namespace === $namespace) {
                        instance = $info.connector;
                    }
                });
                if (ObjectValidator.IsEmptyOrNull(instance)) {
                    LogIt.Debug("Constructing new connector {0} for {1}", $namespace, $token);
                    const prototype : any = Reflection.getInstance().getClass($namespace);
                    instance = new prototype();

                    LiveContentResolver.connectorsRegister.push({token: $token, namespace: $namespace, connector: instance});
                }
            } else {
                instance = Reflection.getInstance().getClass($namespace).getInstance();
            }

            return instance;
        }
    }

    export abstract class IEventSubscriber {
        public protocol : IWebServiceProtocol;
        public connector : IRequestConnector;
    }

    export abstract class IConnectorInfo {
        public token : string;
        public namespace : string;
        public connector : BaseConnector;
    }
}
