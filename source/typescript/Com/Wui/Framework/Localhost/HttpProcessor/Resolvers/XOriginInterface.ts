/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.Resolvers {
    "use strict";
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class XOriginInterface extends BaseHttpResolver {

        protected resolver() : void {
            const EOL : string = StringUtils.NewLine(false);
            StaticPageContentManager.Clear(true);
            StaticPageContentManager.Title("WUI - REST services");
            StaticPageContentManager.License(
                "<!--" + EOL +
                EOL +
                "Copyright (c) 2010-2013 Jakub Cieslar" + EOL +
                "Copyright (c) 2014-2016 Freescale Semiconductor, Inc." + EOL +
                "Copyright (c) 2017-2019 NXP" + EOL +
                EOL +
                "SPDX-License-Identifier: BSD-3-Clause" + EOL +
                "The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution" + EOL +
                "or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText" + EOL +
                EOL +
                "-->"
            );
            StaticPageContentManager.getHeadLinks().Clear();
            StaticPageContentManager.HeadScriptAppend("/resource/javascript/xservices.js?version=" +
                this.getEnvironmentArgs().getProjectVersion());
            StaticPageContentManager.BodyAppend("WUI Cross-origin REST service interface");
            StaticPageContentManager.Draw();
        }
    }
}
