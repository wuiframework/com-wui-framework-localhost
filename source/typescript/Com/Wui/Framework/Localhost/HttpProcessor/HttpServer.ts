/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IWebServiceConfigurationProtocol = Com.Wui.Framework.Commons.Interfaces.IWebServiceConfigurationProtocol;
    import IWebServiceProtocol = Com.Wui.Framework.Services.Interfaces.IWebServiceProtocol;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import IProject = Com.Wui.Framework.Localhost.Interfaces.IProject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IResponseProtocol = Com.Wui.Framework.Localhost.Interfaces.IResponseProtocol;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import WebResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.WebResponse;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import ConnectorResponse = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.ConnectorResponse;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import IRequestConnector = Com.Wui.Framework.Localhost.Interfaces.IRequestConnector;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import HttpMethodType = Com.Wui.Framework.Commons.Enums.HttpMethodType;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import EventsManager = Com.Wui.Framework.Localhost.Events.EventsManager;
    import HttpServerEventType = Com.Wui.Framework.Localhost.Enums.HttpServerEventType;
    import HttpServerEventArgs = Com.Wui.Framework.Localhost.Events.Args.HttpServerEventArgs;
    import IHttpServerEvents = Com.Wui.Framework.Localhost.Interfaces.Events.IHttpServerEvents;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import IServerConfigurationSSL = Com.Wui.Framework.Localhost.Interfaces.IServerConfigurationSSL;
    import Metrics = Com.Wui.Framework.Localhost.Utils.Metrics;
    import CertsManager = Com.Wui.Framework.Localhost.Utils.CertsManager;
    import CertsConnector = Com.Wui.Framework.Services.Connectors.CertsConnector;
    import Resources = Com.Wui.Framework.Services.DAO.Resources;
    import Modules = Types.NodeJS.Modules;

    export class HttpServer extends BaseObject {
        private ports : number[];
        private httpServers : any[];
        private wsServers : any[];
        private readonly clients : any[];
        private responseStatus : number[];
        private readonly responseBuffer : IResponse[];
        private environment : EnvironmentArgs;
        private readonly properties : IProject;
        private onCloseHandler : () => void;
        private responsePersistence : IPersistenceHandler;
        private events : EventsManager;
        private fileSystem : FileSystemHandler;
        private ssl : any;
        private readonly tunnels : any;
        private certsPort : number;

        public static SendData($request : HttpRequestParser, $response : WebResponse, $status : HttpStatusType,
                               $headers : any, $data : string) : void {
            if (!Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed() && !ObjectValidator.IsEmptyOrNull($response)) {
                $response.Send({
                    body   : $data.replace(/<body(.*)>/gm, "<body>"),
                    headers: $headers,
                    status : $status
                });
            }
        }

        constructor() {
            super();
            this.onCloseHandler = () : void => {
                // default handler
            };
            this.ports = [];
            this.httpServers = [];
            this.wsServers = [];
            this.clients = [];
            this.responseBuffer = <any>{};
            this.responseStatus = [];
            this.environment = Loader.getInstance().getEnvironmentArgs();
            this.properties = Loader.getInstance().getAppConfiguration();
            this.responsePersistence = PersistenceFactory.getPersistence(ConnectorResponse.ClassName());
            this.events = new EventsManager();
            this.fileSystem = Loader.getInstance().getFileSystemHandler();
            this.ssl = {
                cert: null,
                key : null
            };
            this.tunnels = {};
            if (!ObjectValidator.IsEmptyOrNull(this.properties.reverseProxy) &&
                !ObjectValidator.IsEmptyOrNull(this.properties.reverseProxy.tunnels)) {
                this.tunnels = this.properties.reverseProxy.tunnels;
            }
            this.certsPort = -1;
        }

        public setOnCloseHandler($handler : () => void) : void {
            this.onCloseHandler = $handler;
        }

        public Start() : void {
            this.getCerts(this.properties.domain.location, ($cert : string, $key : string) : void => {
                this.ssl.cert = $cert;
                this.ssl.key = $key;
                if (this.properties.reverseProxy.enabled) {
                    this.createProxy(true, true);
                    if (this.properties.reverseProxy.withServer) {
                        let registerTunnel : boolean = true;
                        this.getEvents().OnStart(() : void => {
                            if (registerTunnel) {
                                registerTunnel = false;
                                this.tunnels[this.properties.domain.location] = {
                                    port: this.ports[this.ports.length - 1],
                                    ssl : {
                                        cert: this.ssl.cert,
                                        key : this.ssl.key
                                    }
                                };
                            }
                        });
                        this.createServer(0, false, true,
                            [Loader.getInstance().getProgramArgs().ProjectBase()]);
                    }
                } else {
                    this.createServer(this.properties.domain.ports.http, false, true);
                    this.createServer(this.properties.domain.ports.https, true, true,
                        [Loader.getInstance().getProgramArgs().ProjectBase()]);
                }
            });
        }

        public Stop() : void {
            this.wsServers.forEach(($server : any) : void => {
                try {
                    $server.shutDown();
                } catch (ex) {
                    LogIt.Warning("Unable to stop WS server. " + ex.stack);
                }
            });
            this.httpServers.forEach(($server : any) : void => {
                try {
                    $server.close();
                } catch (ex) {
                    LogIt.Warning("Unable to stop HTTP server. " + ex.stack);
                }
            });
            this.wsServers = [];
            this.httpServers = [];
            this.ports = [];
        }

        public getEvents() : IHttpServerEvents {
            return {
                OnConnect   : ($handler : any) : void => {
                    this.events.setEvent(this.getClassName(), HttpServerEventType.ON_CONNECT, $handler);
                },
                OnDisconnect: ($handler : any) : void => {
                    this.events.setEvent(this.getClassName(), HttpServerEventType.ON_DISCONNECT, $handler);
                },
                OnStart     : ($handler : any) : void => {
                    this.events.setEvent(this.getClassName(), EventType.ON_START, $handler);
                }
            };
        }

        protected getCerts($domain : string, $callback : ($cert : string, $key : string) => void) : void {
            $domain = StringUtils.Remove($domain, "https://", "http://");
            const handler : CertsManager = new CertsManager($domain, this.getTunnel($domain));
            const paths : IServerConfigurationSSL = handler.getCertPaths();
            if (this.fileSystem.Exists(paths.certificatePath) && this.fileSystem.Exists(paths.privateKeyPath)) {
                $callback(<string>this.fileSystem.Read(paths.certificatePath), <string>this.fileSystem.Read(paths.privateKeyPath));
            } else if (this.properties.certs.external) {
                new CertsConnector()
                    .getCertsFor($domain)
                    .Then(($cert : string, $key : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($cert)) {
                            this.fileSystem.Write(paths.certificatePath, $cert);
                        }
                        if (!ObjectValidator.IsEmptyOrNull($key)) {
                            this.fileSystem.Write(paths.privateKeyPath, $key);
                        }
                        $callback($cert, $key);
                    });
            } else {
                let createServer : boolean = false;
                if (this.certsPort <= 0) {
                    this.certsPort = this.properties.certs.port;
                    if (this.ports.indexOf(this.certsPort) === -1) {
                        createServer = true;
                    } else if (this.ports.indexOf(this.certsPort) !== -1 && !this.properties.reverseProxy.withServer) {
                        createServer = true;
                        this.certsPort = 0;
                    }
                } else if (this.ports.indexOf(this.certsPort) === -1) {
                    createServer = true;
                }
                let serverStarted : boolean = false;
                const handleCerts : any = () : void => {
                    handler.Create(($status : boolean, $cert : any, $key : any) : void => {
                        if (serverStarted) {
                            const index : number = this.ports.indexOf(this.certsPort);
                            if (index > -1) {
                                try {
                                    this.httpServers[index].close();
                                    this.httpServers.splice(index, 1);
                                    this.ports.splice(index, 1);
                                } catch (ex) {
                                    LogIt.Warning("Unable to stop ACME server. " + ex.stack);
                                }
                            }
                        }
                        if ($status) {
                            $callback($cert, $key);
                        } else {
                            $callback(null, null);
                        }
                    });
                };
                if (createServer) {
                    LogIt.Debug("> create certs server at: " + this.certsPort);
                    if (this.certsPort === 0) {
                        this.getEvents().OnStart(() : void => {
                            if (this.certsPort === 0) {
                                this.certsPort = this.ports[this.ports.length - 1];
                                handleCerts();
                            }
                        });
                    }
                    this.createServer(this.certsPort, false, false);
                    serverStarted = true;
                    if (this.certsPort > 0) {
                        handleCerts();
                    }
                } else {
                    handleCerts();
                }
            }
        }

        protected createServer($port : number, $isSecure : boolean, $includeWebSocket : boolean, $configPaths : string[] = []) : void {
            const metrics : Metrics = Metrics.getInstance();
            const url : Types.NodeJS.url = require("url");
            const path : Types.NodeJS.path = require("path");

            let config : IWebServiceConfigurationProtocol = <any>{};
            const protocolName : string = "wuihost";
            const token : string = "";
            const hostName : string = Loader.getInstance().getHttpManager().getRequest().getServerIP();
            let protocol : string;

            let httpServer : Modules.http.Server;
            if ($isSecure) {
                protocol = "https://";
                if (!ObjectValidator.IsEmptyOrNull(this.ssl.cert) && !ObjectValidator.IsEmptyOrNull(this.ssl.key)) {
                    httpServer = require("https").createServer({
                        cert: this.ssl.cert,
                        key : this.ssl.key
                    });
                } else {
                    LogIt.Warning("Failed to start HTTPS server. " +
                        "Certificates has not been found, please validate server configuration.");
                }
            } else {
                protocol = "http://";
                httpServer = require("http").createServer();
            }
            if (!ObjectValidator.IsEmptyOrNull(httpServer)) {
                this.httpServers.push(httpServer);

                httpServer.on("request", ($request : any, $response : any) : void => {
                    this.clients.push($response);
                    const clientId : number = this.clients.length - 1;

                    let requestProtocol : string = protocol;
                    if (!ObjectValidator.IsEmptyOrNull($request.headers.referer)) {
                        requestProtocol = StringUtils.Contains($request.headers.referer, "https://") ? "https://" : "http://";
                    }
                    $request.protocol = requestProtocol;
                    let origin : string = requestProtocol + config.address;
                    if (config.port !== 80 && config.port !== 433) {
                        origin += ":" + config.port;
                    }

                    let requestUrl : string = url.parse($request.url).pathname;
                    if (StringUtils.OccurrenceCount(requestUrl, "/resource/") > 1) {
                        requestUrl = StringUtils.Substring(requestUrl, StringUtils.IndexOf(requestUrl, "/resource/", false));
                    }
                    if (!this.isRequestAllowed($request, hostName, requestProtocol, $port)) {
                        this.sendHttpResponse(
                            clientId,
                            400,
                            {},
                            "Request is not allowed",
                            $request.headers["accept-encoding"]);
                        LogIt.Debug("rejected {0} request: {1}", $request.method, requestUrl);
                        return;
                    } else {
                        LogIt.Debug("{0} request: {1}", $request.method, requestUrl);
                        metrics.Start(origin + requestUrl, $request.method);
                    }

                    try {
                        if (this.isRequestWithBody($request)) {
                            let messageData : string = "";
                            $request.on("data", ($message : string) : void => {
                                messageData += $message.toString();
                            });
                            $request.on("end", () : void => {
                                this.resolveHttpRequest(requestUrl, messageData, clientId, $request, origin, token);
                                metrics.End();
                            });
                        } else if ($request.method === HttpMethodType.OPTIONS && StringUtils.StartsWith(requestUrl, "/xorigin")) {
                            this.sendHttpResponse(
                                clientId,
                                HttpStatusType.SUCCESS,
                                {
                                    /* tslint:disable: object-literal-sort-keys */
                                    "access-control-allow-origin" : requestProtocol + hostName,
                                    "access-control-allow-methods": "POST, GET, OPTIONS",
                                    "access-control-allow-headers": "X-PINGOTHER, Content-Type",
                                    "access-control-max-age"      : 86400,
                                    "content-length"              : 0,
                                    "content-type"                : "text/plain"
                                    /* tslint:enable */
                                },
                                "",
                                $request.headers["accept-encoding"]);
                            metrics.End();
                        } else {
                            this.resolveHttpRequest(requestUrl, config, clientId, $request, origin, token);
                            metrics.End();
                        }
                    } catch (ex) {
                        this.sendHttpResponse(
                            clientId,
                            HttpStatusType.ERROR,
                            {},
                            "Internal server error: " + ex.stack,
                            $request.headers["accept-encoding"]);
                    }
                });

                httpServer.once("error", ($error : any) : void => {
                    if ($error.code === "EADDRINUSE" || $error.code === "EACCES") {
                        LogIt.Error("Cannot start server, because required port " + $port + " is already in use.");
                    }
                });

                httpServer.listen($port, () : void => {
                    if (ObjectValidator.IsEmptyOrNull(this.properties.domain.location)) {
                        this.properties.domain.location = hostName;
                    }
                    const port : number = (<Modules.net.AddressInfo>httpServer.address()).port;
                    this.ports.push(port);
                    config = <IWebServiceConfigurationProtocol>{
                        address    : this.properties.domain.location,
                        base       : "/" + protocolName,
                        location   : __dirname,
                        port,
                        responseUrl: protocol + this.properties.domain.location + ":" + port + "/{0}/response.jsonp",
                        timeout    : httpServer.timeout / 1000
                    };
                    const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                    const configFileName : string =
                        "connector.config" + (Loader.getInstance().getProgramArgs().IsDebug() ? ".js" : ".jsonp");
                    const createdConfigs : string[] = [];
                    $configPaths.forEach(($path : string) : void => {
                        let directory : string = $path;
                        if (this.fileSystem.Exists(directory) && !this.fileSystem.IsDirectory(directory)) {
                            directory = path.dirname($path);
                        }
                        if (this.fileSystem.Exists(directory)) {
                            if (!StringUtils.EndsWith(directory, "/" + configFileName)) {
                                $path = directory + "/" + configFileName;
                            }
                            if (createdConfigs.indexOf($path) === -1) {
                                if (fileSystem.Write($path, "JsonpData(" + JSON.stringify(config) + ");")) {
                                    createdConfigs.push($path);
                                } else {
                                    LogIt.Warning("Failed to create config file at: \"" + $path + "\"");
                                }
                            }
                        } else {
                            LogIt.Warning("Creation of config file at \"" + $path + "\" skipped: path does not exist");
                        }
                    });

                    LogIt.Info(($isSecure ? "SECURE server" : "Server") + " " +
                        this.environment.getAppName() + " has been started at: " +
                        protocol + ($isSecure ? this.properties.domain.location : hostName) + ":" + port);
                    this.events.FireEvent(this.getClassName(), EventType.ON_START);
                });

                if ($includeWebSocket) {
                    const targetPath : string = Loader.getInstance().getProgramArgs().TargetBase();
                    const configPath : string = targetPath + "/resource/libs/WuiConnector/connector.config.jsonp";
                    let configData : any;
                    if (this.fileSystem.Exists(configPath) && this.properties.connectorProxy) {
                        try {
                            let config : string = this.fileSystem.Read(configPath).toString();
                            config = StringUtils.Substring(config,
                                StringUtils.IndexOf(config, "({") + 1,
                                StringUtils.IndexOf(config, "});", false) + 1);
                            configData = JSON.parse(config);
                        } catch (ex) {
                            LogIt.Error("Unable to parse " + configPath, ex);
                        }
                    }
                    if (!ObjectValidator.IsEmptyOrNull(configData)) {
                        httpServer.on("upgrade", ($serverRequest : any, $serverResponse : any, $serverHead : any) : void => {
                            LogIt.Debug("Started target WS communication at: {0}",
                                "ws://" + configData.address + ":" + configData.port + $serverRequest.url);
                            this.createWsProxyClient($serverRequest, $serverResponse, $serverHead,
                                this.getRequestOptions($serverRequest, {
                                    location: "http://" + configData.address,
                                    port    : configData.port
                                }, {
                                    origin: configData.address
                                }));
                        });
                    } else {
                        const webSocketServer : any = require("websocket").server;
                        const wsServer : any = new webSocketServer({
                            autoAcceptConnections : false,
                            httpServer,
                            maxReceivedFrameSize  : 1024 * 1024,
                            maxReceivedMessageSize: 1024 * 1024 * 1024
                        });
                        this.wsServers.push(wsServer);

                        wsServer.on("request", ($request : any) : void => {
                            if (!this.isRequestAllowed($request, hostName, protocol, $port)) {
                                $request.reject();
                                LogIt.Info((new Date()) + " Connection from origin \"" + $request.origin + "\" rejected.");
                                return;
                            }

                            const connection : any = $request.accept(protocolName, $request.origin);
                            this.clients.push(connection);
                            const clientId : number = this.clients.length - 1;
                            LogIt.Info((new Date()) + " Connection accepted.");
                            this.events.FireEvent(this.getClassName(), HttpServerEventType.ON_CONNECT, new HttpServerEventArgs(clientId));

                            const threadsRegister : number[] = [];
                            let isChunked : boolean = false;
                            const processRawData : any = ($data : string) : void => {
                                isChunked = false;
                                let headers : any = $request.httpRequest.headers;
                                if (ObjectValidator.IsEmptyOrNull(headers)) {
                                    headers = {};
                                }

                                if (!ObjectValidator.IsEmptyOrNull(headers.referer)) {
                                    $isSecure = StringUtils.Contains(headers.referer, "https://");
                                }
                                let address : string = config.address;
                                let port : number = config.port;
                                if (!ObjectValidator.IsEmptyOrNull(headers.host)) {
                                    port = $isSecure ? 443 : 80;
                                    if (!ObjectValidator.IsEmptyOrNull(headers["x-forwarded-host"])) {
                                        address = headers["x-forwarded-host"];
                                    }
                                    if (StringUtils.Contains(address, ":")) {
                                        port = StringUtils.ToInteger(StringUtils.Substring(address,
                                            StringUtils.IndexOf(address, ":") + 1));
                                        address = StringUtils.Substring(address,
                                            0, StringUtils.IndexOf(address, ":"));
                                    }
                                }
                                let origin : string = (!$isSecure ? "ws://" : "wss://") + address;
                                if (port !== 80 && port !== 433) {
                                    origin += ":" + port;
                                }
                                threadsRegister.push(setTimeout(() : void => {
                                    try {
                                        this.resolveRequest("/xorigin", $data, {
                                            Send($data : any) : void {
                                                try {
                                                    if (!ObjectValidator.IsString($data)) {
                                                        $data.origin = origin;
                                                        $data.token = token;
                                                        $data = JSON.stringify($data);
                                                    }
                                                    connection.sendUTF($data);
                                                } catch (ex) {
                                                    metrics.Error(ex);
                                                    LogIt.Info(ex.stack);
                                                }
                                            },
                                            getOwnerId: () : string => {
                                                return clientId + "";
                                            }
                                        }, $request, true);
                                    } catch (ex) {
                                        metrics.Error(ex);
                                        connection.sendUTF(JSON.stringify(this.getErrorProtocol(ex, origin, token)));
                                    }
                                }));
                            };

                            let messageLength : number = -1;
                            let chunkLength : number = -1;
                            let messageData : string = "";
                            const readChunk : any = ($data : string) : void => {
                                let initStream : boolean = true;
                                if (messageLength === -1) {
                                    const info : string[] = $data.split("\n");
                                    messageLength = parseInt(info[0], 10);
                                    chunkLength = parseInt(info[1], 10);
                                    messageData = "";
                                } else if (messageLength > chunkLength && messageData.length < messageLength) {
                                    messageData += $data;
                                    if (messageData.length >= messageLength) {
                                        initStream = false;
                                    }
                                } else {
                                    messageData = $data;
                                    initStream = false;
                                }

                                if (messageData.length >= messageLength) {
                                    messageData = messageData.substr(0, messageLength);
                                    messageLength = -1;
                                    processRawData(messageData);
                                } else if (!initStream) {
                                    setTimeout(() : void => {
                                        const errorMessage : string =
                                            "Received message with incorrect length: " + messageData.length;
                                        metrics.Error(errorMessage);
                                        LogIt.Info(errorMessage);
                                        connection.sendUTF(JSON.stringify(this.getErrorProtocol(errorMessage, origin, token)));
                                    });
                                }
                                if (!initStream) {
                                    connection.sendUTF("OK");
                                }
                            };

                            connection.on("message", ($message : any) : any => {
                                if ($message.type === "utf8") {
                                    if ($message.utf8Data === "Server.Stop") {
                                        wsServer.shutDown();
                                        this.onCloseHandler();
                                    } else {
                                        if (!isChunked && messageLength === -1 && $message.utf8Data.endsWith("\n35000")) {
                                            isChunked = true;
                                        }
                                        if (isChunked) {
                                            readChunk($message.utf8Data);
                                        } else {
                                            processRawData($message.utf8Data);
                                            connection.sendUTF("OK");
                                        }
                                    }
                                }
                            });

                            connection.on("close", ($reasonCode : number, $description : string) : void => {
                                try {
                                    threadsRegister.forEach(($threadId : number) : void => {
                                        clearTimeout($threadId);
                                    });
                                    let responseId : string;
                                    for (responseId in this.responseBuffer) {
                                        if (this.responseBuffer.hasOwnProperty(responseId) && StringUtils.StartsWith(responseId, "res_")) {
                                            if (this.responseBuffer[responseId].getOwnerId() === this.clients.indexOf(connection) + "") {
                                                LogIt.Info("Abort response: " + responseId);
                                                this.responseBuffer[responseId].Abort();
                                            }
                                        }
                                    }
                                } catch (ex) {
                                    LogIt.Warning(ex.stack);
                                }
                                LogIt.Info("Peer " + connection.remoteAddress + " disconnected. [" + $reasonCode + "]: " + $description);
                                this.events.FireEvent(this.getClassName(),
                                    HttpServerEventType.ON_DISCONNECT, new HttpServerEventArgs(this.clients.indexOf(connection)));
                            });

                            connection.on("error", ($error : any) : void => {
                                metrics.Error($error);
                                LogIt.Warning($error.stack);
                            });
                        });

                        wsServer.on("error", ($error : any) : void => {
                            LogIt.Warning($error);
                        });
                    }
                }
            }
        }

        protected createProxy($isSecure : boolean, $includeWebSocket : boolean) : void {
            const Readable : any = require("stream").Readable;
            const http : Types.NodeJS.http = require("http");
            const https : Types.NodeJS.https = require("https");
            const tls : Types.NodeJS.tls = require("tls");

            const initServer : any = ($isSecure : boolean, $server : Modules.https.Server, $port : number) : void => {
                this.ports.push($port);
                this.httpServers.push($server);

                $server.on("request", ($serverRequest : any, $serverResponse : any) : void => {
                    const proxy : any = ($config : any, $body? : any) : void => {
                        try {
                            let options : any = this.getRequestOptions($serverRequest, $config);
                            if (StringUtils.Contains($serverRequest.url, ".well-known/acme-challenge")) {
                                if (ObjectValidator.IsEmptyOrNull(this.properties.certs.location)) {
                                    this.properties.certs.location = "http://location";
                                }
                                options = this.getRequestOptions($serverRequest, {
                                    location: this.properties.certs.location,
                                    port    : this.certsPort
                                });
                                LogIt.Debug("proxy challenge for: {0}", $config.name);
                            } else {
                                LogIt.Debug("proxy {0}: {1}", $serverRequest.method,
                                    $config.location + ":" + $config.port + $serverRequest.url);
                            }

                            const client : any = (options.protocol === "https:" ? https : http)
                                .request(options, ($targetResponse : any) : void => {
                                    const data : any = [];
                                    $targetResponse.on("data", ($chunk : string) : void => {
                                        data.push($chunk);
                                    });
                                    $targetResponse.on("end", () : void => {
                                        try {
                                            const body : Modules.stream.Readable = new Readable();
                                            body._read = () : void => {
                                                // mock _read
                                            };
                                            body.push(Buffer.concat(data));
                                            body.push(null);
                                            $serverResponse.writeHead($targetResponse.statusCode, $targetResponse.headers);
                                            body.pipe($serverResponse);
                                        } catch (ex) {
                                            LogIt.Warning(ex.stack);
                                        }
                                    });
                                    $targetResponse.on("close", () : void => {
                                        LogIt.Debug("Proxy request closed.");
                                    });
                                    $targetResponse.on("error", ($error : Error) : void => {
                                        LogIt.Warning("Proxy " + $error.stack);
                                    });
                                });
                            $serverRequest.on("aborted", () : void => {
                                client.abort();
                            });
                            client.on("error", ($ex : Error) : void => {
                                LogIt.Warning("Proxy " + $ex.stack);
                            });
                            if (!ObjectValidator.IsEmptyOrNull($body)) {
                                client.write($body);
                            }
                            client.end();
                        } catch (ex) {
                            LogIt.Warning(ex.stack);
                        }
                    };
                    const config : any = this.getTunnel($serverRequest);
                    if (!ObjectValidator.IsEmptyOrNull(config)) {
                        if (this.isRequestWithBody($serverRequest)) {
                            const data : any = [];
                            $serverRequest.on("data", ($chunk : string) : void => {
                                data.push($chunk);
                            });
                            $serverRequest.on("end", () : void => {
                                proxy(config, Buffer.concat(data));
                            });
                        } else {
                            proxy(config);
                        }
                    }
                });
                if ($includeWebSocket) {
                    $server.on("upgrade", ($serverRequest : any, $serverResponse : any, $serverHead : any) : void => {
                        const config : any = this.getTunnel($serverRequest);
                        if (!ObjectValidator.IsEmptyOrNull(config)) {
                            LogIt.Debug("proxy WS: " + config.location + ":" + config.port + $serverRequest.url);
                            this.createWsProxyClient($serverRequest, $serverResponse, $serverHead,
                                this.getRequestOptions($serverRequest, config));
                        }
                    });
                }
                $server.once("error", ($error : any) : void => {
                    if ($error.code === "EADDRINUSE" || $error.code === "EACCES") {
                        LogIt.Error("Cannot start proxy, because required port " + $port + " is already in use.");
                    }
                });
                $server.listen($port, () : void => {
                    LogIt.Debug("Proxy has been started at: " + ($isSecure ? "https://" : "http://") +
                        Loader.getInstance().getHttpManager().getRequest().getServerIP() + ":" + $port);
                });
            };

            initServer(false, http.createServer(), this.properties.domain.ports.http);
            if ($isSecure) {
                initServer(true, https.createServer(<any>{
                    SNICallback: ($hostname : string, $callback : ($error : Error | null, $context : any) => void) : void => {
                        const config : any = this.getTunnel($hostname);
                        if (ObjectValidator.IsEmptyOrNull(config)) {
                            $callback(null, tls.createSecureContext({}));
                        } else {
                            if ((ObjectValidator.IsEmptyOrNull(config.ssl.cert) || ObjectValidator.IsEmptyOrNull(config.ssl.key)) &&
                                !config.requested) {
                                this.getCerts(config.name, ($cert : string, $key : string) : void => {
                                    config.requested = true;
                                    if (!ObjectValidator.IsEmptyOrNull($cert) && !ObjectValidator.IsEmptyOrNull($key)) {
                                        config.ssl.cert = $cert;
                                        config.ssl.key = $key;
                                        $callback(null, tls.createSecureContext(config.ssl));
                                    } else {
                                        LogIt.Warning("Certificates for " + config.name + " has not been found, " +
                                            "please validate proxy configuration.");
                                        $callback(null, tls.createSecureContext({}));
                                    }
                                });
                            } else if (!ObjectValidator.IsEmptyOrNull(config.ssl.cert) && !ObjectValidator.IsEmptyOrNull(config.ssl.key)) {
                                $callback(null, tls.createSecureContext(config.ssl));
                            } else {
                                $callback(null, tls.createSecureContext({}));
                            }
                        }
                    },
                    cert       : this.ssl.cert,
                    key        : this.ssl.key
                }), this.properties.domain.ports.https);
            }
        }

        protected isRequestAllowed($request : any, $hostName : string, $protocol : string, $port : number) : boolean {
            const origins : string[] = [
                "file://",
                $protocol + "127.0.0.1",
                this.properties.domain.location,
                $protocol + this.properties.domain.location,
                $protocol + this.properties.domain.location + ":" + $port,
                $protocol + $hostName,
                $protocol + $hostName + ":" + $port
            ];
            if (!ObjectValidator.IsEmptyOrNull(this.properties.domain.origins)) {
                this.properties.domain.origins.forEach(($origin : string) : void => {
                    if (origins.indexOf($origin) === -1) {
                        origins.push($origin);
                        origins.push($protocol + $origin);
                        origins.push($protocol + $origin + ":" + $port);
                    }
                });
            }
            const origin : string = $request.origin;
            return (ObjectValidator.IsEmptyOrNull(origin) ||
                origins.Contains(origin) ||
                StringUtils.StartsWith(origin, $protocol + "localhost")) &&
                !StringUtils.EndsWith($request.url, ".php");
        }

        protected resolveRequest($url : string, $data : any, $connector : IRequestConnector, $request : any, $keepAlive : boolean) : void {
            try {
                if (StringUtils.StartsWith($url, "/xorigin") &&
                    ObjectValidator.IsString($data) && !ObjectValidator.IsEmptyOrNull($data)) {
                    const requestProtocol : IWebServiceProtocol = JSON.parse($data);
                    const response : IResponse = new WebResponse($connector, requestProtocol);
                    const responseId : string = "res_" + response.getId();
                    const responseExists : boolean = this.responseBuffer.hasOwnProperty(responseId);

                    if (!ObjectValidator.IsEmptyOrNull(requestProtocol.type) && !responseExists) {
                        if ($keepAlive) {
                            this.responseBuffer[responseId] = response;
                        }
                        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs("/liveContent/");
                        args.POST().Add(requestProtocol, "LiveProtocol");
                        args.POST().Add(response, "LiveResponse");
                        args.POST().Add($data, "Protocol");
                        args.POST().Add($connector, "Connector");
                        Loader.getInstance().getHttpResolver().ResolveRequest(args);
                    } else if (responseExists) {
                        this.responseBuffer[responseId].OnMessage(requestProtocol);
                    } else {
                        LogIt.Info("Received message with incorrect protocol: " + $data);
                        response.OnError("Incorrect request data.");
                    }
                } else {
                    let resolveEnabled : boolean = true;
                    if ($url === "/") {
                        const filePath : string = this.fileSystem.NormalizePath(
                            Loader.getInstance().getProgramArgs().TargetBase() + "/index.html");
                        if (this.fileSystem.Exists(filePath)) {
                            LogIt.Debug("Fetch index page from: {0}", filePath);
                            resolveEnabled = false;
                            $connector.Send(require("fs").createReadStream(filePath));
                        }
                    }
                    if (resolveEnabled) {
                        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs($url);
                        args.POST().Add($data, "Protocol");
                        args.POST().Add($connector, "Connector");
                        args.Owner(new HttpRequestParser("", $request));
                        Loader.getInstance().getHttpResolver().ResolveRequest(args);
                    }
                }
            } catch (ex) {
                LogIt.Info("Received message in incorrect format: \"" + $data + "\"");
                LogIt.Info(ex.stack);
                $connector.Send(this.getErrorProtocol("Unable to parse request data."));
            }
        }

        protected getAppConfiguration() : IProject {
            return this.properties;
        }

        private getDefaultHeaders() : any {
            const os : Types.NodeJS.os = require("os");
            const defaultHeaders : any = {
                /* tslint:disable: object-literal-sort-keys */
                "date"        : Convert.TimeToGMTformat(new Date()),
                "server"      :
                    this.environment.getAppName() + "/" + this.environment.getProjectVersion() + " (" + os.platform() + ")",
                "vary"        : "Accept-Encoding, Origin",
                "keep-alive"  : "timeout=2, max=100",
                "connection"  : "Keep-Alive",
                "content-type": "text/html"
                /* tslint:enable */
            };
            this.getDefaultHeaders = () : any => {
                return defaultHeaders;
            };
            return defaultHeaders;
        }

        private serializeHeaders($line : string, $headers : any) : string {
            return Object.keys($headers)
                .reduce(($head : string[], $key : string) : string[] => {
                    const values : string[] = $headers[$key];
                    if (!ObjectValidator.IsArray(values)) {
                        $head.push($key + ": " + values);
                        return $head;
                    }
                    for (const value of values) {
                        $head.push($key + ": " + value);
                    }
                    return $head;
                }, [$line])
                .join("\r\n") + "\r\n\r\n";
        }

        private getTunnel($nameOrRequest : any) : any {
            let name : string;
            if (!ObjectValidator.IsString($nameOrRequest)) {
                if (!ObjectValidator.IsEmptyOrNull($nameOrRequest.headers.host)) {
                    name = $nameOrRequest.headers.host;
                } else if (!ObjectValidator.IsEmptyOrNull($nameOrRequest.headers.origin)) {
                    name = $nameOrRequest.headers.origin;
                }
            } else {
                name = $nameOrRequest;
            }
            name = StringUtils.Remove(name, "https://", "http://");
            if (!ObjectValidator.IsEmptyOrNull(name) && this.tunnels.hasOwnProperty(name)) {
                const output : any = this.tunnels[name];
                output.name = name;
                if (ObjectValidator.IsEmptyOrNull(output.location)) {
                    output.location = "http://localhost";
                }
                if (!ObjectValidator.IsSet(output.ssl)) {
                    output.ssl = {
                        cert: "",
                        key : ""
                    };
                }
                if (ObjectValidator.IsEmptyOrNull(output.port)) {
                    output.port = 80;
                }
                if (!ObjectValidator.IsSet(output.requested)) {
                    output.requested = false;
                }
                return output;
            } else {
                return null;
            }
        }

        private sendHttpResponse($clientId : number, $status : HttpStatusType, $headers : any, $body : any, $encoding : string,
                                 $normalizeHeaders : boolean = false) : void {
            const Readable : any = require("stream").Readable;
            const zlib : any = require("zlib");
            const response : any = this.clients[$clientId];
            if (this.responseStatus.indexOf($clientId) !== -1) {
                LogIt.Warning("Data has been already send.");
            } else {
                this.responseStatus.push($clientId);
                if (ObjectValidator.IsEmptyOrNull($status)) {
                    $status = HttpStatusType.SUCCESS;
                }
                if (ObjectValidator.IsEmptyOrNull($headers)) {
                    $headers = {};
                }
                let stream : Modules.stream.Readable;
                if (ObjectValidator.IsString($body)) {
                    stream = new Readable();
                    stream.push($body);
                    stream.push(null);
                } else {
                    stream = $body;
                }
                const defaultHeaders : any = JSON.parse(JSON.stringify(this.getDefaultHeaders()));
                let key : string;
                for (key in defaultHeaders) {
                    if (!$headers.hasOwnProperty(key) && !$headers.hasOwnProperty(StringUtils.ToLowerCase(key)) &&
                        defaultHeaders.hasOwnProperty(key)) {
                        $headers[key] = defaultHeaders[key];
                    }
                }
                if ($normalizeHeaders) {
                    const normalized : any = {};
                    for (key in $headers) {
                        if ($headers.hasOwnProperty(key)) {
                            normalized[StringUtils.ToLowerCase(key)] = $headers[key];
                        }
                    }
                    $headers = normalized;
                }

                let encodingKey : string = "content-encoding";
                if (ObjectValidator.IsEmptyOrNull($headers[encodingKey])) {
                    encodingKey = "Content-Encoding";
                }
                if (ObjectValidator.IsEmptyOrNull($headers[encodingKey])) {
                    if (StringUtils.Contains($encoding, "gzip")) {
                        $headers[encodingKey] = "gzip";
                        response.writeHead($status, $headers);
                        stream.pipe(zlib.createGzip()).pipe(response);
                    } else if (StringUtils.Contains($encoding, "deflate")) {
                        $headers[encodingKey] = "deflate";
                        response.writeHead($status, $headers);
                        stream.pipe(zlib.createDeflate()).pipe(response);
                    } else {
                        response.writeHead($status, $headers);
                        stream.pipe(response);
                    }
                } else {
                    response.writeHead($status, $headers);
                    stream.pipe(response);
                }
            }
        }

        private resolveHttpRequest($url : string, $data : any, $clientId : number, $request : any, $origin : string,
                                   $token : string) : void {
            try {
                if (ObjectValidator.IsString($data) && StringUtils.StartsWith($data, "jsonData={")) {
                    $data = StringUtils.Substring($data, 9);
                }
                this.resolveRequest($url, $data, {
                    Send      : ($data : IResponseProtocol | IWebServiceProtocol) : void => {
                        const liveProtocol : IWebServiceProtocol = <IWebServiceProtocol>$data;
                        if (ObjectValidator.IsSet(liveProtocol.id)) {
                            liveProtocol.origin = $origin;
                            liveProtocol.token = $token;
                            const data : string = JSON.stringify(liveProtocol);
                            if (!ObjectValidator.IsEmptyOrNull(liveProtocol.clientId)) {
                                this.responsePersistence.Variable(liveProtocol.clientId.toString(), data);
                            }
                            this.sendHttpResponse(
                                $clientId,
                                HttpStatusType.SUCCESS,
                                {"content-type": "application/javascript"},
                                data,
                                $request.headers["accept-encoding"]);
                        } else {
                            let staticProtocol : IResponseProtocol = <IResponseProtocol>$data;
                            if (!ObjectValidator.IsSet(staticProtocol.body)) {
                                staticProtocol = <IResponseProtocol>{
                                    body   : $data,
                                    headers: {},
                                    status : HttpStatusType.SUCCESS
                                };
                            }
                            if (!ObjectValidator.IsSet(staticProtocol.status)) {
                                staticProtocol.status = HttpStatusType.SUCCESS;
                            }
                            if (!ObjectValidator.IsSet(staticProtocol.headers)) {
                                staticProtocol.headers = {};
                            }
                            if (!ObjectValidator.IsSet(staticProtocol.normalize)) {
                                staticProtocol.normalize = false;
                            }
                            if ($request.method === HttpMethodType.HEAD) {
                                staticProtocol.body = "";
                            }
                            this.sendHttpResponse(
                                $clientId,
                                staticProtocol.status,
                                staticProtocol.headers,
                                staticProtocol.body,
                                $request.headers["accept-encoding"],
                                staticProtocol.normalize);
                        }
                    },
                    getOwnerId: () : string => {
                        return $clientId + "";
                    }
                }, $request, false);
            } catch (ex) {
                if (StringUtils.StartsWith($url, "/xorigin")) {
                    this.sendHttpResponse(
                        $clientId,
                        HttpStatusType.SUCCESS,
                        {"content-type": "application/javascript"},
                        JSON.stringify(this.getErrorProtocol(ex, $origin, $token)),
                        $request.headers["accept-encoding"]);
                } else {
                    this.sendHttpResponse(
                        $clientId,
                        HttpStatusType.ERROR,
                        {},
                        "Internal server error: " + ex.stack,
                        $request.headers["accept-encoding"]);
                }
            }
        }

        private setupWsResponse($response : any, $head : any) : void {
            $response.setTimeout(0);
            $response.setNoDelay(true);
            $response.setKeepAlive(true, 0);
            if ($head && $head.length) {
                $response.unshift($head);
            }
        }

        private getRequestOptions($request : any, $config : any, $headers? : any) : any {
            const headers : any = JSON.parse(JSON.stringify($request.headers));
            headers["x-forwarded-host"] = headers.host;
            headers.origin = headers.host;
            headers.referer = ($request.connection.encrypted ? "https://" : "http://") +
                headers.host + $request.url;
            headers.host = StringUtils.Remove($config.location, "https://", "http://") + ":" + $config.port;
            if (!ObjectValidator.IsEmptyOrNull($headers)) {
                Resources.Extend(headers, $headers);
            }

            const options : any = require("url").parse($config.location + ":" + $config.port + $request.url);
            options.method = $request.method;
            options.headers = headers;
            options.rejectUnauthorized = false;
            return options;
        }

        private createWsProxyClient($serverRequest : any, $serverResponse : any, $serverHead : any, $options : any) : void {
            if (ObjectValidator.IsEmptyOrNull($options.headers)) {
                $options.headers = {};
            }
            $options.headers.upgrade = "websocket";
            const onErrorHandler : any = ($error : Error) : void => {
                LogIt.Warning("Proxy " + $error.stack);
            };
            this.setupWsResponse($serverResponse, $serverHead);
            const client : Modules.http.ClientRequest = ($options.protocol === "https:" ? require("https") : require("http"))
                .request($options, ($targetResponse : any) : void => {
                    if (!$targetResponse.upgrade) {
                        $serverResponse.write(this.serializeHeaders("HTTP/" + $targetResponse.httpVersion + " " +
                            $targetResponse.statusCode + " " + $targetResponse.statusMessage, $targetResponse.headers));
                        $targetResponse.pipe($serverResponse);
                    }
                });
            $serverRequest.on("aborted", () : void => {
                client.abort();
            });
            client.on("error", ($error : Error) : void => {
                onErrorHandler($error);
                $serverResponse.end();
            });
            client.on("upgrade", ($targetRequest : any, $targetResponse : any, $targetHead : any) : void => {
                $targetResponse.on("error", ($error : Error) : void => {
                    onErrorHandler($error);
                    $serverResponse.end();
                });
                $serverResponse.on("error", ($error : Error) : void => {
                    onErrorHandler($error);
                    $targetResponse.end();
                });
                this.setupWsResponse($targetResponse, $targetHead);
                $serverResponse.write(this.serializeHeaders("HTTP/1.1 101 Switching Protocols", $targetRequest.headers));
                $targetResponse.pipe($serverResponse).pipe($targetResponse);
            });
            client.end();
        }

        private getErrorProtocol($message : string | Error, $origin? : string, $token? : string) : any {
            let data : any = {};
            if (ObjectValidator.IsString($message)) {
                data = {
                    code   : 0,
                    file   : ObjectEncoder.Base64(__filename),
                    line   : 0,
                    message: ObjectEncoder.Base64(<string>$message),
                    stack  : ""
                };
            } else {
                const ex : any = <Error>$message;
                data = {
                    code   : ex.code,
                    file   : ObjectEncoder.Base64(ex.message),
                    line   : ex.line,
                    message: ObjectEncoder.Base64(ex.message),
                    stack  : ObjectEncoder.Base64(ex.stack)
                };
            }
            if (ObjectValidator.IsEmptyOrNull($origin)) {
                $origin = "http://localhost";
            }
            if (ObjectValidator.IsEmptyOrNull($token)) {
                $token = "";
            }
            return {
                data,
                origin: $origin,
                status: HttpStatusType.ERROR,
                token : $token,
                type  : "Server.Exception"
            };
        }

        private isRequestWithBody($request : any) : boolean {
            if ($request.method === HttpMethodType.POST) {
                return true;
            }
            const knownTypes : string[] = ["Transfer-Encoding", "Content-Length"];
            for (const $type of knownTypes) {
                if (!ObjectValidator.IsEmptyOrNull($request.headers[$type]) ||
                    !ObjectValidator.IsEmptyOrNull($request.headers[StringUtils.ToLowerCase($type)])) {
                    return true;
                }
            }
            return false;
        }
    }
}
