/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor {
    "use strict";
    import HttpRequestConstants = Com.Wui.Framework.Commons.Enums.HttpRequestConstants;
    import EventsManager = Com.Wui.Framework.Localhost.Events.EventsManager;

    export class HttpResolver extends Com.Wui.Framework.Services.HttpProcessor.HttpResolver {

        public CreateRequest($baseUrl? : string) : HttpRequestParser {
            return <HttpRequestParser>super.CreateRequest($baseUrl);
        }

        public getEvents() : EventsManager {
            return <EventsManager>super.getEvents();
        }

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            let indexClass : any = Com.Wui.Framework.Localhost.Controllers.Pages.MainPage;
            if (!Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                indexClass = Com.Wui.Framework.Localhost.Index;
            }
            resolvers["/ServerError/Http/DefaultPage"] = Com.Wui.Framework.Localhost.ErrorPages.Http404NotFoundPage;
            resolvers["/"] = indexClass;
            resolvers["/index"] = indexClass;
            resolvers["/index.html"] = indexClass;
            resolvers["/web/"] = indexClass;

            if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
                return resolvers;
            }

            resolvers["/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}"] =
                Com.Wui.Framework.Localhost.ErrorPages.ExceptionErrorPage;
            resolvers["/ServerError/Http/NotAuthorized"] = Com.Wui.Framework.Services.ErrorPages.AuthHttpResolver;
            resolvers["/ServerError/Http/ConnectionLost"] = Com.Wui.Framework.Services.ErrorPages.ConnectionLostPage;
            resolvers["/wuihost/"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.XOriginInterface;
            resolvers["/xorigin/"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.XOriginInterface;
            resolvers["/xorigin/Request"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.XOriginInterface;
            resolvers["/wuihost/CloseClient"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.ConnectorResponse;
            resolvers["/{clientId}/response.jsonp"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.ConnectorResponse;
            resolvers["connector.config.jsonp"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.ConnectorResponse;
            resolvers["connector.config.js"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.ConnectorResponse;
            resolvers["/resource/javascript/xservices.js"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.XOriginServices;
            resolvers["/package.conf.jsonp"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.FileRequestResolver;
            resolvers["/private.conf.jsonp"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.FileRequestResolver;
            resolvers["/favicon.ico"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.FileRequestResolver;
            resolvers["/liveContent/"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.LiveContentResolver;
            resolvers["/.well-known/acme-challenge/{token}"] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.AcmeResolver;

            const fileExtensions : string[] = [
                "*.htm", "*.html", "*.txt", "*.log", "*.pdf",
                "*.min.css", "*.css",
                "*.min.js", "*.js", "*.json", "*.jsonp", "*.map", "*.ts",
                "*.bmp", "*.jpg", "*.jpe", "*.jpeg", "*.png", "*.gif", "*.ico", "*.icon", "*.svg",
                "*.woff", "*.woff2", "*.eot"
            ];
            for (const $extension of fileExtensions) {
                resolvers["/**/" + $extension] = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.FileRequestResolver;
            }

            return resolvers;
        }

        protected getRequestParserClass() : any {
            return HttpRequestParser;
        }

        protected getHttpManagerClass() : any {
            return HttpManager;
        }

        protected getEventsManagerClass() : any {
            return EventsManager;
        }
    }
}
