/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers {
    "use strict";
    import IResponsePromise = Com.Wui.Framework.Localhost.Interfaces.IResponsePromise;

    export class CallbackResponse extends BaseResponse {

        constructor($owner : (...$args : any[]) => void) {
            super();
            this.owner = $owner;
        }

        public Send(...$args : any[]) : IResponsePromise {
            this.owner.apply(this, $args);
            return {
                Then: ($callback : ($data : any) => void) : void => {
                    this.callbacks.onsend = $callback;
                }
            };
        }

        public OnComplete(...$args : any[]) : IResponsePromise {
            this.owner.apply(this, $args);
            return {
                Then: ($callback : ($data : any) => void) : void => {
                    this.callbacks.oncomplete = $callback;
                }
            };
        }
    }
}
