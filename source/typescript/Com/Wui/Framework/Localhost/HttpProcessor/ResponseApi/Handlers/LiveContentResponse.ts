/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers {
    "use strict";
    import IResponsePromise = Com.Wui.Framework.Localhost.Interfaces.IResponsePromise;
    import ILiveContentProtocol = Com.Wui.Framework.Services.Interfaces.ILiveContentProtocol;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class LiveContentResponse extends WebResponse {

        public Send(...$args : any[]) : IResponsePromise {
            const response : ILiveContentProtocol = <ILiveContentProtocol>{
                args       : "",
                name       : (<ILiveContentProtocol>this.data.data).name,
                returnValue: null
            };
            if (ObjectValidator.IsSet($args[0])) {
                response.returnValue = $args[0];
                $args.shift();
            }
            if ($args.length > 0) {
                response.args = <any>$args;
            }
            if (!ObjectValidator.IsString(response.returnValue)) {
                response.returnValue = JSON.stringify(response.returnValue);
            }
            response.returnValue = Buffer.from(response.returnValue).toString("base64");

            if (!ObjectValidator.IsString(response.args)) {
                response.args = JSON.stringify(response.args);
            }
            response.args = Buffer.from(response.args).toString("base64");

            this.data.data = response;
            this.data.status = HttpStatusType.SUCCESS;
            this.owner.Send(this.data);

            return {
                Then: ($callback : ($data : string | ILiveContentProtocol) => void) : void => {
                    this.callbacks.oncomplete = $callback;
                }
            };
        }
    }
}
