/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IWebServiceProtocol = Com.Wui.Framework.Services.Interfaces.IWebServiceProtocol;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import IResponsePromise = Com.Wui.Framework.Localhost.Interfaces.IResponsePromise;
    import IRequestConnector = Com.Wui.Framework.Localhost.Interfaces.IRequestConnector;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IWebServiceException = Com.Wui.Framework.Services.Interfaces.IWebServiceException;

    export class WebResponse extends BaseResponse {
        protected data : IWebServiceProtocol;

        constructor($owner : IRequestConnector | IResponse, $data? : IWebServiceProtocol) {
            super();
            if ($owner instanceof WebResponse) {
                this.Clone($owner);
            } else {
                this.data = $data;
                this.owner = $owner;
            }
        }

        public Clone($instance : IResponse) : void {
            super.Clone($instance);
            if (ObjectValidator.IsSet((<WebResponse>$instance).data)) {
                this.data = (<WebResponse>$instance).data;
                try {
                    this.data.data = JSON.parse(Buffer.from(<string>this.data.data, "base64").toString("utf8"));
                } catch (ex) {
                    // pass data as is, because they can be already in correct format
                }
            }
        }

        public Send(...$args : any[]) : IResponsePromise {
            this.data.data = Buffer.from(JSON.stringify($args)).toString("base64");
            this.data.status = HttpStatusType.SUCCESS;
            this.owner.Send(this.data);

            return {
                Then: ($callback : ($data : any) => void) : void => {
                    this.callbacks.onsend = $callback;
                }
            };
        }

        public OnStart(...$args : any[]) : IResponsePromise {
            this.Send(this.normalizeArgs($args, "onstart"));
            return {
                Then: ($callback : ($data : any) => void) : void => {
                    this.callbacks.onstart = $callback;
                }
            };
        }

        public OnChange(...$args : any[]) : IResponsePromise {
            this.Send(this.normalizeArgs($args, "onchange"));
            return {
                Then: ($callback : ($data : any) => void) : void => {
                    this.callbacks.onchange = $callback;
                }
            };
        }

        public OnComplete(...$args : any[]) : IResponsePromise {
            this.Send(this.normalizeArgs($args, "oncomplete"));
            return {
                Then: ($callback : ($data : any) => void) : void => {
                    this.callbacks.oncomplete = $callback;
                }
            };
        }

        public OnError($message : string | Error, ...$args : any[]) : void {
            if (ObjectValidator.IsSet((<Error>$message).message)) {
                if (ObjectValidator.IsSet((<any>$message).stack)) {
                    $message = (<any>$message).stack;
                } else {
                    $message = (<Error>$message).message;
                }
            } else if (!ObjectValidator.IsString($message)) {
                $message = JSON.stringify($message);
            }
            if (ObjectValidator.IsEmptyOrNull($message)) {
                $message = "";
            }
            LogIt.Info(<string>$message);
            $message = Buffer.from((<string>$message).replace(/[\n]/gmi, "<br/>")).toString("base64");

            this.data.status = HttpStatusType.ERROR;
            this.data.type = "Request.Exception";
            if (!ObjectValidator.IsEmptyOrNull($args)) {
                this.data.data = <IWebServiceException>{
                    args   : Buffer.from(JSON.stringify($args)).toString("base64"),
                    message: $message
                };
            } else {
                this.data.data = <string>$message;
            }
            this.owner.Send(this.data);
        }

        public OnMessage($data : IWebServiceProtocol) : void {
            let data : any = Buffer.from(<string>$data.data, "base64").toString("utf8");
            try {
                data = JSON.parse(data);
            } catch (ex) {
                // provide data as is
            }
            super.OnMessage(data);
        }

        public getId() : number {
            return this.data.id;
        }

        public getOwnerId() : string {
            return this.owner.getOwnerId();
        }

        private normalizeArgs($args : any[], $type : string) : any {
            let data : any = "";
            if ($args.length > 0) {
                data = $args.length === 1 ? $args[0] : $args;
            }
            return {
                data,
                type: $type
            };
        }
    }
}
