/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import CallbackResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.CallbackResponse;
    import BaseResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.BaseResponse;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ResponseFactory extends BaseObject {

        public static getResponse($callback : ((...$args : any[]) => void) | IResponse) : IResponse {
            let response : IResponse;
            if (!($callback instanceof BaseResponse)) {
                if (ObjectValidator.IsFunction($callback)) {
                    response = new CallbackResponse(<(...$args : any[]) => void>$callback);
                } else {
                    response = new BaseResponse();
                }
            } else {
                response = <IResponse>$callback;
            }
            return response;
        }
    }
}
