/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogLevel = Com.Wui.Framework.Commons.Enums.LogLevel;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ConsoleHandler = Com.Wui.Framework.Commons.IOApi.Handlers.ConsoleHandler;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import HttpServer = Com.Wui.Framework.Localhost.HttpProcessor.HttpServer;
    import IProject = Com.Wui.Framework.Localhost.Interfaces.IProject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HttpResolver = Com.Wui.Framework.Localhost.HttpProcessor.HttpResolver;
    import HttpManager = Com.Wui.Framework.Localhost.HttpProcessor.HttpManager;
    import ProgramArgs = Com.Wui.Framework.Localhost.Structures.ProgramArgs;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import Terminal = Com.Wui.Framework.Localhost.Connectors.Terminal;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import FirewallManager = Com.Wui.Framework.Localhost.Utils.FirewallManager;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import IOHandlerFactory = Com.Wui.Framework.Commons.IOApi.IOHandlerFactory;
    import IOHandlerType = Com.Wui.Framework.Commons.Enums.IOHandlerType;
    import Metrics = Com.Wui.Framework.Localhost.Utils.Metrics;
    import IProjectTarget = Com.Wui.Framework.Localhost.Interfaces.IProjectTarget;

    export class Loader extends Com.Wui.Framework.Services.Loader {
        private fileSystem : FileSystemHandler;
        private terminal : Terminal;
        private server : HttpServer;
        private appArgs : ProgramArgs;
        private logPath : string;
        private logAnonymize : any;
        private isClosing : boolean;

        public static Load($appConfig : string | IProject) : void {
            if (!isBrowser) {
                try {
                    const instance : Loader = new this();
                    (<any>Com.Wui.Framework.Commons.Loader).singleton = instance;
                    instance.isClosing = false;

                    if (process.argv.indexOf("--debug") !== -1) {
                        console.log("Loading application config..."); // tslint:disable-line
                    }
                    instance.Process($appConfig);
                    instance.initDOM();
                    instance.initLogIt();

                    const exitHandler : any = () : void => {
                        instance.Exit();
                    };
                    process.on("exit", exitHandler);
                    process.on("SIGINT", exitHandler);
                    process.on("uncaughtException", ($ex) : void => {
                        Metrics.Error($ex);
                        ExceptionsManager.HandleException($ex);
                    });
                    process.on("warning", ($data : any) : void => {
                        if ($data.name !== "ExperimentalWarning") {
                            LogIt.Warning($data.toString());
                        }
                    });
                    (<any>global).window.close = exitHandler;

                    if (!EnvironmentHelper.IsDetached()) {
                        try {
                            if (EnvironmentHelper.IsTTY()) {
                                require("readline").emitKeypressEvents(process.stdin);
                                process.stdin.setRawMode(true);
                                process.stdin.on("keypress", ($character : string, $key : any) : void => {
                                    if ($key.ctrl && ($key.name === "c" || $key.name === "q" || $key.name === "x")) {
                                        Loader.getInstance().Exit();
                                    }
                                });
                            } else {
                                process.stdin.on("data", ($value : string) : void => {
                                    $value = (<any>$value.toString()).trim();
                                    if ($value === "exit" || $value === "quit") {
                                        Loader.getInstance().Exit();
                                    }
                                });
                            }
                        } catch (ex) {
                            // unable to hook on stdin
                        }
                    }

                    instance.appArgs = instance.initProgramArgs();
                    instance.appArgs.Parse([].concat(process.argv));
                    instance.processProgramArgs();
                } catch (ex) {
                    process.stderr.write("Application start error. " + ex.stack);
                    process.exit(1);
                }
            } else {
                super.Load($appConfig);
            }
        }

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        public getHttpManager() : HttpManager {
            return <HttpManager>super.getHttpManager();
        }

        public getEnvironmentArgs() : EnvironmentArgs {
            return <EnvironmentArgs>super.getEnvironmentArgs();
        }

        public getAppConfiguration() : IProject {
            return this.getEnvironmentArgs().getProjectConfig();
        }

        public getProgramArgs() : ProgramArgs {
            return this.appArgs;
        }

        public getFileSystemHandler() : FileSystemHandler {
            if (!ObjectValidator.IsSet(this.fileSystem)) {
                this.fileSystem = this.initFileSystem();
                this.getFileSystemHandler = () : FileSystemHandler => {
                    return this.fileSystem;
                };
            }
            return this.fileSystem;
        }

        public getTerminal() : Terminal {
            if (!ObjectValidator.IsSet(this.terminal)) {
                this.terminal = this.initTerminal();
                this.getTerminal = () : Terminal => {
                    return this.terminal;
                };
            }
            return this.terminal;
        }

        public setLogPath($path : string) : void {
            if (!ObjectValidator.IsEmptyOrNull($path)) {
                this.logPath = StringUtils.Replace($path, "\\", "/");
                this.pmkdir(StringUtils.Substring(this.logPath, 0, StringUtils.IndexOf(this.logPath, "/", false)));
            }
        }

        public setAnonymizer($patterns? : any) : void {
            if (!ObjectValidator.IsSet(this.logAnonymize)) {
                let anonymizeMap : string[];
                let keyFormat : string;
                if (EnvironmentHelper.IsWindows()) {
                    keyFormat = "%{0}%";
                    anonymizeMap = [
                        "TMP", "USERPROFILE", "HOMEPATH", "USERDOMAIN", "COMPUTERNAME", "USERNAME"
                    ];
                } else {
                    keyFormat = "${0}";
                    anonymizeMap = [
                        "HOME", "HOSTNAME", "LOGNAME", "USER"
                    ];
                }
                const binBase : string = StringUtils.Replace(require("path").normalize(__dirname + "/../.."), "\\", "/");
                const anonymizeValues : any = {
                    BINBASE    : StringUtils.Replace(binBase, "/", "\\"),
                    PROJECTBASE: StringUtils.Replace(this.getCwd(), "/", "\\"),
                    appData    : this.getAppConfiguration().target.appDataPath,
                    binBase,
                    projectBase: this.getCwd()
                };
                anonymizeMap.forEach(($key : string) : void => {
                    if (process.env.hasOwnProperty($key)) {
                        anonymizeValues[$key] = process.env[$key];
                        anonymizeMap.forEach(($key : string) : void => {
                            if (process.env.hasOwnProperty($key)) {
                                anonymizeValues[$key] = process.env[$key];
                                if (StringUtils.Contains(anonymizeValues[$key], "\\")) {
                                    anonymizeValues[StringUtils.ToLowerCase($key)] =
                                        StringUtils.Replace(anonymizeValues[$key], "\\", "/");
                                }
                            }
                        });
                    }
                });
                this.logAnonymize = {
                    format: keyFormat,
                    values: anonymizeValues
                };
            }
            if (!ObjectValidator.IsEmptyOrNull($patterns)) {
                let pattern : string;
                for (pattern in $patterns) {
                    if ($patterns.hasOwnProperty(pattern) && !ObjectValidator.IsEmptyOrNull($patterns[pattern])) {
                        this.logAnonymize.values[pattern] = $patterns[pattern];
                    }
                }
            }
        }

        public Exit($exitCode? : number) : void {
            if (!this.isClosing) {
                this.isClosing = true;
                this.onCloseHandler(($handlerExitCode? : number) : void => {
                    if (ObjectValidator.IsSet($handlerExitCode)) {
                        $exitCode = $handlerExitCode;
                    }
                    process.exit($exitCode);
                });
            }
        }

        protected initProgramArgs() : ProgramArgs {
            return new ProgramArgs();
        }

        protected initEnvironment() : Com.Wui.Framework.Commons.EnvironmentArgs {
            if (isBrowser) {
                return new FrontEndEnvironmentArgs();
            } else {
                return new EnvironmentArgs();
            }
        }

        protected initHttpServer() : HttpServer {
            return new HttpServer();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver("/");
        }

        protected initFileSystem() : FileSystemHandler {
            return new FileSystemHandler();
        }

        protected initTerminal() : Terminal {
            return new Terminal();
        }

        protected processProgramArgs() : boolean {
            if (this.appArgs.IsVerbose() || !this.getEnvironmentArgs().IsProductionMode()) {
                LogIt.setLevel(LogLevel.ALL);
            } else if (this.appArgs.IsDebug()) {
                LogIt.setLevel(LogLevel.DEBUG);
            } else {
                LogIt.setLevel(LogLevel.ERROR);
            }

            const target : IProjectTarget = this.getAppConfiguration().target;
            this.appArgs.BinBase(this.getFileSystemHandler().NormalizePath(__dirname + "/../.."));
            this.appArgs.ProjectBase(this.getCwd());
            this.appArgs.AppDataPath(target.appDataPath);

            if (!StringUtils.Contains(this.appArgs.ProjectBase(), "/build/target")) {
                if (ObjectValidator.IsEmptyOrNull(this.appArgs.AppDataPath())) {
                    this.appArgs.AppDataPath(this.getFileSystemHandler().getTempPath() + "/" + this.getEnvironmentArgs().getProjectName());
                }
            } else {
                this.appArgs.AppDataPath(this.appArgs.ProjectBase());
            }

            if (ObjectValidator.IsEmptyOrNull(this.appArgs.TargetBase())) {
                if (ObjectValidator.IsEmptyOrNull(target.projectBase)) {
                    target.projectBase = this.appArgs.BinBase();
                }
                const fs : Types.NodeJS.fs = require("fs");
                const targetPath : string = target.projectBase;
                if (!ObjectValidator.IsEmptyOrNull(targetPath) && fs.existsSync(targetPath)) {
                    this.appArgs.TargetBase(this.getFileSystemHandler().NormalizePath(StringUtils.Remove(targetPath, "/index.html")));
                } else if (fs.existsSync(__dirname + "/../../../index.html")) {
                    this.appArgs.TargetBase(this.getFileSystemHandler().NormalizePath(__dirname + "/../../.."));
                } else {
                    this.appArgs.TargetBase(this.appArgs.BinBase());
                }
            } else if (StringUtils.EndsWith(this.appArgs.TargetBase(), "index.html")) {
                this.appArgs.TargetBase(StringUtils.Remove(this.getFileSystemHandler().NormalizePath(this.appArgs.TargetBase()),
                    "/index.html"));
            }
            target.projectBase = this.appArgs.TargetBase();

            this.setAnonymizer({
                appData    : this.getProgramArgs().AppDataPath(),
                binBase    : this.getProgramArgs().BinBase(),
                projectBase: this.getProgramArgs().ProjectBase(),
                targetBase : this.getProgramArgs().TargetBase()
            });
            process.env.PATH = this.getTerminal().NormalizeEnvironmentPath(this.getAppEnvironmentPath());

            let processed : boolean = false;
            if (this.appArgs.IsTestRun()) {
                processed = true;
                Echo.Print("Detected --test-run argument: processing of other args will be skipped");
            } else if (this.appArgs.IsHelp()) {
                this.appArgs.IsHelp(false);
                processed = true;
                this.appArgs.setStdinHandler(($data : string) : void => {
                    if (ObjectValidator.IsEmptyOrNull($data)) {
                        this.Exit();
                    } else {
                        Echo.Println("");
                        this.appArgs.Parse(StringUtils.Split($data, " "));
                        this.processProgramArgs();
                    }
                });
                this.appArgs.PrintHelp();
            } else if (this.appArgs.IsVersion()) {
                this.appArgs.PrintVersion();
                this.Exit();
            } else if (this.appArgs.IsPath()) {
                this.appArgs.PrintPath();
                this.Exit();
            } else if (this.appArgs.StartServer() || this.appArgs.RestartServer()) {
                processed = true;
                if (!ObjectValidator.IsEmptyOrNull(this.server)) {
                    this.server.Stop();
                }
                Echo.Print(this.printLogo());
                FirewallManager.AddServiceRule(
                    "NXP",
                    "WUI Framework services",
                    this.getEnvironmentArgs().getAppName(),
                    "Allow incoming network traffic to WUI Framework",
                    this.appArgs.ProjectBase() + "/" + this.getEnvironmentArgs().getAppName(), () : void => {
                        this.server = this.initHttpServer();
                        this.server.setOnCloseHandler(() : void => {
                            LogIt.Info("Server thread has been terminated.");
                            this.Exit();
                        });
                        this.server.Start();
                    });
            } else if (this.appArgs.StopServer()) {
                processed = true;
                if (!ObjectValidator.IsEmptyOrNull(this.server)) {
                    this.server.Stop();
                }
            }
            return processed;
        }

        protected onCloseHandler($done : ($exitCode? : number) => void) : void {
            if (!ObjectValidator.IsEmptyOrNull(this.server)) {
                this.server.Stop();
            }
            $done();
        }

        protected getAppEnvironmentPath() : string {
            return this.appArgs.ProjectBase();
        }

        protected printLogo() : string {
            return "" +
                " _       ____  ______   ______                                             __  \n" +
                "| |     / / / / /  _/  / ____/________ _____ ___  ___ _      ______  _____/ /__\n" +
                "| | /| / / / / // /   / /_  / ___/ __ `/ __ `__ \\/ _ \\ | /| / / __ \\/ ___/ //_/\n" +
                "| |/ |/ / /_/ // /   / __/ / /  / /_/ / / / / / /  __/ |/ |/ / /_/ / /  / ,<   \n" +
                "|__/|__/\\____/___/  /_/   /_/   \\__,_/_/ /_/ /_/\\___/|__/|__/\\____/_/  /_/|_|  \n" +
                "                                                                               \n";
        }

        private initDOM() : void {
            const os : Types.NodeJS.os = require("os");
            const url : Types.NodeJS.url = require("url");
            const LocalStorage : any = require("node-localstorage").LocalStorage;
            WebSocket = require("websocket").w3cwebsocket;
            const {JSDOM, ResourceLoader, VirtualConsole} = require("jsdom");

            let localStoragePath : string;
            const cwd : string = this.getCwd();
            if (StringUtils.Contains(cwd, "/build/target")) {
                localStoragePath = cwd;
            } else {
                localStoragePath = this.getFileSystemHandler().getTempPath() + "/" +
                    this.getEnvironmentArgs().getProjectName();
            }
            localStoragePath += "/localStorage";
            this.pmkdir(localStoragePath);
            const localStorageInstance : typeof LocalStorage = new LocalStorage(localStoragePath);

            const origResourceFetch : typeof ResourceLoader.prototype.fetch = ResourceLoader.prototype.fetch;
            ResourceLoader.prototype.fetch = function ($url : string, $options : any) : any {
                return origResourceFetch
                    .apply(this, [$url, $options])
                    .catch(() : void => {
                        LogIt.Warning("Failed to load resource from: " + $url);
                    });
            };
            const virtualConsole : typeof VirtualConsole = new VirtualConsole();
            virtualConsole.on("jsdomError", ($error : Error) : void => {
                if (!StringUtils.ContainsIgnoreCase($error.message, "Could not load")) {
                    LogIt.Warning($error.stack);
                }
            });
            const dom : typeof JSDOM = new JSDOM("", {
                /* tslint:disable: object-literal-sort-keys */
                url                 : "http://" + this.getHttpManager().getRequest().getServerIP() + "/?" +
                    "AppName=" + ObjectEncoder.Url(this.getEnvironmentArgs().getAppName()) + "&" +
                    "AppPid=" + process.pid + "&" +
                    "Platform=" + this.getEnvironmentArgs().getPlatform() + "&" +
                    "ReleaseName=" + this.getEnvironmentArgs().getReleaseName(),
                contentType         : "text/html",
                userAgent           : "Node.js/" + process.version + " (" + os.platform() + "; rv: " + process.version + ") " +
                    "Chrome/52.0.2743.116",
                includeNodeLocations: true,
                resources           : new ResourceLoader(),
                runScripts          : "dangerously",
                virtualConsole
                /* tslint:enable */
            });
            this.getEnvironmentArgs().getNamespaces().forEach(($namespace : string) : void => {
                const parts : string[] = StringUtils.Split($namespace, ".");
                if (parts.length >= 1 && global.hasOwnProperty(parts[0])) {
                    dom.window[parts[0]] = global[parts[0]];
                }
            });
            dom.window.scrollTo = () : void => {
                // default handler
            };
            (<any>dom.window).localStorage = localStorageInstance;
            (<any>dom.window).JsonpData = JsonpData;
            (<any>dom.window).WebSocket = WebSocket;
            (<any>window).location = {
                __href: ""
            };
            Object.defineProperty(window.location, "href", {
                get() : string {
                    return this.__href;
                },
                set($value : string) : void {
                    this.__href = $value;
                    const parsedUrl : any = url.parse($value);
                    let property : string;
                    for (property in parsedUrl) {
                        if (parsedUrl.hasOwnProperty(property) && property !== "href") {
                            this[property] = parsedUrl[property];
                        }
                    }
                }
            });
            let domProperty : string;
            for (domProperty in dom.window) {
                if (domProperty !== "location") {
                    window[domProperty] = dom.window[domProperty];
                }
            }
            for (domProperty in dom.window.location) {
                if (dom.window.location.hasOwnProperty(domProperty) && domProperty !== "href") {
                    window.location[domProperty] = dom.window.location[domProperty];
                }
            }
            localStorage = localStorageInstance;

            document = window.document;
            (<any>global).window = window;
            (<any>global).document = document;
            Object.keys((<any>global).window).forEach(($property : string) : void => {
                if (typeof global[$property] === "undefined") {
                    global[$property] = (<any>global).window[$property];
                }
            });

            const element : HTMLElement = document.createElement("span");
            element.id = "Content";
            document.body.appendChild(element);
        }

        private initLogIt() : void {
            const fs : Types.NodeJS.fs = require("fs");
            const os : Types.NodeJS.os = require("os");
            const date : Date = new Date();
            const year : string = date.getFullYear() + "";
            const day : string = date.getDate() < 10 ? "0" + date.getDate() : date.getDate() + "";
            const monthNumber : number = date.getMonth() + 1;
            const month : string = monthNumber < 10 ? "0" + monthNumber : monthNumber + "";
            this.setLogPath(this.getCwd() + "/log/" + year + "/" + month + "/" + day + "_" + month + "_" + year + ".txt");

            let BAR : string = EnvironmentHelper.IsWindows() ? "█" : "▇";
            let FAIL : string = "×";
            let OK : string = "✓";
            let ARROW : string = "→";
            if (!EnvironmentHelper.IsTTY()) {
                BAR = "=";
                FAIL = "x";
                OK = "+";
                ARROW = ">";
            }
            const stdoutWrite : any = ($message : string) : void => {
                try {
                    $message = StringUtils.Replace($message, "[__BAR__]", BAR);
                    $message = StringUtils.Replace($message, "[__FAIL__]", FAIL);
                    $message = StringUtils.Replace($message, "[__OK__]", OK);
                    $message = StringUtils.Replace($message, "[__ARROW__]", ARROW);
                    process.stdout.write($message);
                } catch (ex) {
                    console.log($message); // tslint:disable-line
                }
            };

            const colors : any = require("colors");
            let logsBackup : string = "";
            LogIt.Init(LogLevel.ALL, <any>{
                Init() : void {
                    // default Init
                },
                NewLineType() : string {
                    return os.EOL;
                },
                setOnPrint() : void {
                    // default setOnPrint
                },
                Print: ($message : string) : void => {
                    if ($message !== "" && ExceptionsManager.getAll().IsEmpty()) {
                        $message += os.EOL;
                        stdoutWrite($message);
                        $message = logsBackup + colors.stripColors($message);
                        try {
                            fs.appendFileSync(this.logPath, $message);
                            logsBackup = "";
                        } catch (ex) {
                            if (!fs.existsSync(this.logPath)) {
                                this.setLogPath(this.logPath);
                                try {
                                    fs.appendFileSync(this.logPath, $message);
                                    logsBackup = "";
                                } catch (ex) {
                                    logsBackup += $message;
                                }
                            }
                        }
                    }
                },
                IsTypeOf($type : any) : boolean {
                    return $type === ConsoleHandler;
                }
            });

            this.setAnonymizer();
            const stackTrace : any = require("stack-trace");
            (<any>LogIt).formatter = ($type : LogLevel, $message : string) : string => {
                const traceback : any = stackTrace.get();
                let at : string = traceback[3].getFunctionName();
                if (ObjectValidator.IsEmptyOrNull(at)) {
                    at = "anonymous";
                }
                let from : string;
                for (let index : number = 4; index < traceback.length; index++) {
                    from = traceback[index].getFunctionName();
                    if (ObjectValidator.IsEmptyOrNull(from)) {
                        from = "anonymous";
                    }
                    if (at !== from) {
                        break;
                    }
                }
                let key : string;
                for (key in this.logAnonymize.values) {
                    if (!this.appArgs.IsDebug() && this.logAnonymize.values.hasOwnProperty(key) &&
                        StringUtils.Contains($message, this.logAnonymize.values[key])) {
                        $message = StringUtils.Replace($message,
                            this.logAnonymize.values[key], StringUtils.Format(this.logAnonymize.format, key));
                    }
                }
                $message = StringUtils.Format("{0}: {1} at {2} from {3}{4}{5}{6}{4}",
                    LogLevel[$type], Convert.TimeToGMTformat(new Date()), at, from,
                    os.EOL, StringUtils.Tab(1, false),
                    $message);
                if ($type === LogLevel.ERROR || $type === LogLevel.WARNING) {
                    if ($type === LogLevel.ERROR) {
                        Metrics.Error($message);
                    }
                    stdoutWrite($message);
                    return "";
                } else {
                    return $message;
                }
            };
            Echo.Init(IOHandlerFactory.getHandler(IOHandlerType.CONSOLE), true);
            (<any>Echo).output.Print = stdoutWrite;
            Echo.Println = ($message : string) : void => {
                Echo.Print($message + os.EOL);
            };
        }

        private getCwd() : string {
            let cwd : string = EnvironmentHelper.getNodejsRoot();
            if (StringUtils.Contains(cwd, "/dependencies/nodejs")) {
                cwd = StringUtils.Replace(cwd, "/dependencies/nodejs", "/build/target");
            }
            this.getCwd = () : string => {
                return cwd;
            };
            return cwd;
        }

        private pmkdir($path : any) : void {
            const fs : Types.NodeJS.fs = require("fs");
            if (!fs.existsSync($path)) {
                $path.split("/").reduce(($currentPath : string, $folder : string) : string => {
                    $currentPath += $folder + "/";
                    if (!fs.existsSync($currentPath)) {
                        fs.mkdirSync($currentPath);
                    }
                    return $currentPath;
                }, "");
            }
        }
    }
}
