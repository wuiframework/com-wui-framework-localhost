/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Controllers.Pages {
    "use strict";
    import BaseHttpResolver = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.BaseHttpResolver;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;

    export class MainPage extends BaseHttpResolver {

        protected resolver() : void {
            const EOL : string = StringUtils.NewLine(false);
            StaticPageContentManager.Clear();
            StaticPageContentManager.Title("WUI Framework Localhost");
            StaticPageContentManager.FaviconSource("/resource/graphics/icon.ico");
            StaticPageContentManager.License(
                "<!--" + EOL +
                EOL +
                "Copyright (c) 2010-2013 Jakub Cieslar" + EOL +
                "Copyright (c) 2014-2016 Freescale Semiconductor, Inc." + EOL +
                "Copyright (c) 2017-2019 NXP" + EOL +
                EOL +
                "SPDX-License-Identifier: BSD-3-Clause" + EOL +
                "The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution" + EOL +
                "or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText" + EOL +
                EOL +
                "-->"
            );
            if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
                StaticPageContentManager.BodyAppend("" +
                    "<div class=\"" + StringUtils.Remove(this.getNamespaceName(), ".") + "\">" + EOL +
                    "   <div class=\"" + this.getClassNameWithoutNamespace() + "\">" + EOL +
                    "       <div class=\"Logo\"></div>" + EOL +
                    "       <div class=\"Note\">" + EOL +
                    "Localhost version: " + this.getEnvironmentArgs().getProjectVersion() + ", " +
                    "build: " + this.getEnvironmentArgs().getBuildTime() + EOL +
                    "       </div>" + EOL +
                    "   </div>" + EOL +
                    "</div>");
            }
            StaticPageContentManager.Draw();
        }
    }
}
