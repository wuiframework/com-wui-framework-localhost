/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.IOApi.Handlers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ScriptHandler extends Com.Wui.Framework.Commons.IOApi.Handlers.ScriptHandler {

        public Load() : void {
            const fs : Types.NodeJS.fs = require("fs");
            const path : Types.NodeJS.path = require("path");
            const strip : any = require("strip-json-comments");
            const vm : Types.NodeJS.vm = require("vm");

            if (ObjectValidator.IsEmptyOrNull(this.Path()) && ObjectValidator.IsEmptyOrNull(this.Data())) {
                this.errorHandler(new Error("Path or body for resource must be defined before load."));
            } else {
                const executeScript : boolean = ObjectValidator.IsEmptyOrNull(this.Path());
                let filepath : string = "";
                if (!executeScript) {
                    filepath = path.normalize(this.Path());
                }

                const execute : any = () : void => {
                    try {
                        const reader : any = ($data : any) : void => {
                            this.Data($data);
                        };
                        (<any>Com.Wui.Framework.Localhost).DAO = {Resources: {Data: reader}};
                        Com.Wui.Framework.Localhost.EnvironmentArgs.AppConfigData = reader;
                        const sandbox : any = {
                            Com
                        };
                        vm.createContext(sandbox);
                        vm.runInContext(this.Data(), sandbox, filepath);
                        this.successHandler();
                    } catch (ex) {
                        this.errorHandler(ex);
                    }
                };

                if (executeScript) {
                    execute();
                } else if (fs.existsSync(filepath)) {
                    this.Data(fs.readFileSync(filepath).toString());
                    if (StringUtils.EndsWith(filepath, ".json")) {
                        try {
                            this.Data(JSON.parse(strip(this.Data())));
                            this.successHandler();
                        } catch (ex) {
                            this.errorHandler(ex);
                        }
                    } else if (StringUtils.EndsWith(filepath, ".jsonp") || StringUtils.EndsWith(filepath, ".js")) {
                        execute();
                    } else {
                        this.errorHandler(new Error("Unsupported script extension."));
                    }
                } else {
                    this.errorHandler(new Error("Unable to find script."));
                }
            }
        }
    }
}
