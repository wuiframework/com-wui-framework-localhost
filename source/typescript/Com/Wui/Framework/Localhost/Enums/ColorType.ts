/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class ColorType extends BaseEnum {
        public static readonly YELLOW : string = "yellow";
        public static readonly RED : string = "red";
        public static readonly GREEN : string = "green";
    }
}

if (!isBrowser) {
    Object.defineProperty(String.prototype, "underline", {
        get() : string {
            return require("colors").underline(this.toString());
        }
    });
    [
        Com.Wui.Framework.Localhost.Enums.ColorType.YELLOW,
        Com.Wui.Framework.Localhost.Enums.ColorType.RED,
        Com.Wui.Framework.Localhost.Enums.ColorType.GREEN
    ].forEach(($value : string) : void => {
        const colors : any = require("colors");
        Object.defineProperty(String.prototype, $value, {
            get() : string {
                return colors[$value](this.toString());
            }
        });
    });
}
