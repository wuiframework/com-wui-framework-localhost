/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Interfaces {
    "use strict";

    export interface IConnector extends Com.Wui.Framework.Commons.Interfaces.IBaseObject {
        getAuthorizedMethods() : string[];

        setAuthorizedMethods($methodList : string[]);
    }
}
