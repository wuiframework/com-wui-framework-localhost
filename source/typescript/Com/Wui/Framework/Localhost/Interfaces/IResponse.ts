/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Interfaces {
    "use strict";
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;

    export interface IResponse {
        Send(...$args : any[]) : IResponsePromise;

        OnStart(...$args : any[]) : IResponsePromise;

        OnChange(...$args : any[]) : IResponsePromise;

        OnComplete(...$args : any[]) : IResponsePromise;

        OnError($message : string | Error, ...$args : any[]) : void;

        OnMessage($data : any) : void;

        FireEvent($name : string, $args? : any) : void;

        getId() : number;

        getOwnerId() : string;

        AddAbortHandler($handler : () => void) : void;

        Abort() : void;
    }

    export interface IResponsePromise {
        Then($callback : (...$args : any[]) => void) : void;
    }

    export abstract class IResponseProtocol {
        public status : HttpStatusType;
        public headers : any;
        public body : any;
        public normalize? : boolean;
    }

    export abstract class IAuthPromise {
        public resolve : () => void;
        public auth : string | string[];
    }
}
