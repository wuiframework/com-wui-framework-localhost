/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Interfaces {
    "use strict";

    export abstract class IProject extends Com.Wui.Framework.Commons.Interfaces.IProject {
        public domain : IServerConfigurationDomain;
        public httpProxy : string;
        public reverseProxy : IReverseProxyConfiguration;
        public connectorProxy : boolean;
        public certs : ICertsConfiguration;
        public target : IProjectTarget;
    }

    export abstract class IProjectTarget extends Com.Wui.Framework.Commons.Interfaces.IProjectTarget {
        public projectBase : string;
        public appDataPath : string;
        public requestPatterns : string[];
        public metrics : IMetricsConfiguration;
    }

    export abstract class IServerConfigurationDomain {
        public location : string;
        public ports : IServerConfigurationPorts;
        public ssl : IServerConfigurationSSL | string;
        public origins : string[];
    }

    export abstract class IServerConfigurationPorts {
        public http : number;
        public https : number;
    }

    export abstract class IServerConfigurationSSL {
        public certificatePath : string;
        public privateKeyPath : string;
    }

    export abstract class IMetricsConfiguration {
        public enabled : boolean;
        public location : string;
        public port : number;
    }

    export abstract class ICertsConfiguration {
        public external : boolean;
        public mail : string;
        public port : number;
        public location : string;
    }

    export abstract class IReverseProxyConfiguration {
        public enabled : boolean;
        public withServer : boolean;
        public tunnels : IProxyTunnelsConfiguration[];
    }

    export abstract class IProxyTunnelsConfiguration {
        public port : number;
        public ssl : IServerConfigurationSSL | string;
        public location : string;
    }
}
