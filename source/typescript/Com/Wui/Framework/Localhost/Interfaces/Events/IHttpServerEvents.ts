/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Interfaces.Events {
    "use strict";
    import HttpServerEventArgs = Com.Wui.Framework.Localhost.Events.Args.HttpServerEventArgs;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    export interface IHttpServerEvents {
        OnStart($eventHandler : ($eventArgs? : EventArgs) => void) : void;

        OnConnect($eventHandler : ($eventArgs? : HttpServerEventArgs) => void) : void;

        OnDisconnect($eventHandler : ($eventArgs? : HttpServerEventArgs) => void) : void;
    }
}
