/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
declare let nodejsRoot : string;

function define() : any {
    // amd loader fallback
}

let isBrowser : boolean = true;
try {
    if (require) {
        isBrowser = false;
    }
} catch (ex) {
    // should behave like browser
}
if (isBrowser) {
    require = <any>(() : any => {
        return {};
    });
    module = <any>{
        exports: {
            Load() : void {
                // loader declare
            }
        }
    };
} else {
    WebSocket = require("websocket").w3cwebsocket; // tslint:disable-line
    (<any>WebSocket).setLogItClass = () : void => {
        // fallback for older builder
    };
    if (process.env.IS_WUI_DETACHED_PROCESS) {
        process.stdout = <any>{
            write: console.log // tslint:disable-line
        };
        process.stderr = <any>{
            write: console.error // tslint:disable-line
        };
    }
}

namespace Com.Wui.Framework.Localhost.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class EnvironmentHelper extends BaseObject {

        public static IsTTY() : boolean {
            let status : boolean = false;
            if (!this.IsDetached() && process.stdout.isTTY) {
                try {
                    process.stdin.setRawMode(true);
                    status = true;
                } catch ($ex) {
                    // runtime has not been detected as TTY
                }
            }
            EnvironmentHelper.IsTTY = () : boolean => {
                return status;
            };
            return status;
        }

        public static IsWindows() : boolean {
            const os : Types.NodeJS.os = require("os");
            const status : boolean = os.platform().toString() === "win32" || os.platform().toString() === "win64";
            EnvironmentHelper.IsWindows = () : boolean => {
                return status;
            };
            return status;
        }

        public static IsLinux() : boolean {
            const status : boolean = !EnvironmentHelper.IsWindows() && !EnvironmentHelper.IsMac();
            EnvironmentHelper.IsLinux = () : boolean => {
                return status;
            };
            return status;
        }

        public static IsMac() : boolean {
            const status : boolean = require("os").platform() === "darwin";
            EnvironmentHelper.IsMac = () : boolean => {
                return status;
            };
            return status;
        }

        public static Is64bit() : boolean {
            const status : boolean =
                StringUtils.Contains(process.arch, "64") || process.env.hasOwnProperty("PROCESSOR_ARCHITEW6432") ||
                StringUtils.Contains(require("os").arch(), "64") ||
                EnvironmentHelper.IsMac();
            EnvironmentHelper.Is64bit = () : boolean => {
                return status;
            };
            return status;
        }

        public static IsArm() : boolean {
            const status : boolean = StringUtils.ContainsIgnoreCase(require("os").arch(), "arm");
            EnvironmentHelper.IsArm = () : boolean => {
                return status;
            };
            return status;
        }

        public static getCores() : number {
            const cores : number = require("os").cpus().length;
            EnvironmentHelper.getCores = () : number => {
                return cores;
            };
            return cores;
        }

        public static IsElevated($callback : ($status : boolean) => void) : void {
            let status : boolean = false;
            const isElevated : any = require("is-elevated");
            isElevated()
                .then(($elevated : boolean) : void => {
                    status = $elevated;
                    EnvironmentHelper.IsElevated = ($callback : ($status : boolean) => void) : void => {
                        $callback(status);
                    };
                    $callback(status);
                });
        }

        public static IsEmbedded() : boolean {
            const status : boolean = StringUtils.StartsWith(__dirname, "/snapshot/");
            this.IsEmbedded = () : boolean => {
                return status;
            };
            return status;
        }

        public static IsDetached() : boolean {
            const status : boolean = !ObjectValidator.IsEmptyOrNull(process.env.IS_WUI_DETACHED_PROCESS) &&
                <any>process.env.IS_WUI_DETACHED_PROCESS === 1;
            this.IsDetached = () : boolean => {
                return status;
            };
            return status;
        }

        public static getNodejsRoot() : string {
            return nodejsRoot;
        }
    }
}
