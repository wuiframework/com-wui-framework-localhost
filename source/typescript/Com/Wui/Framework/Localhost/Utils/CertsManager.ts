/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Utils {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IProject = Com.Wui.Framework.Localhost.Interfaces.IProject;
    import IServerConfigurationSSL = Com.Wui.Framework.Localhost.Interfaces.IServerConfigurationSSL;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ProgramArgs = Com.Wui.Framework.Localhost.Structures.ProgramArgs;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;

    export class CertsManager extends BaseObject {
        private greenlock : any;
        private fileSystem : FileSystemHandler;
        private properties : IProject;
        private appArgs : ProgramArgs;
        private readonly domain : string;
        private readonly config : IServerConfigurationSSL;

        public static getInstance() : CertsManager {
            const instance : CertsManager = new CertsManager();
            this.getInstance = () : CertsManager => {
                return instance;
            };
            return instance;
        }

        constructor($domain? : string, $config? : IServerConfigurationSSL) {
            super();
            const instance : Loader = Loader.getInstance();
            this.fileSystem = instance.getFileSystemHandler();
            this.properties = instance.getAppConfiguration();
            this.appArgs = instance.getProgramArgs();
            try {
                this.greenlock = require("greenlock").create({
                    server : this.appArgs.IsDebug() ?
                        "https://acme-staging-v02.api.letsencrypt.org/directory" : "https://acme-v02.api.letsencrypt.org/directory",
                    store  : require("greenlock-store-fs"),
                    version: "draft-12"
                });
                if (ObjectValidator.IsEmptyOrNull($domain)) {
                    this.domain = this.properties.domain.location;
                } else {
                    this.domain = $domain;
                }
                if (ObjectValidator.IsEmptyOrNull($config)) {
                    this.config = <IServerConfigurationSSL>{
                        certificatePath: "",
                        privateKeyPath : ""
                    };
                } else {
                    this.config = $config;
                }
            } catch (ex) {
                LogIt.Warning(ex.stack);
                this.config = <IServerConfigurationSSL>{
                    certificatePath: "",
                    privateKeyPath : ""
                };
            }
        }

        public Create($callback : ($status : boolean, $cert? : string, $key? : string) => void) : void {
            const domains : string[] = [this.domain];
            const generate : any = () : void => {
                LogIt.Debug("Generating certs for: {0}", this.domain);
                this.greenlock
                    .register({
                        agreeTos   : true,
                        debug      : this.appArgs.IsDebug(),
                        domains,
                        email      : this.properties.certs.mail,
                        webrootPath: this.getStoragePath()
                    })
                    .then(($results : any) : void => {
                        try {
                            $callback(true, $results.cert + $results.chain, $results.privkey);
                        } catch (ex) {
                            LogIt.Warning(ex.stack);
                            $callback(false);
                        }
                    }, ($error : Error) : void => {
                        LogIt.Warning($error.stack);
                        $callback(false);
                    });
            };
            if (!this.appArgs.IsDebug()) {
                LogIt.Debug("Check certs validity for: {0}", this.domain);
                this.greenlock.check({domains}).then(($results : any) : void => {
                    if ($results) {
                        LogIt.Debug("> Certs are valid for: {0}", this.domain);
                        $callback(true, $results.cert + $results.chain, $results.privkey);
                    } else {
                        generate();
                    }
                });
            } else {
                generate();
            }
        }

        public getStoragePath() : string {
            return this.appArgs.AppDataPath() + "/resource/data/acme-challenge";
        }

        public getCertPaths() : IServerConfigurationSSL {
            const output : IServerConfigurationSSL = this.config;

            const binBase : string = this.appArgs.BinBase();
            const targetBase : string = this.appArgs.TargetBase();

            if (ObjectValidator.IsObject(this.properties.domain.ssl)) {
                const ssl : IServerConfigurationSSL = (<IServerConfigurationSSL>this.properties.domain.ssl);
                output.certificatePath = ssl.certificatePath;
                if (!this.fileSystem.Exists(output.certificatePath)) {
                    output.certificatePath = binBase + "/" + ssl.certificatePath;
                }
                if (!this.fileSystem.Exists(output.certificatePath)) {
                    output.certificatePath = targetBase + "/" + ssl.certificatePath;
                }
                output.privateKeyPath = ssl.privateKeyPath;
                if (!this.fileSystem.Exists(output.privateKeyPath)) {
                    output.privateKeyPath = binBase + "/" + ssl.privateKeyPath;
                }
                if (!this.fileSystem.Exists(output.privateKeyPath)) {
                    output.privateKeyPath = targetBase + "/" + ssl.privateKeyPath;
                }
            } else {
                let sslBasePath : string = <string>this.properties.domain.ssl;
                if (!this.fileSystem.Exists(sslBasePath)) {
                    sslBasePath = binBase + "/" + sslBasePath;
                }
                if (!this.fileSystem.Exists(sslBasePath)) {
                    sslBasePath = targetBase + "/" + sslBasePath;
                }

                const domain : string = StringUtils.Replace(this.domain, ".", "_");

                output.certificatePath = sslBasePath + "/" + domain + ".crt";
                if (!this.fileSystem.Exists(output.certificatePath)) {
                    const sslBasePathCp : string = sslBasePath + "/" + this.domain;
                    output.certificatePath = sslBasePathCp + "/" + domain + ".crt";
                    if (!this.fileSystem.Exists(output.certificatePath)) {
                        output.certificatePath = sslBasePathCp + "/fullchain.pem";
                    }
                }

                output.privateKeyPath = sslBasePath + "/" + domain + ".key";
                if (!this.fileSystem.Exists(output.privateKeyPath)) {
                    const sslBasePathKp : string = sslBasePath + "/" + this.domain;
                    output.privateKeyPath = sslBasePathKp + "/" + domain + ".key";
                    if (!this.fileSystem.Exists(output.privateKeyPath)) {
                        output.privateKeyPath = sslBasePathKp + "/privkey.pem";
                    }
                }
            }

            this.getCertPaths = () : IServerConfigurationSSL => {
                return output;
            };
            return output;
        }
    }
}
