/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IMetricsConfiguration = Com.Wui.Framework.Localhost.Interfaces.IMetricsConfiguration;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    export class Metrics extends BaseObject {
        private static apm : any;
        private instrumented : string[];
        private readonly isStarted : boolean;

        public static getInstance() : Metrics {
            const instance : Metrics = new Metrics();
            this.getInstance = () : Metrics => {
                return instance;
            };
            return instance;
        }

        public static Error($message : string | Error) : void {
            this.getInstance().Error($message);
        }

        constructor() {
            super();
            this.instrumented = [];
            this.isStarted = false;
            const config : IMetricsConfiguration = Loader.getInstance().getAppConfiguration().target.metrics;
            const serverUrl : string = "http://" + config.location + ":" + config.port;
            try {
                if (config.enabled) {
                    if (ObjectValidator.IsEmptyOrNull(Metrics.apm)) {
                        Metrics.apm = require("elastic-apm-node").start({
                            active          : true,
                            frameworkName   : "WUI Framework",
                            frameworkVersion: ">= 2019.0",
                            ignoreUrls      : [/^\//i],
                            serverUrl,
                            serviceName     : Loader.getInstance().getEnvironmentArgs().getAppName(),
                            serviceVersion  : Loader.getInstance().getEnvironmentArgs().getProjectVersion()
                        });
                    }
                    LogIt.Info("Started sending of monitoring data to: " + serverUrl);
                    this.isStarted = true;
                    this.Instrument();
                } else {
                    LogIt.Debug("Server monitoring has been disable for this instance.");
                }
            } catch (ex) {
                LogIt.Warning("Failed to start monitoring agent.\n" + ex.stack);
            }
        }

        public Instrument($namespaces? : any[]) : void {
            if (Loader.getInstance().getAppConfiguration().target.metrics.enabled && this.isStarted) {
                LogIt.Debug("Instrumenting code for monitoring...");
                try {
                    if (ObjectValidator.IsEmptyOrNull($namespaces)) {
                        $namespaces = [];
                    }
                    $namespaces.push(
                        Com.Wui.Framework.Commons.Interfaces.IHttpRequestResolver,
                        Com.Wui.Framework.Localhost.Connectors
                    );
                    const include : string[] = [];
                    const collectClasses : any = ($namespaces : any[]) : void => {
                        $namespaces.forEach(($type : any) : void => {
                            if (!ObjectValidator.IsString($type)) {
                                if ($type.hasOwnProperty("ClassName")) {
                                    $type = $type.ClassName();
                                } else {
                                    const subnamespaces : any[] = [];
                                    let property : string;
                                    for (property in $type) {
                                        if ($type.hasOwnProperty(property)) {
                                            subnamespaces.push($type[property]);
                                        }
                                    }
                                    collectClasses(subnamespaces);
                                }
                            }
                            if (include.indexOf($type) === -1) {
                                include.push($type);
                            }
                        });
                    };
                    collectClasses($namespaces);
                    let classesCount : number = 0;
                    let methodsCount : number = 0;
                    const wrapMethods : any = ($api : any, $class, $type : string) : void => {
                        let property : string;
                        for (property in $api) {
                            if ($api.hasOwnProperty(property) && ObjectValidator.IsFunction($api[property]) &&
                                this.instrumented.indexOf($class + "." + property + "[" + $type + "]") === -1) {
                                const origMethod : any = $api[property];
                                const trace : string = $class + "." + property;
                                $api[property] = function (...$args : any[]) : any {
                                    const span : any = Metrics.apm.startSpan(trace, $type);
                                    const returnValue : any = origMethod.apply(this, $args);
                                    if (span) {
                                        span.end();
                                    }
                                    return returnValue;
                                };
                                methodsCount++;
                                this.instrumented.push($class + "." + property + "[" + $type + "]");
                            }
                        }
                    };
                    const reflection : Reflection = Reflection.getInstance();
                    reflection.getAllClasses().forEach(($class : string) : void => {
                        let instrument : boolean = false;
                        include.forEach(($type : string) : void => {
                            if ($class === $type || reflection.ClassHasInterface($class, $type)) {
                                instrument = true;
                            }
                        });
                        if (instrument) {
                            const definition : any = reflection.getClass($class);
                            wrapMethods(definition, $class, "static");
                            wrapMethods(definition.prototype, $class, "instance");
                            classesCount++;
                        }
                    });
                    LogIt.Debug("Instrumented {0} methods in {1} monitored classes.",
                        methodsCount.toString(), classesCount.toString());
                } catch (ex) {
                    LogIt.Warning(ex.stack);
                }
            }
        }

        public Start($owner : string, $type : string) : void {
            if (this.isStarted) {
                Metrics.apm.startTransaction($owner, $type);
            }
        }

        public End($result? : HttpStatusType, $force : boolean = false) : void {
            if (this.isStarted) {
                if (ObjectValidator.IsEmptyOrNull($result)) {
                    $result = HttpStatusType.SUCCESS;
                }
                Metrics.apm.setCustomContext({
                    heap: process.memoryUsage()
                });
                Metrics.apm.endTransaction($result);
                if ($force) {
                    Metrics.apm.flush(($error : any) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($error)) {
                            LogIt.Warning($error.stack);
                        }
                    });
                }
            }
        }

        public Error($message : string | Error) : void {
            if (this.isStarted) {
                Metrics.apm.captureError($message);
            }
        }
    }
}
