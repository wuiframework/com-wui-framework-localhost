/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Utils {
    "use strict";
    import ColorType = Com.Wui.Framework.Localhost.Enums.ColorType;
    import StdinType = Com.Wui.Framework.Localhost.Enums.StdinType;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class StdinManager extends BaseObject {
        private questions : StdinQuestion[];
        private question : StdinQuestion;
        private passwordCharacter : string;
        private index : number;
        private prefixLength : number;
        private cursorPosition : number;
        private onCompleteHandler : () => void;
        private onKeyPressHandler : ($value : string, $skipped : boolean) => void;

        public static Process($questions : StdinQuestion[], $callback : () => void) : void {
            const manager : StdinManager = this.getInstance();
            manager.setQuestions($questions);
            manager.setOnComplete($callback);
            manager.NextQuestion();
        }

        public static getInstance() : StdinManager {
            const instance : StdinManager = new StdinManager();
            this.getInstance = () : StdinManager => {
                return instance;
            };
            return instance;
        }

        constructor() {
            super();
            this.questions = [];
            this.index = 0;
            this.prefixLength = 0;
            this.cursorPosition = 0;
            this.passwordCharacter = "*";
            this.onCompleteHandler = () : void => {
                // default handler
            };
            this.onKeyPressHandler = ($value : string, $skipped : boolean) : void => {
                if (!$skipped) {
                    process.stdout.write(" >>"[ColorType.YELLOW] + " processed");
                    this.question.callback(this.question.value, ($status : boolean) : void => {
                        if ($status) {
                            this.NextQuestion();
                        } else {
                            process.stdout.write("Unable to process cmd input."[ColorType.RED]);
                        }
                    });
                } else {
                    process.stdout.write(" >>"[ColorType.RED] + " skipped");
                    this.NextQuestion();
                }
            };
            this.init();
        }

        public setPasswordCharacter($value : string) : void {
            this.passwordCharacter = Property.String(this.passwordCharacter, $value);
        }

        public setQuestions($value : StdinQuestion[]) : void {
            this.index = 0;
            this.questions = $value;
        }

        public setOnComplete($callback : () => void) : void {
            if (ObjectValidator.IsFunction($callback)) {
                this.onCompleteHandler = $callback;
            }
        }

        public setOnKeyPress($callback : ($value : string, $skipped : boolean) => void) : void {
            if (ObjectValidator.IsFunction($callback)) {
                this.onKeyPressHandler = $callback;
            }
        }

        public NextQuestion() : void {
            if (this.index < this.questions.length) {
                this.question = this.questions[this.index];
                this.index++;
                if (!process.env.IS_WUI_PROXY_TASK && !EnvironmentHelper.IsTTY() && this.question.hidden) {
                    process.stdout.write(this.question.prefix + ":");
                    process.stdout.write(" >>"[ColorType.RED] + " skipped. " +
                        "Hidden stdin can be processed only inside a text terminal (\"TTY\") context.");
                    this.NextQuestion();
                } else {
                    this.question.value = "";
                    process.stdin.setEncoding("utf8");
                    const prefix : string = this.question.prefix + ": ";
                    if (process.env.IS_WUI_PROXY_TASK) {
                        if (this.question.hidden) {
                            process.stdout.write(prefix + StdinType.PASS);
                        } else {
                            process.stdout.write(prefix + StdinType.STRING);
                        }
                    } else {
                        this.prefixLength = prefix.length;
                        this.cursorPosition = this.prefixLength;
                        process.stdout.write(prefix);
                        process.stdin.resume();
                    }
                }
            } else {
                this.onCompleteHandler();
            }
        }

        private init() : void {
            if (EnvironmentHelper.IsTTY()) {
                const readline : Types.NodeJS.readline = require("readline");
                readline.emitKeypressEvents(process.stdin);
                process.stdin.setRawMode(true);

                const moveTo : any = ($index : number) : void => {
                    readline.cursorTo(process.stdout, $index);
                    this.cursorPosition = $index;
                };

                const printLine : any = ($message : string) : void => {
                    readline.clearLine(process.stdout, 0);
                    readline.cursorTo(process.stdout, 0);
                    let line : string = this.question.prefix + ": ";
                    if (this.question.hidden) {
                        line += Array($message.length + 1).join(this.passwordCharacter);
                    } else {
                        line += $message;
                    }
                    process.stdout.write(line);
                    readline.cursorTo(process.stdout, this.cursorPosition);
                };

                const removeAtCursorPosition : any = () : void => {
                    if (this.question.value.length > 0) {
                        const index : number = this.cursorPosition - this.prefixLength + 1;
                        this.question.value = this.question.value.substring(0, index - 1) +
                            this.question.value.substring(index, this.question.value.length);
                        printLine(this.question.value);
                    }
                };

                const getEndPosition : any = () : number => {
                    return this.prefixLength + this.question.value.length;
                };

                process.stdin.on("keypress", ($character : string, $key : any) : void => {
                    if ($key.ctrl && $key.name === "c" && EnvironmentHelper.IsDetached()) {
                        Loader.getInstance().Exit();
                    } else if ($key.name === "escape" || $key.name === "return" && this.question.value === "") {
                        moveTo(getEndPosition());
                        this.onKeyPressHandler("", true);
                    } else if ($key.name === "return") {
                        moveTo(getEndPosition());
                        this.onKeyPressHandler(this.question.value + "", false);
                    } else if ($key.name === "left") {
                        this.cursorPosition--;
                        if (this.cursorPosition < this.prefixLength) {
                            this.cursorPosition = this.prefixLength;
                        }
                        moveTo(this.cursorPosition);
                    } else if ($key.name === "right") {
                        this.cursorPosition++;
                        const maxPosition : number = getEndPosition();
                        if (this.cursorPosition > maxPosition) {
                            this.cursorPosition = maxPosition;
                        }
                        moveTo(this.cursorPosition);
                    } else if ($key.name === "down" || $key.name === "home") {
                        moveTo(this.prefixLength);
                    } else if ($key.name === "up" || $key.name === "end") {
                        moveTo(getEndPosition());
                    } else if ($key.name === "backspace") {
                        if (this.cursorPosition > this.prefixLength) {
                            this.cursorPosition--;
                            removeAtCursorPosition();
                        }
                    } else if ($key.name === "delete") {
                        removeAtCursorPosition();
                    } else if (!ObjectValidator.IsEmptyOrNull($character)) {
                        const endPosition : number = getEndPosition();
                        if (this.cursorPosition < endPosition) {
                            const addAt : number = this.cursorPosition - this.prefixLength;
                            this.question.value =
                                this.question.value.substring(0, addAt) +
                                $character +
                                this.question.value.substring(addAt, this.question.value.length);
                            this.cursorPosition++;
                        } else {
                            this.question.value += $character;
                            this.cursorPosition = getEndPosition();
                        }
                        printLine(this.question.value);
                    }
                });
            } else {
                process.stdin.on("data", ($value : string) : void => {
                    $value = $value.toString().trim();
                    if (ObjectValidator.IsEmptyOrNull($value)) {
                        this.onKeyPressHandler("", true);
                    } else {
                        this.question.value = $value;
                        this.onKeyPressHandler(this.question.value + "", false);
                    }
                });
            }
        }
    }

    export abstract class StdinQuestion {
        public prefix : string;
        public callback : ($value : string, $callback : ($status : boolean) => void) => void;
        public hidden? : boolean;
        public value? : string;
    }
}
