/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.ErrorPages {
    "use strict";
    import HttpRequestParser = Com.Wui.Framework.Localhost.HttpProcessor.HttpRequestParser;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import HttpServer = Com.Wui.Framework.Localhost.HttpProcessor.HttpServer;

    export class ExceptionErrorPage extends Com.Wui.Framework.Gui.ErrorPages.ExceptionErrorPage {
        public Process() : void {
            super.Process();
            HttpServer.SendData(this.getRequest(), this.RequestArgs().POST().getItem("Connector"),
                HttpStatusType.ERROR, {}, document.documentElement.innerHTML);
        }

        protected getRequest() : HttpRequestParser {
            return <HttpRequestParser>super.getRequest();
        }
    }
}
