/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Primitives {
    import IAuthPromise = Com.Wui.Framework.Localhost.Interfaces.IAuthPromise;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import LiveContentArgumentType = Com.Wui.Framework.Services.Enums.LiveContentArgumentType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * This decorator wraps decorated method to method with authorization promise as return value.
     * @param $autoResponse Set false to disable automatic check for IResponse argument at last position in given arguments and force to
     * call method with each arguments include IResponse from LiveContentResolver.
     * If IResponse has not been identified or $autoResponse = false than method will be called with all given arguments and IResponse has
     * to be handled by method itself.
     * Otherwise ($autoResponse = true) if IResponse is detected in argument list method will be called and return value will be
     * delegated to response.Send(...) automatically.
     * @example Method($arg: number) will be automatically called in response.Send(...) and Method($arg: number, $callback: IResponse)
     * will be automatically called as is.
     */
    export function Extern($autoResponse : boolean = true) {
        // tslint:disable-next-line:only-arrow-functions
        return function ($target : any, $propertyKey : string, $descriptor : PropertyDescriptor) {
            $target[$propertyKey + "_Extern"] = (...$args : any[]) : IAuthPromise => {
                return <IAuthPromise>{
                    auth: Convert.ToType($target) + "." + $propertyKey,
                    resolve() {
                        if ($autoResponse && Reflection.getInstance().Implements($args[$args.length - 1],
                            "Com.Wui.Framework.Localhost.Interfaces.IResponse") && $descriptor.value.length < $args.length) {
                            let retVal : any = $descriptor.value.apply(this, $args);
                            if (!ObjectValidator.IsSet(retVal)) {
                                retVal = LiveContentArgumentType.RETURN_VOID;
                            } else if (typeof retVal === "object" && retVal.constructor.name === "Buffer") {
                                retVal = retVal.toString();
                            }
                            Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory.getResponse($args.pop()).Send(retVal);
                        } else {
                            $descriptor.value.apply(this, $args);
                        }
                    }
                };
            };
        };
    }
}
