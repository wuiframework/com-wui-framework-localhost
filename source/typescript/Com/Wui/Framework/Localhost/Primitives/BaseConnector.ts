/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Primitives {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IConnector = Com.Wui.Framework.Localhost.Interfaces.IConnector;

    export class BaseConnector extends BaseObject implements IConnector {
        private authorizedMethods : string[];

        public static getInstance<TConnector>() : TConnector {
            const instance : any = new this();
            this.getInstance = () : TConnector => {
                return instance;
            };
            return instance;
        }

        protected static pipe<TConnector>($callback : IResponse) : TConnector {
            const instance : any = this.getInstance();
            const wrapper : any = {};
            instance.getMethods().forEach(($method : string) : void => {
                wrapper[$method] = (...$args : any[]) : any => {
                    ResponseFactory.getResponse($callback).Send(instance[$method].apply(instance, $args));
                };
            });
            return wrapper;
        }

        constructor() {
            super();
            this.authorizedMethods = [];
        }

        public getAuthorizedMethods() : string[] {
            return this.authorizedMethods;
        }

        public setAuthorizedMethods($methodList : string[]) {
            this.authorizedMethods = $methodList;
        }
    }
}
