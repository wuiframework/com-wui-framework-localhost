/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost {
    "use strict";
    import IProject = Com.Wui.Framework.Localhost.Interfaces.IProject;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ScriptHandler = Com.Wui.Framework.Localhost.IOApi.Handlers.ScriptHandler;
    import IEnvironmentConfig = Com.Wui.Framework.Commons.IEnvironmentConfig;

    /**
     * EnvironmentArgs class provides environment args mainly set at build time.
     */
    export class EnvironmentArgs extends Com.Wui.Framework.Commons.EnvironmentArgs {
        private readonly isDebug : boolean;

        constructor() {
            super();
            this.isDebug = process.argv.indexOf("--debug") !== -1;
        }

        public HtmlOutputAllowed() : boolean {
            return false;
        }

        public getProcessID() : number {
            return process.pid;
        }

        public getProjectConfig() : IProject {
            return <IProject>super.getProjectConfig();
        }

        protected getConfigPaths() : string[] {
            let paths : string[] = super.getConfigPaths();
            if (!this.HtmlOutputAllowed()) {
                paths = [];
                let cwd : string = EnvironmentHelper.getNodejsRoot();
                if (StringUtils.Contains(cwd, "/dependencies/nodejs")) {
                    cwd = StringUtils.Replace(cwd, "/dependencies/nodejs", "/build/target");
                }

                if (StringUtils.Contains(cwd, "/build/target")) {
                    paths.push(StringUtils.Remove(cwd + "/package.conf.json", "/build/target"));
                    paths.push(StringUtils.Remove(cwd + "/private.conf.json", "/build/target"));
                } else {
                    const appName : string = this.getAppName();
                    const getAppConfigs : any = ($cwd : string) : void => {
                        paths.push($cwd + "/" + appName + ".conf.json");
                        paths.push($cwd + "/" + appName + ".config.jsonp");
                    };
                    paths.push(cwd + "/" + appName + ".config.jsonp");
                    paths.push(cwd + "/private.conf.json");
                    getAppConfigs(cwd + "/..");
                    let appDataPath : string = this.getProjectConfig().target.appDataPath;
                    if (!ObjectValidator.IsEmptyOrNull(appDataPath)) {
                        appDataPath = StringUtils.Replace(appDataPath, "\\", "/");
                        this.getProjectConfig().target.appDataPath = appDataPath;
                        getAppConfigs(appDataPath);
                    }
                }
            }
            return paths;
        }

        protected fileLoaderClass() : any {
            return ScriptHandler;
        }

        protected validate($config : IEnvironmentConfig) : boolean {
            let passed : boolean = false;
            const validator : any = this.getValidator();
            if (!ObjectValidator.IsEmptyOrNull(validator)) {
                try {
                    validator.validate($config.data, validator.schemaConfig);
                    const errors : any[] = validator.getLastErrors();
                    if (!ObjectValidator.IsEmptyOrNull(errors)) {
                        this.printHandler("ERROR: invalid configuration detected at: " + $config.path + "\n" +
                            JSON.stringify(errors[0], null, 2), true);
                    } else {
                        passed = true;
                    }
                } catch (ex) {
                    this.printHandler("> failed validation of " + $config.path + " " + ex.stack);
                }
            }

            if (!passed || !super.validate($config)) {
                Loader.getInstance().Exit(-1);
                return false;
            }
            return true;
        }

        protected printHandler($message : string, $force : boolean = false) : void {
            if (this.isDebug || $force) {
                console.log($message); // tslint:disable-line
            }
        }

        protected errorHandler($error : Error | ErrorEvent, $path? : string) : boolean {
            if (StringUtils.ContainsIgnoreCase($error.message, "Unable to find script")) {
                return super.errorHandler($error, $path);
            }
            this.printHandler("ERROR: " + (ObjectValidator.IsSet((<Error>$error).stack) ? (<Error>$error).stack : $error.message), true);
            Loader.getInstance().Exit(-1);
        }

        private getValidator() : any {
            let validator : any = null;
            try {
                const ZSchema : any = require("z-schema");
                const fs : Types.NodeJS.fs = require("fs");
                const schemaPath : string = require("path").normalize(__dirname + "/../../resource/configs/package.conf.schema.json");
                if (fs.existsSync(schemaPath)) {
                    try {
                        const schemaConfig : any = JSON.parse(fs.readFileSync(schemaPath).toString());
                        validator = new ZSchema({
                            assumeAdditional: true,
                            noExtraKeywords : true
                        });
                        validator.schemaConfig = schemaConfig;
                    } catch (ex) {
                        this.printHandler("> failed processing of validation schema\n" + ex.stack);
                    }
                } else {
                    this.printHandler("> config schema not found at: " + schemaPath);
                }
            } catch (ex) {
                this.printHandler("> failed initialization of config validator\n" + ex.stack);
            }
            this.getValidator = () : any => {
                return validator;
            };
            return validator;
        }
    }

    /**
     * FrontEndEnvironmentArgs class provides environment args mainly used by hosted content.
     */
    export class FrontEndEnvironmentArgs extends Com.Wui.Framework.Commons.EnvironmentArgs {
        protected getConfigPaths() : string[] {
            return [];
        }
    }
}
