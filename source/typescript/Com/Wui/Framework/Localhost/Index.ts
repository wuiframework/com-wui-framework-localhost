/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class Index extends Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.BaseHttpResolver {
        protected resolver() : void {
            const EOL : string = StringUtils.NewLine(false);
            const wrapper : any = ($content : string) : string => {
                return "" +
                    "<div class=\"GuiInterface\">" + EOL +
                    "   <h1>WUI Framework Localhost " + this.getEnvironmentArgs().getProjectVersion() + "</h1>" + EOL +
                    "   <h3>WUI Framework's code base for localhost based on Node.js</h3>" + EOL +
                    "   <div class=\"Index\">" + EOL +
                    $content + EOL +
                    "   </div>" + EOL +
                    "</div>" + EOL +
                    "<div class=\"Note\">" + EOL +
                    "version: " + this.getEnvironmentArgs().getProjectVersion() +
                    ", build: " + this.getEnvironmentArgs().getBuildTime() +
                    "</div>" + EOL +
                    "<div class=\"Logo\">" + EOL +
                    "   <div class=\"WUI\"></div>" + EOL +
                    "</div>";
            };

            StaticPageContentManager.Clear();
            StaticPageContentManager.Title("WUI Framework Localhost");
            if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
                StaticPageContentManager.License(
                    "<!--" + EOL +
                    EOL +
                    "Copyright (c) 2010-2013 Jakub Cieslar" + EOL +
                    "Copyright (c) 2014-2016 Freescale Semiconductor, Inc." + EOL +
                    "Copyright (c) 2017-2019 NXP" + EOL +
                    EOL +
                    "SPDX-License-Identifier: BSD-3-Clause" + EOL +
                    "The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution" + EOL +
                    "or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText" + EOL +
                    EOL +
                    "-->"
                );
                let packageName : string = this.getEnvironmentArgs().getProjectName() + "-" + this.getEnvironmentArgs().getProjectVersion();
                packageName = StringUtils.Replace(packageName, ".", "-");
                StaticPageContentManager.HeadScriptAppend("resource/javascript/" + packageName + ".min.js");
                StaticPageContentManager.HeadScriptAppend("resource/javascript/loader.min.js");

                StaticPageContentManager.BodyAppend(
                    "<noscript>" + EOL +
                    wrapper(
                        "This library requires enabled JavaScript in the browser. See link below for more information:" +
                        StringUtils.NewLine() +
                        "<a href=\"http://www.enable-javascript.com/\" target=\"_blank\">How to enable JavaScript?</a>"
                    ) + EOL +
                    "</noscript>");
            } else {
                let content : string = "";
                /* dev:start */
                content +=
                    "<H3>Runtime tests</H3>" +
                    "<a href=\"" + RuntimeTests.RestServiceTest.CallbackLink() + "\">Back-end connection test</a>" + StringUtils.NewLine() +
                    "<a href=\"" + RuntimeTests.AuthConnectorTest.CallbackLink() + "\">Auth connector access test</a>" +
                    StringUtils.NewLine() +
                    StringUtils.NewLine();
                /* dev:end */
                StaticPageContentManager.BodyAppend(wrapper(content));
            }
            StaticPageContentManager.Draw();
        }
    }
}
