/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Localhost.Structures {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StdinManager = Com.Wui.Framework.Localhost.Utils.StdinManager;
    import StdinQuestion = Com.Wui.Framework.Localhost.Utils.StdinQuestion;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class ProgramArgs extends BaseObject {
        private stdinHandler : any;
        private isVersion : boolean;
        private isHelp : boolean;
        private isPath : boolean;
        private startServer : boolean;
        private stopServer : boolean;
        private restartServer : boolean;
        private targetBase : string;
        private binBase : string;
        private projectBase : string;
        private appDataPath : string;
        private isDebug : boolean;
        private isVerbose : boolean;
        private isTestRun : boolean;

        constructor() {
            super();
            this.stdinHandler = () : void => {
                // default handler
            };
            this.isVersion = false;
            this.isHelp = false;
            this.isPath = false;
            this.startServer = false;
            this.stopServer = false;
            this.restartServer = false;
            this.targetBase = "";
            this.binBase = "";
            this.projectBase = "";
            this.appDataPath = "";
            this.isDebug = false;
            this.isVerbose = false;
            this.isTestRun = false;
        }

        public Normalize($args : string[]) : string[] {
            const output : string[] = [];
            let index : number;
            const length : number = $args.length;
            let key : string = "";
            let value : string = "";
            const assignPair : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(key) && ObjectValidator.IsEmptyOrNull(value) && StringUtils.Contains(key, "=")) {
                    const index : number = StringUtils.IndexOf(key, "=");
                    value = StringUtils.Substring(key, index + 1);
                    key = StringUtils.Substring(key, 0, index);
                }
                if (!ObjectValidator.IsEmptyOrNull(value)) {
                    if (StringUtils.Contains(value, " ")) {
                        if (StringUtils.StartsWith(value, "\"") && StringUtils.EndsWith(value, "\"") ||
                            StringUtils.StartsWith(value, "'") && StringUtils.EndsWith(value, "'")) {
                            value = StringUtils.Substring(value, 1, StringUtils.Length(value) - 1);
                        }
                        value = "\"" + value + "\"";
                    }
                    output.push(key + "=" + value);
                    value = "";
                } else if (!ObjectValidator.IsEmptyOrNull(key)) {
                    output.push(key);
                }
            };
            for (index = 0; index < length; index++) {
                const arg : string = $args[index];
                if ((StringUtils.StartsWith(arg, "-") ||
                    StringUtils.StartsWith(arg, "/") && StringUtils.Length(arg) === 2 ||
                    StringUtils.Contains(arg, "=")) &&
                    !ObjectValidator.IsDigit(arg)) {
                    assignPair();
                    key = arg;
                } else {
                    if (!ObjectValidator.IsEmptyOrNull(key)) {
                        if (!ObjectValidator.IsEmptyOrNull(value)) {
                            value += " ";
                        }
                        value += arg;
                    } else {
                        output.push(arg);
                    }
                }
            }
            assignPair();
            return output;
        }

        public Parse($args : string[]) : void {
            $args = this.Normalize($args);
            if (StringUtils.ContainsIgnoreCase($args[1], "loader.min.js")) {
                $args.shift();
                $args.shift();
            }
            if ($args.length > 0) {
                const pairs : ArrayList<string> = new ArrayList<string>();
                for (const arg of $args) {
                    let key : string = arg;
                    let value : string = "";
                    if (StringUtils.Contains(arg, "=")) {
                        key = StringUtils.Substring(arg, 0, StringUtils.IndexOf(arg, "="));
                        value = StringUtils.Substring(arg, StringUtils.IndexOf(arg, "=") + 1);
                        if (StringUtils.StartsWith(value, "\"")) {
                            value = StringUtils.Substring(value, 1);
                        }
                        if (StringUtils.EndsWith(value, "\"")) {
                            value = StringUtils.Substring(value, 0, StringUtils.Length(value) - 1);
                        }
                    }
                    pairs.Add(value, key);
                }
                this.processArgs(pairs);
            } else {
                this.processDefaultArgs();
            }
        }

        public setStdinHandler($handler : ($data : string) => void) : void {
            if (ObjectValidator.IsFunction($handler)) {
                this.stdinHandler = $handler;
            }
        }

        public IsVersion($value? : boolean) : boolean {
            return this.isVersion = Property.Boolean(this.isVersion, $value);
        }

        public IsHelp($value? : boolean) : boolean {
            return this.isHelp = Property.Boolean(this.isHelp, $value);
        }

        public IsPath($value? : boolean) : boolean {
            return this.isPath = Property.Boolean(this.isPath, $value);
        }

        public StartServer($value? : boolean) : boolean {
            return this.startServer = Property.Boolean(this.startServer, $value);
        }

        public StopServer($value? : boolean) : boolean {
            return this.stopServer = Property.Boolean(this.stopServer, $value);
        }

        public RestartServer($value? : boolean) : boolean {
            return this.restartServer = Property.Boolean(this.restartServer, $value);
        }

        public BinBase($value? : string) : string {
            return this.binBase = Property.String(this.binBase, $value);
        }

        public ProjectBase($value? : string) : string {
            return this.projectBase = Property.String(this.projectBase, $value);
        }

        public TargetBase($value? : string) : string {
            return this.targetBase = Property.String(this.targetBase, $value);
        }

        public AppDataPath($value? : string) : string {
            return this.appDataPath = Property.String(this.appDataPath, $value);
        }

        public IsDebug($value? : boolean) : boolean {
            return this.isDebug = Property.Boolean(this.isDebug, $value);
        }

        public IsVerbose($value? : boolean) : boolean {
            return this.isVerbose = Property.Boolean(this.isVerbose, $value);
        }

        public IsTestRun($value? : boolean) : boolean {
            this.isTestRun = Property.Boolean(this.isTestRun, $value);
            return this.isTestRun || <any>process.env.IS_WUI_TEST_RUN;
        }

        public PrintVersion() : void {
            Echo.Print(Loader.getInstance().getEnvironmentArgs().getAppName() +
                " v" + Loader.getInstance().getEnvironmentArgs().getProjectVersion());
        }

        public PrintPath() : void {
            Echo.Print(this.ProjectBase());
        }

        public PrintHelp() : void {
            this.getHelp(
                "" +
                "WUI Framework Localhost                                                      \n" +
                "   - Localhost service for WUI Framework with websockets support             \n" +
                "                                                                             \n" +
                "   copyright:         Copyright (c) 2010-2013 Jakub Cieslar                  \n" +
                "                      Copyright (c) 2014-2016 Freescale Semiconductor, Inc.  \n" +
                "                      Copyright (c) 2017-2019 NXP                            \n" +
                "   author:            Jakub Cieslar, jakub.cieslar@nxp.com                   \n" +
                "   license:           BSD-3-Clause License distributed with this material    \n",

                "" +
                "Basic options:                                                               \n" +
                "   -h [ --help ]      Prints application help description.                   \n" +
                "   -v [ --version ]   Prints application version message.                    \n" +
                "   -p [ --path ]      Print current WUI Host location.                       \n" +
                "   --debug            Run current WUI Host instance in debug mode.           \n" +
                "   --verbose          Run current WUI Host instance in verbose mode.         \n" +
                "                                                                             \n" +
                "Server options:                                                              \n" +
                "   start              Start localhost service.                               \n" +
                "   stop               Stop localhost service.                                \n" +
                "   restart            Restart localhost service.                             \n" +
                "                                                                             \n" +
                "Other options:                                                               \n" +
                "   --target=<path>    Specify target *.html file to host by localhost.       \n"
            );
        }

        protected processDefaultArgs() : void {
            this.IsHelp(true);
        }

        protected processArgs($args : ArrayList<string>) : void {
            $args.foreach(($value : string, $key : string) : void => {
                if (!this.processArg($key, $value)) {
                    Echo.Printf("Unrecognized program option: {0}", $key);
                    Loader.getInstance().Exit(-1);
                }
            });
        }

        protected processArg($key : string, $value : string) : boolean {
            switch (StringUtils.ToLowerCase($key)) {
            case "--help":
            case "-h":
            case "/h":
            case "/?":
            case "help":
            case "?":
                this.IsHelp(true);
                break;
            case "--version":
            case "-v":
            case "/v":
            case "version":
                this.IsVersion(true);
                break;
            case "--path":
            case "-p":
            case "/p":
            case "path":
                this.IsPath(true);
                break;
            case "--debug":
                this.IsDebug(true);
                break;
            case "--verbose":
                this.IsVerbose(true);
                break;
            case "start":
                this.StartServer(true);
                break;
            case "stop":
                this.StopServer(true);
                break;
            case "restart":
                this.RestartServer(true);
                break;
            case "--target":
                this.TargetBase($value);
                break;
            case "--test-run":
                this.IsTestRun(true);
                break;
            default:
                return false;
            }
            return true;
        }

        protected getHelp($header : string, $body : string, $footer? : string, $question? : string) : void {
            if (ObjectValidator.IsEmptyOrNull($footer)) {
                $footer =
                    "NOTE:                                                                        \n" +
                    "    This program can depends on several prerequisites for more               \n" +
                    "    info see README.md distributed with this material.                       \n";
            }

            Echo.Print("" +
                "_____________________________________________________________________________\n" +
                "                                                                             \n" +
                $header +
                "_____________________________________________________________________________\n" +
                "/////////////////////////////////////////////////////////////////////////////\n" +
                "                                                                             \n" +
                $body +
                "                                                                             \n" +
                $footer +
                "                                                                             \n" +
                "-----------------------------------------------------------------------------\n");

            if (ObjectValidator.IsEmptyOrNull($question)) {
                $question = "Hit Enter for exit or specify program options";
            }
            let value : string = "";
            StdinManager.Process([
                <StdinQuestion>{
                    callback: ($task : string, $callback : ($status : boolean) => void) : void => {
                        value = $task;
                        $callback(true);
                    },
                    prefix  : $question
                }
            ], () : void => {
                this.stdinHandler(value);
            });
        }
    }
}
