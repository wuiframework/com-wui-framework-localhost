/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Localhost.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import WebSocketsClient = Com.Wui.Framework.Commons.WebServiceApi.Clients.WebSocketsClient;
    import ILiveContentErrorPromise = Com.Wui.Framework.Services.Interfaces.ILiveContentErrorPromise;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class RestServiceTestHandler extends Com.Wui.Framework.Localhost.Primitives.BaseConnector {

        @Extern()
        public SelfCallMethod($message : string) : boolean {
            Echo.Printf("SelfCallMethod message: {0}", $message);
            return true;
        }

        @Extern()
        public ExceptionTest($message : string) : void {
            throw new Error($message);
        }

        @Extern()
        public ErrorTest($message : string, $callback? : IResponse) : void {
            ResponseFactory.getResponse($callback).OnError($message, {responseType: 404}, "additional message");
        }

        @Extern()
        public UnpackTest($path : string, $callback? : IResponse) : void {
            Loader.getInstance().getFileSystemHandler().Unpack($path, null, $callback);
        }

        @Extern()
        public DownloadTest($callback? : IResponse) : void {
            Loader.getInstance().getFileSystemHandler().Download(
                "https://github.com/git-for-windows/git/releases/download/v2.21.0.windows.1/Git-2.21.0-64-bit.exe",
                $callback);
        }

        @Extern()
        public DataTransferTest($data : string) : number {
            return $data.length;
        }
    }

    export class RestServiceTestConnector extends BaseConnector {

        public SelfCallMethod($message : string) : ILiveContentErrorPromise {
            return this.invoke("SelfCallMethod", $message);
        }

        public ExceptionTest($message : string) : ILiveContentErrorPromise {
            return this.invoke("ExceptionTest", $message);
        }

        public ErrorTest($message : string) : ILiveContentErrorPromise {
            return this.invoke("ErrorTest", $message);
        }

        public Unpack($path : string) : ILiveContentErrorPromise {
            return this.invoke("UnpackTest", $path);
        }

        public Download() : ILiveContentErrorPromise {
            return this.invoke("DownloadTest");
        }

        public DataTransfer($data : string) : ILiveContentErrorPromise {
            return this.invoke("DataTransferTest", $data);
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.WEB_SOCKETS;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WEB_SOCKETS] = RestServiceTestHandler.ClassName();
            return namespaces;
        }
    }

    export class RestServiceTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testWebsocketsConnection() : void {
            const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                this.getRequest().getHostUrl() + "connector.config.jsonp");
            client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf("General error handler: {0}", $eventArgs.Exception().ToString());
            });

            const invoke : any = ($method : string, $message : string) : void => {
                LiveContentWrapper.InvokeMethod(client, RestServiceTestHandler.ClassName(), $method, $message)
                    .Then(() : void => {
                        Echo.Printf("Function successfully invoked.");
                    });
            };

            this.addButton("Self-Invoke function", () : void => {
                invoke("SelfCallMethod", "test message");
            });

            /// TODO: enable test after auth method will be possible
            // this.addButton("Secure Self-Invoke function", () : void => {
            //     invoke("SelfSecureCallMethod", "secure test message");
            // });
        }

        public testWebsocketsErrors() : void {
            const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                this.getRequest().getHostUrl() + "connector.config.jsonp");
            (<WebSocketsClient>client).MaxReconnectsCount(10);
            // (<WebSocketsClient>client).MaxReconnectsCount(0);
            client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf("General error handler: {0}", $eventArgs.Exception().ToString());
            });

            this.addButton("Missing API invoke", () : void => {
                LiveContentWrapper.InvokeMethod(client, RestServiceTestHandler.ClassName(), "SomethingMissing", "test message")
                    .Then(() : void => {
                        Echo.Printf("Function successfully invoked?");
                    });
            });

            this.addButton("Exception invoke", () : void => {
                LiveContentWrapper.InvokeMethod(client, RestServiceTestHandler.ClassName(), "ExceptionTest", "test message")
                    .OnError(($eventArgs : ErrorEventArgs) : void => {
                        Echo.Printf("Exception successfully handled: {0}", $eventArgs.Exception().ToString());
                    })
                    .Then(() : void => {
                        Echo.Printf("Function successfully invoked?");
                    });
            });

            this.addButton("Error invoke", () : void => {
                LiveContentWrapper.InvokeMethod(client, RestServiceTestHandler.ClassName(), "ErrorTest", "test message")
                    .OnError(($eventArgs : ErrorEventArgs, $result : any, $nextMessage : string) : void => {
                        Echo.Printf("Error successfully handled: {0}", $eventArgs.Exception().ToString());
                        Echo.Printf("{0}{1}", $result, $nextMessage);
                    })
                    .Then(() : void => {
                        Echo.Printf("Function successfully invoked?");
                    });
            });

            this.addButton("Global exception invoke", () : void => {
                LiveContentWrapper.InvokeMethod(client, RestServiceTestHandler.ClassName(), "ExceptionTest", "test message")
                    .Then(() : void => {
                        Echo.Printf("Function successfully invoked?");
                    });
            });

            this.addButton("Global error invoke", () : void => {
                LiveContentWrapper.InvokeMethod(client, RestServiceTestHandler.ClassName(), "ErrorTest", "test message")
                    .Then(() : void => {
                        Echo.Printf("Function successfully invoked?");
                    });
            });
        }

        public testHttpConnection() : void {
            const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.WUI_REST_CONNECTOR,
                this.getRequest().getHostUrl() + "connector.config.jsonp");
            client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf($eventArgs.Exception().ToString());
            });

            this.addButton("Self-Invoke function", () : void => {
                LiveContentWrapper.InvokeMethod(client, RestServiceTestHandler.ClassName(), "SelfCallMethod", "test message")
                    .Then(() : void => {
                        Echo.Printf("Function successfully invoked.");
                    });
            });

            /// TODO: enable test after auth method will be possible
            // this.addButton("Secure Self-Invoke function", () : void => {
            //    LiveContentWrapper.InvokeMethod(client, RestServiceTestHandler.ClassName(), "SelfSecureCallMethod", "secure test message")
            //         .Then(() : void => {
            //             Echo.Printf("Function successfully invoked.");
            //         });
            // });
        }

        public testConnector() : void {
            const connector : RestServiceTestConnector = new RestServiceTestConnector(10,
                this.getRequest().getHostUrl() + "/connector.config.jsonp");
            this.addButton("Unpack invoke", () : void => {
                const basePath : string = this.getAbsoluteRoot() + "/../../test/resource/data/Com/Wui/Framework/Localhost/Connectors";
                connector
                    .Unpack(basePath + "/archTest1.zip")
                    .Then(($path : string) : void => {
                        Echo.Printf("Unpacked to: {0}", $path);
                        connector
                            .Unpack(basePath + "/archTest1.tar.xz")
                            .Then(($path : string) : void => {
                                Echo.Printf("Unpacked to: {0}", $path);
                            });
                    });
            });

            this.addButton("Download invoke", () : void => {
                Echo.Printf("<span id=\"status\"></span>");
                connector
                    .Download()
                    .Then(($data : any) : void => {
                        if ($data.type === "oncomplete") {
                            Echo.Printf("downloaded");
                        } else if ($data.type === "onstart") {
                            Echo.Printf("download stared");
                        } else if ($data.type === "onchange") {
                            document.getElementById("status").innerText = $data.data.currentValue;
                        }
                    });
            });

            this.addButton("Large data transfer", () : void => {
                let data : string = "";
                while (data.length < 70 * 1024) {
                    data += "Some test string...Some test string...Some test string...Some test string...";
                }
                Echo.Printf("Send message with length: {0}", data.length);
                connector
                    .DataTransfer(data)
                    .Then(($length : number) : void => {
                        Echo.Printf("Received message with length: {0}", $length);
                        this.assertEquals(data.length, $length);
                    });
            });
        }
    }
}
/* dev:end */
