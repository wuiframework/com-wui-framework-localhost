/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Localhost.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import ILiveContentErrorPromise = Com.Wui.Framework.Services.Interfaces.ILiveContentErrorPromise;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;

    export class AuthConnector extends Com.Wui.Framework.Localhost.Primitives.BaseConnector {

        @Extern()
        public static AuthMethodStaticWithCallback($callback : IResponse) : void {
            LogIt.Debug("AuthMethodStaticWithCallback called");
            ResponseFactory.getResponse($callback).Send(true);
        }

        @Extern()
        public static AuthMethodStaticWithReturn() : boolean {
            LogIt.Debug("AuthMethodStaticWithReturn called");
            return true;
        }

        @Extern()
        public AuthMethodWithCallback($callback : IResponse) : void {
            LogIt.Debug("AuthMethodWithCallback called, q: {0}", this.AuthMethodWithReturn());
            ResponseFactory.getResponse($callback).Send(true);
        }

        @Extern()
        public AuthMethodWithReturn() : boolean {
            LogIt.Debug("AuthMethodWithReturn called");
            return true;
        }

        @Extern()
        public AuthMethodVoid() : void {
            LogIt.Debug("AuthMethodVoid called");
        }

        @Extern()
        public AuthMethodNull() : any {
            LogIt.Debug("AuthMethodNull called");
            return null;
        }

        @Extern()
        public AuthMethodEmpty() : any {
            LogIt.Debug("AuthMethodEmpty called");
            return {};
        }

        @Extern()
        public AuthMethodBuffer() : string | Buffer {
            LogIt.Debug("AuthMethodBuffer called");
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const path : string = fileSystem.getTempPath() + "/" + Loader.getInstance().getAppConfiguration().name + "/test.txt";
            fileSystem.Write(path, "hello world");
            return fileSystem.Read(path);
        }
    }

    export class AuthTestConnector extends BaseConnector {

        public AuthMethodStaticWithCallback() : ILiveContentErrorPromise {
            return this.invoke("AuthMethodStaticWithCallback");
        }

        public AuthMethodStaticWithReturn() : ILiveContentErrorPromise {
            return this.invoke("AuthMethodStaticWithReturn");
        }

        public AuthMethodWithCallback() : ILiveContentErrorPromise {
            return this.invoke("AuthMethodWithCallback");
        }

        public AuthMethodWithReturn() : ILiveContentErrorPromise {
            return this.invoke("AuthMethodWithReturn");
        }

        public AuthMethodVoid() : ILiveContentErrorPromise {
            return this.invoke("AuthMethodVoid");
        }

        public AuthMethodNull() : ILiveContentErrorPromise {
            return this.invoke("AuthMethodNull");
        }

        public AuthMethodEmpty() : ILiveContentErrorPromise {
            return this.invoke("AuthMethodEmpty");
        }

        public AuthMethodBuffer() : ILiveContentErrorPromise {
            return this.invoke("AuthMethodBuffer");
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.WEB_SOCKETS;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WEB_SOCKETS] = "Com.Wui.Framework.Localhost.RuntimeTests.AuthConnector";
            return namespaces;
        }
    }

    export class AuthConnectorTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {
        public testConnector() : void {
            const connector : AuthTestConnector = new AuthTestConnector(10,
                this.getRequest().getHostUrl() + "/connector.config.jsonp");
            this.addButton("AuthMethodStaticWithCallback", () : void => {
                connector
                    .AuthMethodStaticWithCallback()
                    .Then(($status : boolean) : void => {
                        Echo.Printf("AuthMethodStaticWithCallback status: {0}", $status);
                    });
            });
            this.addButton("AuthMethodStaticWithReturn", () : void => {
                connector
                    .AuthMethodStaticWithReturn()
                    .Then(($status : boolean) : void => {
                        Echo.Printf("AuthMethodStaticWithReturn status: {0}", $status);
                    });
            });
            this.addButton("AuthMethodWithCallback", () : void => {
                connector
                    .AuthMethodWithCallback()
                    .Then(($status : boolean) : void => {
                        Echo.Printf("AuthMethodWithCallback status: {0}", $status);
                    });
            });
            this.addButton("AuthMethodWithReturn", () : void => {
                connector
                    .AuthMethodWithReturn()
                    .Then(($status : boolean) : void => {
                        Echo.Printf("AuthMethodWithReturn status: {0}", $status);
                    });
            });
            this.addButton("AuthMethodVoid", () : void => {
                connector
                    .AuthMethodVoid()
                    .Then(($retVal : any) : void => {
                        Echo.Printf("AuthMethodVoid status: {0}", $retVal);
                    });
            });
            this.addButton("AuthMethodNull", () : void => {
                connector
                    .AuthMethodNull()
                    .Then(($retVal : any) : void => {
                        Echo.Printf("AuthMethodNull status: {0}", $retVal);
                    });
            });
            this.addButton("AuthMethodEmpty", () : void => {
                connector
                    .AuthMethodEmpty()
                    .Then(($retVal : any) : void => {
                        Echo.Printf("AuthMethodEmpty status: {0}", $retVal);
                    });
            });
            this.addButton("AuthMethodBuffer", () : void => {
                connector
                    .AuthMethodBuffer()
                    .Then(($retVal : any) : void => {
                        Echo.Printf("AuthMethodBuffer status: {0}", $retVal);
                    });
            });
        }
    }
}
/* dev:end */
