/* tslint:disable:no-reference */
/// <reference path="../../dependencies/com-wui-framework-commons/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-gui/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-services/source/typescript/reference.d.ts" />

/// <reference path="Com/Wui/Framework/Localhost/Utils/EnvironmentHelper.ts" />
/// <reference path="Types.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Interfaces/IProject.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Interfaces/IResponse.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Interfaces/IRequestConnector.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/ResponseApi/Handlers/BaseResponse.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/ResponseApi/Handlers/WebResponse.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/Resolvers/BaseHttpResolver.ts" />

// generated-code-start
/// <reference path="Com/Wui/Framework/Localhost/Interfaces/Events/IHttpServerEvents.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Interfaces/IConnector.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Primitives/Decorators.ts" />
/// <reference path="interfacesMap.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Events/EventsManager.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/HttpManager.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Enums/StdinType.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Enums/ColorType.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Enums/HttpServerEventType.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/ResponseApi/Handlers/CallbackResponse.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Events/Args/HttpServerEventArgs.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/Resolvers/XOriginInterface.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/HttpResolver.ts" />
/// <reference path="Com/Wui/Framework/Localhost/IOApi/Handlers/ScriptHandler.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Controllers/Pages/MainPage.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/HttpRequestParser.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Utils/FirewallManager.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Index.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/ResponseApi/Handlers/LiveContentResponse.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/ResponseApi/ResponseFactory.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Utils/StdinManager.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Utils/Metrics.ts" />
/// <reference path="Com/Wui/Framework/Localhost/EnvironmentArgs.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Primitives/BaseConnector.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/Resolvers/ConnectorResponse.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Structures/ProgramArgs.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Connectors/Terminal.ts" />
/// <reference path="Com/Wui/Framework/Localhost/RuntimeTests/RestServiceTest.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Connectors/FileSystemHandler.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/Resolvers/LiveContentResolver.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/Resolvers/FileRequestResolver.ts" />
/// <reference path="Com/Wui/Framework/Localhost/RuntimeTests/AuthConnectorTest.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/Resolvers/XOriginServices.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Utils/CertsManager.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/Resolvers/AcmeResolver.ts" />
/// <reference path="Com/Wui/Framework/Localhost/HttpProcessor/HttpServer.ts" />
/// <reference path="Com/Wui/Framework/Localhost/ErrorPages/ExceptionErrorPage.ts" />
/// <reference path="Com/Wui/Framework/Localhost/ErrorPages/Http404NotFoundPage.ts" />
/// <reference path="Com/Wui/Framework/Localhost/Loader.ts" />
// generated-code-end
