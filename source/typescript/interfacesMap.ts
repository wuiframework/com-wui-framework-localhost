/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Com.Wui.Framework.Localhost.Interfaces.Events {
    "use strict";
    export let IHttpServerEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnStart",
                "OnConnect",
                "OnDisconnect"
            ]);
        }();
}

namespace Com.Wui.Framework.Localhost.Interfaces {
    "use strict";
    export let IResponse : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Send",
                "OnStart",
                "OnChange",
                "OnComplete",
                "OnError",
                "OnMessage",
                "FireEvent",
                "getId",
                "getOwnerId",
                "AddAbortHandler",
                "Abort"
            ]);
        }();
}

namespace Com.Wui.Framework.Localhost.Interfaces {
    "use strict";
    export let IResponsePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Localhost.Interfaces {
    "use strict";
    export let IConnector : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getAuthorizedMethods",
                "setAuthorizedMethods"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

/* tslint:enable */
