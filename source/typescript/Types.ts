/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
import * as _child_process from "child_process";
import * as _fs from "fs";
import * as _http from "http";
import * as _https from "https";
import * as _net from "net";
import * as _os from "os";
import * as _path from "path";
import * as _readline from "readline";
import * as _stream from "stream";
import * as _tls from "tls";
import * as _url from "url";
import * as _vm from "vm";

declare global {
    namespace Types {
        namespace NodeJS {
            type fs = typeof _fs;
            type child_process = typeof _child_process;
            type path = typeof _path;
            type os = typeof _os;
            type url = typeof _url;
            type http = typeof _http;
            type https = typeof _https;
            type net = typeof _net;
            type stream = typeof _stream;
            type tls = typeof _tls;
            type vm = typeof _vm;
            type readline = typeof _readline;

            namespace Modules {
                namespace child_process {
                    type exec = typeof _child_process.exec;
                    type spawn = typeof _child_process.spawn;
                    type spawnSync = typeof _child_process.spawnSync;
                    type ChildProcess = _child_process.ChildProcess;
                    type SpawnSyncReturns<T> = _child_process.SpawnSyncReturns<T>;
                }

                namespace url {
                    type UrlWithStringQuery = _url.UrlWithStringQuery;
                }

                namespace path {
                    type normalize = typeof _path.normalize;
                }

                namespace os {
                    type networkInterfaces = typeof _os.networkInterfaces;
                }

                namespace http {
                    type Server = _http.Server;
                    type ServerOptions = _http.ServerOptions;
                    type ClientRequest = _http.ClientRequest;
                }

                namespace https {
                    type Server = _https.Server;
                    type ServerOptions = _https.ServerOptions;
                }

                namespace net {
                    type AddressInfo = _net.AddressInfo;
                }

                namespace stream {
                    type Readable = _stream.Readable;
                }
            }
        }
    }
}
